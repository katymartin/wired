<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( isset( $this->options['1']['page_size'] ) ) {
	$pagesize = ( $this->options['1']['page_size'] );
} else {
	$pagesize = PDF_PAGE_FORMAT;
}
if ( isset( $this->options['1']['unitmeasure'] ) ) {
	$unit = ( $this->options['1']['unitmeasure'] );
} else {
	$unit = PDF_UNIT;
}
if ( isset( $this->options['1']['page_orientation'] ) ) {
	$orientation = ( $this->options['1']['page_orientation'] );
} else {
	$orientation = PDF_PAGE_ORIENTATION;
}
$pdf = new CUSTOMPDF ( $orientation, $unit, $pagesize, true, 'UTF-8', false );
$pdf->SetCreator ( 'WP Post to PDF plugin by makewebbetter with ' . PDF_CREATOR );
$pdf->SetAuthor ( get_bloginfo ( 'name' ) );
	
// logo width calculation
if (isset ( $this->options['2']['page_header'] ) and ($this->options['2']['page_header']) != 'None' and !empty ( $this->options['2']['logo_img_url'] )) {
	
	if ($this->options['2']['page_header'] == "upload-image") {
		$logoImage_url = $this->options['2']['logo_img_url'];
	}
	$infologo = getimagesize ( $logoImage_url );
	if (isset ( $this->options['2']['image_factor'] )) {
		$logo_width = ( int ) (($this->options['2']['image_factor'] * $infologo [0]) / $infologo [1]);
	} else {
		$logo_width = ( int ) ((12 * $infologo [0]) / $infologo [1]);
	}
	// for PHP 5.4 or below set default header data
	if (version_compare ( phpversion (), '5.4.0', '<' )) {
		$pdf->SetHeaderData ( $logoImage_url, $logo_width, html_entity_decode ( get_bloginfo ( 'name' ), ENT_COMPAT | ENT_QUOTES ), html_entity_decode ( get_bloginfo ( 'description' ) . "\n" . home_url (), ENT_COMPAT | ENT_QUOTES ), $header_text_color, $header_line_color );
	} else {
		$pdf->SetHeaderData ( $logoImage_url, $logo_width, html_entity_decode ( get_bloginfo ( 'name' ), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES ), html_entity_decode ( get_bloginfo ( 'description' ) . "\n" . home_url (), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES )  );
	}
}

if (isset ( $this->options['2']['page_header'] ) and ($this->options['2']['page_header']) == 'None') {
	$pdf->setPrintHeader(false);
}
	
// set header and footer fonts
if(($this->options['2']['header_font_size']) > 0) {
	$header_font_size = $this->options['2']['header_font_size'];
} else {
	$header_font_size = 10;
}
if(  isset($this->options['footer_font_size']) && !empty($this->options['footer_font_size']) && (($this->options['footer_font_size']) > 0)) {
	$footer_font_size = $this->options ['footer_font_size'];
} else {
	$footer_font_size = 10;
}

$header_font_pdf_value_dis = isset ( $this->options['2']['header_font_pdf'] ) ? $this->options['2']['header_font_pdf'] : 'helvetica';

$pdf->setHeaderFont ( array (
		$header_font_pdf_value_dis,
		'',
		$header_font_size 
) );

$header_font_pdf_value_dis = isset ( $this->options ['footer_font_pdf'] ) ? $this->options ['footer_font_pdf'] : 'helvetica';

$pdf->setFooterFont ( array (
		$header_font_pdf_value_dis ,
		'',
		$footer_font_size 
) );
	
$pdf->SetDefaultMonospacedFont ( PDF_FONT_MONOSPACED );

if (isset($this->options['1']['margin_left'])) {
	$pdf->SetLeftMargin ( $this->options['1']['margin_left'] );
} else {
	$pdf->SetLeftMargin ( PDF_MARGIN_LEFT );
}
	
if (isset($this->options['1']['margin_right'])) {
	$pdf->SetRightMargin ( $this->options['1']['margin_right'] );
} else {
	$pdf->SetRightMargin ( PDF_MARGIN_RIGHT );
}
	
if (isset($this->options['1']['margin_top'])) {
	$pdf->SetTopMargin ( $this->options['1']['margin_top'] );
} else {
	$pdf->SetTopMargin ( PDF_MARGIN_TOP );
}
if ((isset($this->options['2']['logomTop']))) {
	$pdf->SetHeaderMargin ( $this->options['2']['logomTop'] );
} else {
	$pdf->SetHeaderMargin ( PDF_MARGIN_HEADER );
}

if (isset($this->options['footer_font_margin'] )) {
	$pdf->SetFooterMargin ( $this->options['footer_font_margin'] );
	// set auto page breaks
	$pdf->SetAutoPageBreak ( TRUE,  $this->options['footer_font_margin']  );
} else {
	$pdf->SetFooterMargin ( PDF_MARGIN_FOOTER );
	// set auto page breaks
	$pdf->SetAutoPageBreak ( TRUE, PDF_MARGIN_FOOTER );
}
if (! empty ( $this->options['5']['bullet_img_url'] )) {
	$temp = $this->options['5']['bullet_img_url'];
	$temp = end (( explode ( '/', $temp ) ));
	$temp = end (( explode ( '.', $temp ) ));
	$listsymbol = 'img|' . $temp . '|' . $this->options['5']['custom_image_width'] . '|' . $this->options['5']['custom_image_height'] . '|' . $this->options['5']['bullet_img_url'];
	$pdf->setLIsymbol ( $listsymbol );
}

// set image scale factor

if ( isset($this->options['1']['image_scale']) && !empty($this->options['1']['image_scale']) &&($this->options['1']['image_scale'] > 0)) {
	$pdf->setImageScale ( $this->options['1']['image_scale'] );
} else {
	$pdf->setImageScale ( PDF_IMAGE_SCALE_RATIO );
}
	
// set default font subsetting mode
$pdf->setFontSubsetting( true );
	
$pdf->SetFont( $this->options['1']['content_font_pdf'], '', $this->options['1']['content_font_size'], '', true );
	
foreach ($post_ids as $post_id) {
	$post = get_post ( $post_id );
	
	$content = $post->the_content;
	{
		if (has_shortcode ( $content, 'mwbpdf' )) {
			if (! $this->options['6'][$post->post_type][$post->post_type])
				return false;
		}
	}
	
	// 	$pdf->AddPage ();
	
	if ($this->options['4']['fontStretching']) {
		$pdf->setFontStretching($this->options['4']['fontStretching']);
	}
	if ($this->options['4']['fontSpacig']) {
		$pdf->setFontSpacing($this->options['4']['fontSpacig']);
	}
	$page_format = array();
	if ($this->options['4']['set_rotation']) {
		$page_format['Rotate'] = $this->options['4']['set_rotation'];
	} else {
		$page_format['Rotate'] = 0;
	}
	$pdf->AddPage($this->options['1']['page_orientation'], $page_format, false, false);
	$pdf->Bookmark($post->post_title, 1, -1, '', '', array(0,0,0));
	$html = '';
	if (isset($this->options ['applyCSS']) && !empty($this->options ['applyCSS']) && $this->options ['applyCSS']) {
		$html .= '<style>' . $this->options['1']['Customcss'] . '</style>';
	}
	$html .= "<body>";
	
	if (! empty ( $this->options['1']['custom_title'] ) && $this->options['1']['custom_title'] !="") {
		$pdf_title = $this->options['1']['custom_title'];
		echo "yess";
	} else {
		echo "noe";
		$pdf_title = $post->post_title;
	}
	 
	$html .= "<h1 style=\"text-align:center\">" . html_entity_decode ( $pdf_title, ENT_QUOTES ) . "</h1>";
	 
	if (!empty ( $this->options['1']['rtl_support'] )) {
		// set some language dependent data:
		$lg = Array();
		$lg['a_meta_charset'] = 'UTF-8';
		$lg['a_meta_dir'] = 'rtl';
		$lg['a_meta_language'] = 'fa';
		$lg['w_page'] = 'page';
			
		// set some language-dependent strings (optional)
		$pdf->setLanguageArray($lg);
		$pdf->setRTL(true);
	}
	if (isset ( $this->options['0']['author_name'] ) and ! $this->options['0']['author_name'] == '') {
		$author_id = $post->post_author;
		$author_meta_key = $this->options['0']['author_name'];
		$author = get_user_meta ( $author_id );
		$html .= '<p><strong>'.__('Author','wp-ultimate-pdf').' : </strong>' . $author [$author_meta_key] [0] . '</p>';
	}
				
	if (isset ( $this->options['0']['post_categ'] ))
	{
		$categories = get_the_category ( $post->ID );
		if ($categories) 
		{
			$html .= '<p><strong>'.__('Categories','wp-ultimate-pdf').' : </strong>' . $categories [0]->cat_name . '</p>';
		}
	}
 
	if (isset ( $this->options['0']['post_tags'] )) {
		$tags = get_the_tags ( $post->the_tags );
		if ($tags) {
			$html .= '<p><strong>'.__('Tagged as','wp-ultimate-pdf').' : </strong>';
			foreach ( $tags as $tag ) {
				$tag_link = get_tag_link ( $tag->term_id );
				$html .= '<a href="' . $tag_link . '">' . $tag->name . '</a>';
				if (next ( $tags )) {
					$html .= ', ';
				}
			}
			$html .= '</p>';
		}
	}
	 
	// Set some content to print
	 
	// Display featured image if set in config on page/post
	
	if (isset ( $this->options['0']['featured'] )) 
	{
		if (has_post_thumbnail ( $post->ID )) 
		{
			$html .= get_the_post_thumbnail ( $post->ID );
		}
	}
	
	$post_content = $post->post_content;
	if (empty ( $post->post_content )) 
	{
		$post_content = isset ( $this->options['4']['docEntryTpl'] ) ? $this->options['4']['docEntryTpl'] : '';
	}
	
	$html .= htmlspecialchars_decode ( htmlentities ( $post_content, ENT_NOQUOTES, 'UTF-8', false ), ENT_NOQUOTES );

	// Display date if set in config
	if (isset ( $this->options['0']['post_date'] )) {
		$newDate = date ( "d-m-Y", strtotime ( $post->post_date ) );
		$html .= '<p><strong>'.__('Date','wp-ultimate-pdf').': </strong>' . $newDate . '</p>';
	}
	$arr=get_post_custom($post_id);	
	unset($arr['_edit_last']);
	unset($arr['_edit_lock']);
	unset($arr['_thumbnail_id']);
	 
	$post_type_val=get_post_type($post_id);
	foreach ($arr as $key=>$val)
	{ 
	
		if( isset($this->options['6'][$post_type_val][$key]) && !empty($this->options['6'][$post_type_val][$key]) && ($this->options['6'][$post_type_val][$key]==1))
		{ 
		 
			if(isset($this->options['6'][$post_type_val][$key.'name']) && !empty($this->options ['6'][$post_type_val][$key.'name']) && ($this->options ['6'][$post_type_val][$key.'name'] != " ")  && ($this->options ['6'][$post_type_val][$key.'name'] != " ") )
			{  
				if( (null !==(get_post_meta($post_id,$key,true))) && (!is_array(get_post_meta($post_id,$key,true))) )
				{
					 
					if (function_exists('get_fields') && class_exists('acf')) {
		
						$field = get_fields($post_id);
	 					if( (array_key_exists($key, $field)) ){
	 						 continue;
	 					 
	 					}
						 
					}
					 
					$html .= '<p><strong>'.$this->options['6'][$post_type_val][$key.'name'].':'. '</strong>' . get_post_meta($post_id,$key,true) . '</p>';
				}
			}
			 
		}
		
	}

	/*
		Setting for displaying ACF field details only
	*/

	if (function_exists('get_fields') && class_exists('acf')) {
	
		$field = get_fields($post_id);

		if( $field )
		{
			foreach( $field as $field_name => $value )
			{	
				 
				if( !(( isset($this->options['6'][$post_type_val][$field_name]) && ( $this->options['6'][$post_type_val][$field_name]== 1 ) ) || ( isset($this->options['6'][$post_type_val]['_'.$field_name]) && ( $this->options['6'][$post_type_val]['_'.$field_name]== 1 ) )) ){
					continue;
				}
			 
				$field = get_field_object($field_name , $post_id );
			 
				//do not need to display password
				if(isset($field['type']) && ($field['type'] == "password")){
					continue;
				}
				//coding to display ACF field image
				if ( isset($field['type']) && ($field['type'] == 'image')) 
				{ 	
					
					if($field['save_format']=='id' && $field["label"] != '' && $field["value"] != "" ){
						$html .= '<p><strong>'.$field["label"] .':</strong><p>';
						$html .=  wp_get_attachment_image( $field["value"] , 'thumbnail');
					}elseif( $field['save_format']=='url' ){

						if( $field["label"] != '' && $field["value"] != ""){

							$html .= '<p><strong>'.$field["label"] .':</strong><a href=" '.$field["value"].'" >'.$field ['value'].'</a><p>';
							 
						}

					}elseif( $field['save_format'] == 'object' && $field["label"] != '' && $field["value"]['id'] != ""){
						 
						if( $field["label"] != '' && $field["value"]['id'] != ""){
						 
							$html .= $field["label"] .':';
							$html .=  wp_get_attachment_image( $field["value"]['id'] , 'thumbnail');
						}

					}
			 
					continue;

				}

				//coding for displaying Taxonomy fields

				if( isset($field['type'] ) && ($field['type'] == 'taxonomy')){
					 continue;
				}

				//coding for displaying Google map texual address
				if( isset($field['type'] ) && ($field['type'] == 'google_map')){
					if( $field["label"] != '' && $field["value"]['address'] != ""){

						$html .= '<p><strong>'.$field["label"].': </strong>' . $field["value"]['address'] . '</p> ';
					}
					continue;
				}

				//coding to display Date Picker
				if(isset($field['type']) && ($field['type']=='date_picker')){

					if( $field["label"] != '' && $field["value"] != "" && ($field['date_format'] == 'yymmdd')){
						//convert the date string in proper format
						$newstr = substr_replace($field["value"], ' : ', 4, 0);
						$newstr = substr_replace($newstr, ' : ',9 , 0);

						$html .= '<p><strong>'.$field["label"].': </strong>' . $newstr . '</p> ';
					}
					continue;
				}
				//coding to display radio button details
				if( !is_array($value) && ($field['type'] == "radio") && (isset($field['choices']))){
					if( $field["label"] != '' && $field["value"] != ""){
						$html .= '<p><strong>'.$field["label"].': </strong>' . $field["value"] . '</p> ';
					}
					continue;
				}

				//coding to display links for product and any post details
				if(isset($field) && ($field['type'] == 'page_link')){
					if(is_array($field['value']) && !empty($field['value'])){
						$html .= '<p><strong>'.$field["label"].': </strong></p> ';

						foreach ($field['value'] as $current_url_value) {

							if(isset($current_url_value) && ($current_url_value != '')){
								$html .=  '<p><a href="'.$current_url_value.'">'.$current_url_value.'</a><p>'  ;
							}
						}
					}else{
						if(($field["label"] !='') && ($field['value'] != "")){
							$html .= '<p><strong>'.$field["label"].': </strong></p> ';

							$html .=  '<p><a href="'.$field['value'].'">'.$field['value'].'</a><p>'  ;
						}
						 
					}
					continue;
				}

				//coding to handle post data Object
				if(isset($field) && ( $field['type'] == 'post_object' ) && (!empty($field['value'])) ){
					$acf_fetched_post_value=$field['value'];
					if( isset($acf_fetched_post_value) && !empty($acf_fetched_post_value) && is_array($acf_fetched_post_value)){
						$html .= '<p><strong>'.$field["label"].': </strong></p> ';
						foreach ($acf_fetched_post_value as $acf_fetched_one_post_value) {
							
							  if( (get_permalink( $acf_fetched_one_post_value->ID) != '') && ( get_the_title( $acf_fetched_one_post_value->ID) != '' ) ){
								
							 	 $html .= '<p>'.$acf_fetched_one_post_value->post_type.'  -->  '.'<a href="'.get_permalink( $acf_fetched_one_post_value->ID).'">'.get_the_title( $acf_fetched_one_post_value->ID).'</a></p>';
							 }
						}


					}else{
						 if( (get_permalink( $acf_fetched_post_value->ID) != '') && ( get_the_title( $acf_fetched_post_value->ID) != '' ) ){
							$html .= '<p><strong>'.$field["label"].': </strong></p> ';
						 	
						 	$html .= '<p>'.$acf_fetched_post_value->post_type.'  -->  '.'<a href="'.get_permalink( $acf_fetched_post_value->ID).'">'.get_the_title( $acf_fetched_post_value->ID).'</a></p>';
						 }
					}
					 
					continue;
				}

				//cofing to display text, textarea, email, (password need to block), any field returning simple string
				if(!is_array($value) && (!isset($field['choices'])) && !is_object($value)){
					if( $field["label"] != '' && $field["value"] != ""){
						$html .= '<p><strong>'.$field["label"].': </strong>' . $field["value"] . '</p> ';
					}

				}

				//cofing to display radio button, select and checkbox details
				if(is_array($field) && isset($field['choices']) && !empty($field['choices']) && ($field['type'] != "radio")){
					$html .= '<p><strong>'.$field["label"].': </strong></p> ';

					foreach ($field['value'] as $selected_value) {
						if(isset($field['choices'][ $selected_value ]) && ($field['choices'][ $selected_value ] != ''))

							$html .= '<p>' . $field['choices'][ $selected_value ] . '</p> ';

					}
				}


			}
		}
	}

	/*
	  ACF field details end
	*/
 
	$html .="</body>";
	$dom = new simple_html_dom ();
	$dom->load ( $html );
	
	foreach ( $dom->find ( 'img' ) as $e ) {
			
			// Start Image check by om
		$exurl = ''; // external streams
		$imsize = FALSE;
		$file = $e->src;
		// check if we are passing an image as file or string
		if ($file [0] === '@') {
			// image from string
			$imgdata = substr ( $file, 1 );
		} else { // image file
			if ($file {0} === '*') {
				// image as external stream
				$file = substr ( $file, 1 );
				$exurl = $file;
			}
			// check if is local file
			if (! @file_exists ( $file )) {
				// encode spaces on filename (file is probably an URL)
				$file = str_replace ( ' ', '%20', $file );
			}
			if (@file_exists ( $file )) {
				// get image dimensions
				$imsize = @getimagesize ( $file );
			}
			if ($imsize === FALSE) {
				$imgdata = TCPDF_STATIC::fileGetContents ( $file );
			}
		}
		if (isset ( $imgdata ) and ($imgdata !== FALSE) and (strpos ( $file, '__tcpdf_img' ) === FALSE)) {
			// copy image to cache
			$original_file = $file;
			$file = TCPDF_STATIC::getObjFilename ( 'img' );
			$fp = fopen ( $file, 'w' );
			fwrite ( $fp, $imgdata );
			fclose ( $fp );
			unset ( $imgdata );
			$imsize = @getimagesize ( $file );
			
		}
		if ($imsize === FALSE) {
			$e->outertext = '';
		} else {
			
			// End Image Check
			if (preg_match ( '/alignleft/i', $e->class )) {
				$imgalign = 'left';
			} elseif (preg_match ( '/alignright/i', $e->class )) {
				$imgalign = 'right';
			} elseif (preg_match ( '/aligncenter/i', $e->class )) {
				$imgalign = 'center';
				$htmlimgalign = 'middle';
			} else {
				$imgalign = 'none';
			}
			
			$e->class = null;
			$e->align = $imgalign;
			if (isset ( $htmlimgalign )) {
				$e->style = 'float:' . $htmlimgalign;
			} else {
				$e->style = 'float:' . $imgalign;
			}
			
			if (strtolower ( substr ( $e->src, - 4 ) ) == '.svg') {
				$e->src = null;
				$e->outertext = '<div style="text-align:' . $imgalign . '">[ SVG: ' . $e->alt . ' ]</div><br/>';
			} else {
				$e->outertext = '<div style="text-align:' . $imgalign . '">' . $e->outertext . '</div>';
			}
		}
	}
	$html = $dom->save ();
	$dom->clear ();
	
	$pdf->setFormDefaultProp ( array (
			'lineWidth' => 1,
			'borderStyle' => 'solid',
			'fillColor' => array (
					255,
					255,
					200 
			),
			'strokeColor' => array (
					255,
					128,
					128 
			) 
	) );
	// Print text using writeHTML
	$pdf->writeHTML ( $html, true, 0, true, 0 );
}
 
if(isset( $this->options['3']['add_watermark'] )) {
	$no_of_pages = $pdf->getNumPages ();
	for($i = 1; $i <= $no_of_pages; $i ++) {
		$pdf->setPage ( $i );
		
		// Get the page width/height
		$myPageWidth = $pdf->getPageWidth ();
		$myPageHeight = $pdf->getPageHeight ();
		
		// Find the middle of the page and adjust.
		$myX = ($myPageWidth / 2) - 75;
		$myY = ($myPageHeight / 2) + 25;
		
		// Set the transparency of the text to really light
		$pdf->SetAlpha ( 0.09 );
		
		$pdf->StartTransform ();
		$rotate_degr = isset ( $this->options['3']['rotate_water'] ) ? $this->options['3']['rotate_water'] : '45';
		$pdf->Rotate ( $rotate_degr, $myX, $myY );
		$water_font = isset ( $this->options['3']['water_font'] ) ? $this->options['3']['water_font'] : 'courier';
		$pdf->SetFont ( $water_font, "", 30 );
		$watermark_text = isset ( $this->options['3']['watermark_text'] ) ? $this->options['3']['watermark_text'] : '';
		$pdf->Text ( $myX, $myY, $watermark_text );
		$pdf->StopTransform ();
		
		// Reset the transparency to default
		$pdf->SetAlpha ( 1 );
	}
}
if (isset ( $this->options['3']['add_watermark_image'] )) {
	if (! empty ( $this->options['3']['background_img_url'] )) {
		$no_of_pages = $pdf->getNumPages ();
		for($i = 1; $i <= $no_of_pages; $i ++) {
			$pdf->setPage ( $i );
			
			$myPageWidth = $pdf->getPageWidth ();
			$myPageHeight = $pdf->getPageHeight ();
			$myX = ($myPageWidth / 2) - 50; // WaterMark Positioning
			$myY = ($myPageHeight / 2) - 40;
			$ImageT = isset ( $this->options['3']['water_img_t'] ) ? $this->options['3']['water_img_t'] : '';
			// Set the transparency of the text to really light
			$pdf->SetAlpha ( $ImageT );
			
			// Rotate 45 degrees and write the watermarking text
			$pdf->StartTransform ();
			$ImageW = isset ( $this->options['3']['water_img_h'] ) ? $this->options['3']['water_img_h'] : '';
			$ImageH = isset ( $this->options['3']['water_img_w'] ) ? $this->options['3']['water_img_w'] : '';
			
			$watermark_img = isset ( $this->options['3']['background_img_url'] ) ? $this->options['3']['background_img_url'] : '';
			$pdf->Image ( $watermark_img, $myX, $myY, $ImageW, $ImageH, '', '', '', true, 150 );
			
			$pdf->StopTransform ();
			
			$pdf->SetAlpha ( 1 );
		}
	}
}

if (! is_dir ( MWB_CACHE_DIR )) {
	mkdir ( MWB_CACHE_DIR, 0777, true );
}
$pdf->Output ( $filePath, 'F' );