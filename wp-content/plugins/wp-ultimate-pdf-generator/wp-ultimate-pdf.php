<?php
/**
 * Plugin Name: Wp Ultimate Pdf Generator
 * Plugin URI: http://makewebbetter.com
 * Description: Wp Ultimate Pdf Generator plugin is a pdf generator for wordpress default and custom posts to pdf. Blog readers can export post into pdf and can print or send it to registered email address.
 * Version: 1.0.2
 * Text Domain: wp-ultimate-pdf
 * Author: makewebbetter <webmaster@makewebbetter.com>
 * Author URI: http://makewebbetter.com
 * Domain Path: /languages
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

if ( ! defined( 'ABSPATH' ) ) exit; 

define ( 'MWBPDF_GEN_PREFIX', 'mwbpdf_gen' );

global $blog_id;

if( $_SERVER['REQUEST_SCHEME'] == "https" )
{
	define ( 'MWBPDF_GEN_URL',  str_replace( "http://", "https://", WP_PLUGIN_URL . '/wp-ultimate-pdf-generator' ) );
}
else 
{
	define ( 'MWBPDF_GEN_URL', WP_PLUGIN_URL . '/wp-ultimate-pdf-generator' );
}
define ( 'MWB_CACHE_DIR', WP_CONTENT_DIR . '/uploads/wp-ultimate-pdf/' . $blog_id );
define ( 'MWBPDF_GEN_PATH', WP_PLUGIN_DIR . '/wp-ultimate-pdf-generator' );
 
if (function_exists ( 'is_multisite' ) && is_multisite ()) 
{
	include_once (ABSPATH . 'wp-admin/includes/plugin.php');
}

include_once MWBPDF_GEN_PATH.'/Content/mbw-pdf-gen-log.php';

if (! class_exists ( 'mwbpdf' )) 
{
	class mwbpdf 
	{
		static $instance;
		public $log;	
		public static function set_screen( $status, $option, $value ) {
			return $value;
		}
		private $options = array ();
		/**
		 * enqueues all the functionality of plugin
		 * @name mwbpdf  
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		public function __construct()  
		{	
			if(!session_id()){
				session_start();
				 
			}
			global $pagenow;
			$this->options = get_option ( MWBPDF_GEN_PREFIX );
			 
		
			if (is_admin ()) 
			{
				
				add_action('admin_menu',array($this,'ptp_main_menu'));
				add_action( 'admin_menu', [ $this, 'ptp_option_log' ] );
				add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
				add_action ( 'admin_init', array ( $this, 'ptp_on_admin_Init' ) );
				add_filter ( "plugin_action_links_" . plugin_basename ( __FILE__ ), array ( $this, 'ptp_action_links_handler' ) );
				register_activation_hook ( plugin_basename ( __FILE__ ), array ( $this, 'ptp_set_default_on_activate' ) );
				add_filter ( 'page_row_actions', array ($this,'ptp_add_export_to_pdf_option_hier' ), 10, 2 );
				add_filter ( 'post_row_actions', array ( $this, 'ptp_add_export_to_pdf_option_nhier' ), 10, 2 );
				add_action ( 'load-edit.php', array ( $this, 'ptp_custom_bulk_action' ) );
				add_action ( 'admin_notices', array ( $this, 'ptp_custom_bulk_admin_notices' ) );
			} 
			else 
			{

				add_action ( 'wp', array ( $this, 'ptp_generate_post_to_pdf' ) );
				add_filter ( 'the_content', array (	$this,	'ptp_add_pdf_link' ) );
			}
			add_action( 'admin_init', array($this,'deactivate_plugin_conditional') );
			add_filter ( 'cron_schedules', array ( $this, 'ptp_pdfscheduleduration' ) );
			add_action ( 'wp', array ( $this, 'ptp_cronstarter_activation' ) );
			register_deactivation_hook ( __FILE__, array ( $this, 'ptp_cronstarter_deactivate' ) );
			add_action ( 'schedulecacheupdate', array (	$this,	'ptp_update_cache' ) );
			add_action ( 'wp_enqueue_scripts', array ( $this,	'ptp_mwbpdf_gen_theme_scripts' ) );
			add_action ( 'wp_ajax_nopriv_postajax_exportandmail', array ($this, 'postajax_exportandmail_handle' ) );
			add_action ( 'wp_ajax_postajax_exportandmail', array (	$this,	'postajax_exportandmail_handle'	) );
			register_activation_hook ( __FILE__, array ( $this, 'ptp_export_install' ) );
			add_action ( 'wp_head', array (	$this,	'ptp_hook_div_for_guest' ) );
			add_action ( 'init', array ( $this, 'ptp_init_theme_method' ) );
		
			if($pagenow!='post-new.php')
			{
				add_action( 'add_meta_boxes',array($this,'post_meta_box'),10,2 );
			}
			add_action('wp_ajax_savemeta',array($this,'savemeta_function'));
			add_action ( 'wp_ajax_nopriv_savemeta', array ($this,'savemeta_function' ));
			if(isset($this->options['short']))
			{
				remove_filter('the_content',array($this,'ptp_add_pdf_link'));
				add_shortcode('MWB_PDF_GEN',array($this,'ptp_pdf_icon'));
				add_action( 'edit_form_after_title', array($this,'custom_html_post'),10,1);
			}else{
				add_shortcode('MWB_PDF_GEN',array($this,'ptp_pdf_icon_no_shortcode'));

			}
			
			/**
			 * ADD FOLLOWING ACTION OR FITER INTO YOU MAIN CLASS FILE
			 */
			
			add_action ( 'plugins_loaded', array ( $this, 'load_textdomain' ) );
			//add_filter ( 'plugin_row_meta', array (	$this, 'ptp_custom_plugin_row_meta'	), 10, 2 );	
			add_action ( 'wp_ajax_add_custom_font', array (	$this,'ptp_add_custom_font' ) );
			add_action('wp_ajax_remove',array($this,'remove'));
			add_action ( 'wp_ajax_nopriv_remove', array ($this,'remove' ) );
			add_action('wp_ajax_savemetafunction',array($this,'savemetafunction'));
			add_action ( 'wp_ajax_nopriv_savemetafunction', array ($this,'savemetafunction' ) );  
			add_action('wp_ajax_unsavemeta',array($this,'unsave_meta'));
			add_action ( 'wp_ajax_nopriv_unsavemeta', array ($this,'unsave_meta' ) );
			 
			add_action( 'admin_notices',  array ( $this, MWBPDF_GEN_PREFIX.'admin_notice_success') );
			register_deactivation_hook(__FILE__, array ( $this, MWBPDF_GEN_PREFIX.'admin_notices'));
			 
			add_action('admin_init',array($this,'mwbpdf_gen_preview'));
			add_action('wp_ajax_removelog',array($this,'mwbpdf_gen_remove_log'));
			add_action ( 'wp_ajax_nopriv_removelog', array ($this,'mwbpdf_gen_remove_log' ) );
		}
		
		/**
		 * checking if plugin  installed
		 * @name deactivate_plugin_conditional
		 * @author makewebbetter
		 */
		function deactivate_plugin_conditional()
		{
			if ( is_plugin_active('wp-ultimate-pdf/wp-ultimate-pdf.php') )
			{
				deactivate_plugins('wp-ultimate-pdf/wp-ultimate-pdf.php');
			}
		}
		
		/**
		 * generates main menu of settings
		 * @name ptp_main_menu
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */ 
		function ptp_main_menu()
		{
			add_menu_page( __( 'Wp Ultimate Pdf Generator', 'wp-ultimate-pdf-generator' ), __( 'Wp Ultimate Pdf Generator', 'wp-ultimate-pdf' ), 'manage_options', 'wp-ultimate-pdf-generator', array($this,'ptp_option_page_handler'), plugins_url( 'wp-ultimate-pdf-generator/asset/images/pdf3.png' ));
		}
		
		/**
		 * generates a menu for pdf logs
		 * @name ptp_option_log
		 * @author makewebbetter <webmaster@makewebbetter.com>	
		 * @link http://makewebbetter.com
		 */
		
		function ptp_option_log()
		{
			$hook=add_submenu_page ( 'wp-ultimate-pdf-generator','Wp Ultimate Pdf Generator log', __( 'Wp Ultimate Pdf Generator Log' ,'wp-ultimate-pdf' ), 'manage_options', 'advancedpdflog', array ($this,'ptp_option_log_handler'));
			add_action( "load-$hook", [ $this, 'screen_option' ] );
		}
		
		/**
		 * Handles the submenu
		 * @name ptp_option_log_handler
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com 
		 */

		function ptp_option_log_handler()
		{
			?>
			<div class="wrap">
				<h2><?php _e('Log Of PDF downloads','wp-ultimate-pdf');?></h2>
				<div id="poststuff">
					<div id="post-body" class="metabox-holder columns-3">
						<div id="post-body-content">
							<div class="meta-box-sortables ui-sortable">
								<form method="post">
									<?php
									$this->log->prepare_items();
									$this->log->display(); ?>
								</form>
							</div>
						</div>
					</div>
					<br class="clear">
				</div>
			</div>
			<?php 	
		}
		
		/**
		 * adds screen option for Log
		 * @name screen_option
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		
		public function screen_option() 
		{
			$option = 'per_page';
			$args   = [
			'label'   => 'Records',
			'default' => 5,
			'option'  => 'records_per_page'
					];
			add_screen_option( $option, $args );
			$this->log = new mbw_log_list();
		}

		/**
		 * removes log
		 * @name mwbpdf_gen_remove_log
		 * @author makewebbetter
		 * @link http://makewebbetter.com
		 */
		function mwbpdf_gen_remove_log()
		{
			$id= sanitize_text_field($_POST['id']);
			global $wpdb;
			$export=$wpdb->get_results("Delete from ". $wpdb->prefix . "ExporttoPDFRecord where id='$id'",OBJECT);
			_e('Record deleted successfully','wp-ultimate-pdf'); 
			die;
		}
		public function get_post_preview_body( $html ) {
			$post_content = '<p>'.__("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostroris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.","wp-ultimate-pdf").'</p><br><br>'.__("Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Nam libero tempore, cum soluta nobis est eligendi opti Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? ","wp-ultimate-pdf").'<br><br><br>'.__("At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.","wp-ultimate-pdf").'
								<h2>'.__("This Is An H2 Tag",'wp-ultimate-pdf').'</h2>
								<p>'.__('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','wp-ultimate-pdf').' </p>
								<ul style="padding-left: 1em; text-indent: -1em;">
								<li>'.__('First Item','wp-ultimate-pdf').'</li>
								<li>'.__('Second Item','wp-ultimate-pdf').'</li>
								<li>'.__('Third Item','wp-ultimate-pdf').'</li>
								</ul>
								<ol style="padding-left: 1em; text-indent: -1em;">
								<li>'.__('First Item','wp-ultimate-pdf').'</li>
								<li>'.__('Second Item','wp-ultimate-pdf').'</li>
								<li>'.__('Third Item','wp-ultimate-pdf').'</li>
								</ol>';
				
				$html .= htmlspecialchars_decode ( htmlentities ( $post_content, ENT_NOQUOTES, 'UTF-8', false ), ENT_NOQUOTES );
				
			return $html;
		}
		public function get_product_preview_body( $html, $productprevieew,$pdf_title ) {

			
			$html1 ='<img width="250px" height="250px" alt="" src="'.MWBPDF_GEN_URL.'/asset/images/product-image.jpg">';
			$html2 = '<h1 style="text-align:center">' . html_entity_decode ( $pdf_title, ENT_QUOTES ) . '</h1>';
			$html4 = '<p>'.__("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostroris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod  et dolore magna aliqua. ","wp-ultimate-pdf").'</p>';

			$post_content = '<p>'.__("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostroris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod  et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.","wp-ultimate-pdf").'</p><br><br>'.__("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ","wp-ultimate-pdf").'<br><br>'.__("At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. ","wp-ultimate-pdf");
			$html3="";
			if($productprevieew == 'tmplt1') {
				$html3 = $html2.$html1;
			}
			elseif($productprevieew == 'tmplt2') {
				$html3 = $html1."</td><td>".$html2.$html4;
			}
			elseif($productprevieew == 'tmplt3') {
				$html3 = $html2.$html4."</td><td>".$html1;
			}
			$html .= '<table cellpadding="2" cellspacing="2"><tr><td>';
			
			$html .= $html3;
			$html .= "</td></tr></table>";
			$html .= "<h2>".__("Description",'wp-ultimate-pdf')."</h2>";
			$html .= htmlspecialchars_decode ( htmlentities ( $post_content, ENT_NOQUOTES, 'UTF-8', false ), ENT_NOQUOTES );

			return $html;
		}
		/**
		 * shows preview of settings
		 * @name mwbpdf_gen_preview
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function mwbpdf_gen_preview()
		{
 	 		
 	 		$post_product_preview=isset($_GET['mwbpdf_preview'])?sanitize_text_field($_GET['mwbpdf_preview']):'no';
 	 		if( $post_product_preview == 'post' || $post_product_preview == 'product') {
 	 			$setting = isset($_GET['mwbpdf_gensetting'])?sanitize_text_field($_GET['mwbpdf_gensetting']):'';
 	 			$postprevieew=isset($_GET['mwbpdf_posttmplt'])?sanitize_text_field($_GET['mwbpdf_posttmplt']):'tmplt1';
 	 			$productprevieew=isset($_GET['mwbpdf_producttmplt'])?sanitize_text_field($_GET['mwbpdf_producttmplt']):'tmplt1';
 	 			$setname=get_option('ptp_settings',array());
 	 			if (! class_exists ( 'TCPDF' )) {
					require_once MWBPDF_GEN_PATH . '/tcpdf_min/tcpdf.php';
					 
				}
				if (! class_exists ( 'pdfheader' )) {

					require_once MWBPDF_GEN_PATH . '/Content/pdf_headercontent.php';
				 
				}
				if (! class_exists ( 'simple_html_dom_node' )) {
					require_once MWBPDF_GEN_PATH . '/Htmlstructure/Htmlstructure.php';
				 
				}
				if (isset($setname[$setting]['0']['mwb_file_name']) && !empty($setname[$setting]['0']['mwb_file_name']) && ($setname[$setting]['0']['mwb_file_name'] == 'post_name')) {
					$filePath = MWB_CACHE_DIR . '/' . 'demo' . '.pdf';
					 
				} else {
					$filePath = MWB_CACHE_DIR . '/' . 99999 . '.pdf';
					 
				}
				// new PDF document
				if (isset ( $setname[$setting]['1'] ['page_size'] )) {
					$pagesize = ($setname[$setting]['1'] ['page_size']);
					
				} else {
					$pagesize = PDF_PAGE_FORMAT;
				}
				
				if (isset ( $setname[$setting]['1'] ['unitmeasure'] )) {
					$unit = ($setname[$setting]['1'] ['unitmeasure']);
					
				} else {
					$unit = PDF_UNIT;
				}
				if (isset ( $setname[$setting]['1'] ['page_orientation'] )) {
					$orientation = ($setname[$setting]['1'] ['page_orientation']);
				} else {
					$orientation = PDF_PAGE_ORIENTATION;
				}
					
				$pdf = new CUSTOMPDF ( $orientation, $unit, $pagesize, true, 'UTF-8', false );
			 
				// information about doc
				$pdf->SetCreator ( 'Post to PDF plugin by makewebbetter with ' . PDF_CREATOR );
				$pdf->SetAuthor ( get_bloginfo ( 'name' ) );
				
				if (! empty ( $setname[$setting]['1']['custom_title'] )) {
					$pdf_title = $setname[$setting]['1'] ['custom_title'];
				} else {
					$pdf_title = __('Default Title', 'wp-ultimate-pdf' );
				}
			 		
				// logo width calculation
				 
				if (isset ( $setname[$setting]['2'] ['page_header'] ) and ($setname[$setting]['2'] ['page_header']) != 'None' and !empty ( $setname[$setting]['2'] ['logo_img_url'] )) {
				
					if ($setname[$setting]['2'] ['page_header'] == "upload-image") {
						$logoImage_url = $setname[$setting]['2'] ['logo_img_url'];
					}
					$infologo = getimagesize ( $logoImage_url );
					if (isset ( $setname[$setting]['2'] ['image_factor'] )) {
						$logo_width = ( int ) (($setname[$setting]['2'] ['image_factor'] * $infologo [0]) / $infologo [1]);
					} else {
						$logo_width = ( int ) ((12 * $infologo [0]) / $infologo [1]);
					}
					// for PHP 5.4 or below set default header data
					if (version_compare ( phpversion (), '5.4.0', '<' )) {
						$pdf->SetHeaderData ( $logoImage_url, $logo_width, html_entity_decode ( get_bloginfo ( 'name' ), ENT_COMPAT | ENT_QUOTES ), html_entity_decode ( get_bloginfo ( 'description' ) . "\n" . home_url (), ENT_COMPAT | ENT_QUOTES ), $header_text_color, $header_line_color );
					} else {
						$pdf->SetHeaderData ( $logoImage_url, $logo_width, html_entity_decode ( get_bloginfo ( 'name' ), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES ), html_entity_decode ( get_bloginfo ( 'description' ) . "\n" . home_url (), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES )  );
					}
				}
				if (isset ( $setname[$setting]['2'] ['page_header'] ) and ($setname[$setting]['2'] ['page_header']) == 'None') {
					$pdf->setPrintHeader(false);
				}
				
				// set header and footer fonts
			
				if (isset($setname[$setting]['2']['header_font_size']) && !empty($setname[$setting]['2']['header_font_size']) &&  ( ($setname[$setting]['2']['header_font_size']) > 0 )) {
					$header_font_size = $setname[$setting]['2']['header_font_size'];
				} else {
					$header_font_size = 10;
				}
				if (isset($setname[$setting]['footer_font_size']) && !empty($setname[$setting]['footer_font_size']) && (($setname[$setting]['footer_font_size']) > 0)) {
					$footer_font_size = $setname[$setting]['footer_font_size'];
				} else {
					$footer_font_size = 10;
				}
				if(isset($setname[$setting]['2']['header_font_pdf'])){
					$pdf->setHeaderFont ( array (
					$setname[$setting]['2']['header_font_pdf'],
							'',
					$header_font_size
					) );
				}
				if(isset($setname[$setting]['2']['header_font_pdf'])){
					$pdf->setHeaderFont ( array (
					$setname[$setting]['2']['header_font_pdf'],
					'',
					$header_font_size
					) );
				}
				 	
				$pdf->SetDefaultMonospacedFont ( PDF_FONT_MONOSPACED );
				if (isset($setname[$setting]['1'] ['margin_left']) && !empty($setname[$setting]['1'] ['margin_left'])) {
					$pdf->SetLeftMargin ( $setname[$setting]['1'] ['margin_left'] );
				} else {
					$pdf->SetLeftMargin ( PDF_MARGIN_LEFT );
				}
					
				if (isset($setname[$setting]['1']['margin_right'] )) {
					$pdf->SetRightMargin ( $setname[$setting]['1']['margin_right']);
				} else {
					$pdf->SetRightMargin ( PDF_MARGIN_RIGHT );
				}
					
				if (isset($setname[$setting]['1'] ['margin_top'] )) {
					$pdf->SetTopMargin ( $setname[$setting]['1'] ['margin_top'] );
				} else {
					$pdf->SetTopMargin ( PDF_MARGIN_TOP );
				}
				if ((isset($setname[$setting]['2'] ['logomTop']) )) {
					$pdf->SetHeaderMargin ( $setname[$setting]['2'] ['logomTop'] );
				} else {
					$pdf->SetHeaderMargin ( PDF_MARGIN_HEADER );
				}
					 
				if (isset($setname[$setting] ['footer_font_margin'] )) {
					$pdf->SetFooterMargin ( $setname[$setting]['footer_font_margin'] );
					// set auto page breaks
					$pdf->SetAutoPageBreak ( TRUE,  $setname[$setting] ['footer_font_margin']  );
				} else {
					$pdf->SetFooterMargin ( '10' );
					// set auto page breaks
					$pdf->SetAutoPageBreak ( TRUE, '10' );
				}
					
				// set image scale factor
					
				if ( isset($setname[$setting]['1']['image_scale']) && !empty($setname[$setting]['1']['image_scale']) && $setname[$setting]['1']['image_scale'] > 0) {
					$pdf->setImageScale ( $setname[$setting]['1']['image_scale'] );
				} else {
					$pdf->setImageScale ( PDF_IMAGE_SCALE_RATIO );
				}
					
				// set default font subsetting mode
				$pdf->setFontSubsetting ( true );
				if(isset($setname[$setting]['1']['content_font_pdf'])){
					$pdf->SetFont ( $setname[$setting]['1']['content_font_pdf'], '', $setname[$setting]['1']['content_font_size'], '', true );

				}
				 
				if (! empty ( $setname[$setting]['5']['bullet_img_url'] )) {
					
					$temp = $setname[$setting]['5']['bullet_img_url'];
					$temp = end ( ( explode ( '/', $temp ) ) );
					$temp = end ( ( explode ( '.', $temp ) ) );
					$listsymbol = 'img|' . $temp . '|' . $setname[$setting]['5']['custom_image_width'] . '|' . $setname[$setting]['5']['custom_image_height'] . '|' . $setname[$setting]['5']['bullet_img_url'];
					$pdf->setLIsymbol ( $listsymbol );
				}
				// Add a page
				// This method has several options, check the source code documentation for more information.
					
				if (isset($setname[$setting]['4']['fontStretching']) && !empty($setname[$setting]['4']['fontStretching'])) {
					$pdf->setFontStretching($setname[$setting]['4']['fontStretching']);
				}
				if (isset($setname[$setting]['4']['fontSpacig']) && !empty($setname[$setting]['4']['fontSpacig'])) {
					$pdf->setFontSpacing($setname[$setting]['4']['fontSpacig']);
				}else{
					$pdf->setFontSpacing('0');

				}
				$page_format = array();
				if (isset($setname[$setting]['4']['set_rotation']) && !empty($setname[$setting]['4']['set_rotation'])) {
					$page_format['Rotate'] = $setname[$setting]['4']['set_rotation'];
				} else {
					$page_format['Rotate'] = 0;
				}

				if(isset($setname[$setting]['1']['page_orientation'])){
					$pdf->AddPage($setname[$setting]['1']['page_orientation'], $page_format, false, false);

				}
				$html = '';
				if (isset ( $setname[$setting]['1']['CustomCSS_option'] ))
				{
					$html = '<style>' . $setname[$setting]['1']['Customcss'] . '</style>';
				}
				$html .= "<body>";
				
				if (isset ( $setname[$setting]['0']['author_name'] ) and !$setname[$setting]['0']['author_name'] == '') 
				{	
					$html .= '<p><strong>'.__("Author","wp-ultimate-pdf").': </strong>' . __("Author","wp-ultimate-pdf"). '</p>';
				}
				
				if (isset ($setname[$setting]['0']['post_categ'] ))
				{
					$html .= '<p><strong>'.__("Category","wp-ultimate-pdf").': </strong>' . __("Category","wp-ultimate-pdf") . '</p>';
				}
				if($post_product_preview == 'post') {
					$html .= '<h1 style="text-align:center">' . html_entity_decode ( $pdf_title, ENT_QUOTES ) . '</h1>';
					$html .='<img width="700px" height="250px" alt="" src="'.MWBPDF_GEN_URL.'/asset/images/wp-ultimate-pdf-banner-blog-768x256.jpg">';
					if( $postprevieew == 'tmplt1') {
						$html = $this->get_post_preview_body($html);
					}
					elseif($postprevieew == 'tmplt2') {
						$pdf->writeHTML ( $html, true, 0, true, 0 );
						$pdf->setEqualColumns(2, 84);
						$html = $this->get_post_preview_body( '' );
					}
					elseif($postprevieew == 'tmplt3') {
						$pdf->writeHTML ( $html, true, 0, true, 0 );
						$pdf->setEqualColumns(3, 57);
						$html = $this->get_post_preview_body('');
					}
				}
				elseif($post_product_preview == 'product') {
					$html = $this->get_product_preview_body(  $html, $productprevieew , $pdf_title);
				}
				$html .="</body>";
				
				$dom = new simple_html_dom ();
				$dom->load ( $html );
					
				foreach ($dom->find ('img') as $e)
				{
					// Start Image check by om
					$exurl = ''; // external streams
					$imsize = FALSE;
					$file = $e->src;
					// check if we are passing an image as file or string
					if ($file [0] === '@')
					{
						// image from string
						$imgdata = substr ( $file, 1 );
					}
					else
					{ // image file
						if ($file {0} === '*')
						{
							// image as external stream
							$file = substr ( $file, 1 );
							$exurl = $file;
						}
						// check if is local file
						if (! @file_exists ( $file ))
						{
							// encode spaces on filename (file is probably an URL)
							$file = str_replace ( ' ', '%20', $file );
						}
						if (@file_exists ( $file ))
						{
							// get image dimensions
							$imsize = @getimagesize ( $file );
						}
						if($imsize === FALSE)
						{
							$imgdata = TCPDF_STATIC::fileGetContents ( $file );
						}
					}
					if (isset ( $imgdata ) and ($imgdata !== FALSE) and (strpos ( $file, '__tcpdf_img' ) === FALSE))
					{
						// check Image size
						$imsize = @getimagesize ( $file );
					}
					if ($imsize === FALSE)
					{
						$e->outertext = '';
					}
					else
					{
						// End Image Check
						if (preg_match ( '/alignleft/i', $e->class ))
						{
							$imgalign = 'left';
						}
						elseif (preg_match ( '/alignright/i', $e->class ))
						{
							$imgalign = 'right';
						}
						elseif (preg_match ( '/aligncenter/i', $e->class ))
						{
							$imgalign = 'center';
							$htmlimgalign = 'middle';
						}
						else
						{
							$imgalign = 'none';
						}
							
						$e->class = null;
						$e->align = $imgalign;
						if (isset ( $htmlimgalign ))
						{
							$e->style = 'float:' . $htmlimgalign;
						}
						else
						{
							$e->style = 'float:' . $imgalign;
						}
							
						if (strtolower ( substr ( $e->src, - 4 ) ) == '.svg')
						{
							$e->src = null;
							$e->outertext = '<div style="text-align:' . $imgalign . '">[ SVG: ' . $e->alt . ' ]</div><br/>';
						}
						else
						{
							$e->outertext = '<div style="text-align:' . $imgalign . '">' . $e->outertext . '</div>';
						}
					}
				}
				$html = $dom->save ();

				$dom->clear ();

				$pdf->setFormDefaultProp ( array (
						'lineWidth' => 1,
						'borderStyle' => 'solid',
						'fillColor' => array (
								255,
								255,
								200
						),
						'strokeColor' => array (
								255,
								128,
								128
						)
				) );

				// Print text using writeHTML


				$pdf->writeHTML ($html, true, 0, true, 0 );
				 
				 
				if (isset ( $setname[$setting]['3']['add_watermark'] ))
				{
					$no_of_pages = $pdf->getNumPages ();
					for($i = 1; $i <= $no_of_pages; $i ++)
					{
						$pdf->setPage ( $i );
						// Get the page width/height
						$myPageWidth = $pdf->getPageWidth ();
						$myPageHeight = $pdf->getPageHeight ();
							
						// Find the middle of the page and adjust.
						$myX = ($myPageWidth / 2) - 75;
						$myY = ($myPageHeight / 2) + 25;
							
						// Set the transparency of the text to really light
						$pdf->SetAlpha ( 0.09 );
							
						// Rotate 45 degrees and write the watermarking text
						$pdf->StartTransform ();
						$rotate_degr = isset ( $setname[$setting]['3']['rotate_water'] ) ? $setname[$setting]['3']['rotate_water'] : '45';
						$pdf->Rotate ( $rotate_degr, $myX, $myY );
						$water_font = isset ( $setname[$setting]['3']['water_font'] ) ? $setname[$setting]['3']['water_font'] : 'courier';
										$pdf->SetFont ( $water_font, "", 30 );
						$watermark_text = isset ( $setname[$setting]['3']['watermark_text'] ) ? $setname[$setting]['3']['watermark_text'] : '';
										$pdf->Text ( $myX, $myY, $watermark_text );
						$pdf->StopTransform ();
						$pdf->SetAlpha ( 1 );
					}
				}
				if (isset ( $setname[$setting]['3']['add_watermark_image'] ))
				{
					if (! empty ( $setname[$setting]['3']['background_img_url'] ))
					{
						$no_of_pages = $pdf->getNumPages ();
						for($i = 1; $i <= $no_of_pages; $i ++)
						{
							$pdf->setPage ( $i );
							$myPageWidth = $pdf->getPageWidth ();
							$myPageHeight = $pdf->getPageHeight ();
							$myX = ($myPageWidth / 2) - 50; // WaterMark Positioning
							$myY = ($myPageHeight / 2) - 40;
							$ImageT = isset ( $setname[$setting]['3']['water_img_t'] ) ? $setname[$setting]['3']['water_img_t'] : '';
							// Set the transparency of the text to really light
							$pdf->SetAlpha ( $ImageT );
					
							// Rotate 45 degrees and write the watermarking text
							$pdf->StartTransform ();
							$ImageW = isset ( $setname[$setting]['3']['water_img_h'] ) ? $setname[$setting]['3']['water_img_h'] : '';
							$ImageH = isset ( $setname[$setting]['3']['water_img_w'] ) ? $setname[$setting]['3']['water_img_w'] : '';
					
							$watermark_img = isset ( $setname[$setting]['3']['background_img_url'] ) ? $setname[$setting]['3']['background_img_url'] : '';
							$pdf->Image ( $watermark_img, $myX, $myY, $ImageW, $ImageH, '', '', '', true, 150 );
							$pdf->StopTransform ();
							$pdf->SetAlpha ( 1 );
						}
					}
				}

				$pdf->Output ( $filePath, 'I');
				exit;
 	 		}

			$setting=isset($_GET['mwbpdf_gensetting'])?sanitize_text_field($_GET['mwbpdf_gensetting']):'';
			$setname=get_option('ptp_settings',array());

			 
			if(array_key_exists($setting,$setname))
			{ 
			 
				if (! class_exists ( 'TCPDF' )) {
					require_once MWBPDF_GEN_PATH . '/tcpdf_min/tcpdf.php';
					 
				}
				if (! class_exists ( 'pdfheader' )) {

					require_once MWBPDF_GEN_PATH . '/Content/pdf_headercontent.php';
				 
				}
				if (! class_exists ( 'simple_html_dom_node' )) {
					require_once MWBPDF_GEN_PATH . '/Htmlstructure/Htmlstructure.php';
				 
				}
				if (isset($setname[$setting]['0']['mwb_file_name']) && !empty($setname[$setting]['0']['mwb_file_name']) && ($setname[$setting]['0']['mwb_file_name'] == 'post_name')) {
					$filePath = MWB_CACHE_DIR . '/' . 'demo' . '.pdf';
					 
				} else {
					$filePath = MWB_CACHE_DIR . '/' . 99999 . '.pdf';
					 
				}
				// new PDF document
				if (isset ( $setname[$setting]['1'] ['page_size'] )) {
					$pagesize = ($setname[$setting]['1'] ['page_size']);
					
				} else {
					$pagesize = PDF_PAGE_FORMAT;
				}
				
				if (isset ( $setname[$setting]['1'] ['unitmeasure'] )) {
					$unit = ($setname[$setting]['1'] ['unitmeasure']);
					
				} else {
					$unit = PDF_UNIT;
				}
				if (isset ( $setname[$setting]['1'] ['page_orientation'] )) {
					$orientation = ($setname[$setting]['1'] ['page_orientation']);
				} else {
					$orientation = PDF_PAGE_ORIENTATION;
				}
					
				$pdf = new CUSTOMPDF ( $orientation, $unit, $pagesize, true, 'UTF-8', false );
			 
				// information about doc
				$pdf->SetCreator ( 'Post to PDF plugin by makewebbetter with ' . PDF_CREATOR );
				$pdf->SetAuthor ( get_bloginfo ( 'name' ) );
				
				if (! empty ( $setname[$setting]['1']['custom_title'] )) {
					$pdf_title = $setname[$setting]['1'] ['custom_title'];
				} else {
					$pdf_title = __('Default Post', 'wp-ultimate-pdf' );
				}
			 		
				// logo width calculation
				 
				if (isset ( $setname[$setting]['2'] ['page_header'] ) and ($setname[$setting]['2'] ['page_header']) != 'None' and !empty ( $setname[$setting]['2'] ['logo_img_url'] )) {
				
					if ($setname[$setting]['2'] ['page_header'] == "upload-image") {
						$logoImage_url = $setname[$setting]['2'] ['logo_img_url'];
					}
					$infologo = getimagesize ( $logoImage_url );
					if (isset ( $setname[$setting]['2'] ['image_factor'] )) {
						$logo_width = ( int ) (($setname[$setting]['2'] ['image_factor'] * $infologo [0]) / $infologo [1]);
					} else {
						$logo_width = ( int ) ((12 * $infologo [0]) / $infologo [1]);
					}
					// for PHP 5.4 or below set default header data
					if (version_compare ( phpversion (), '5.4.0', '<' )) {
						$pdf->SetHeaderData ( $logoImage_url, $logo_width, html_entity_decode ( get_bloginfo ( 'name' ), ENT_COMPAT | ENT_QUOTES ), html_entity_decode ( get_bloginfo ( 'description' ) . "\n" . home_url (), ENT_COMPAT | ENT_QUOTES ), $header_text_color, $header_line_color );
					} else {
						$pdf->SetHeaderData ( $logoImage_url, $logo_width, html_entity_decode ( get_bloginfo ( 'name' ), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES ), html_entity_decode ( get_bloginfo ( 'description' ) . "\n" . home_url (), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES )  );
					}
				}
				if (isset ( $setname[$setting]['2'] ['page_header'] ) and ($setname[$setting]['2'] ['page_header']) == 'None') {
					$pdf->setPrintHeader(false);
				}
				
				// set header and footer fonts
			
				if (isset($setname[$setting]['2']['header_font_size']) && !empty($setname[$setting]['2']['header_font_size']) &&  ( ($setname[$setting]['2']['header_font_size']) > 0 )) {
					$header_font_size = $setname[$setting]['2']['header_font_size'];
				} else {
					$header_font_size = 10;
				}
				if (isset($setname[$setting]['footer_font_size']) && !empty($setname[$setting]['footer_font_size']) && (($setname[$setting]['footer_font_size']) > 0)) {
					$footer_font_size = $setname[$setting]['footer_font_size'];
				} else {
					$footer_font_size = 10;
				}
				if(isset($setname[$setting]['2']['header_font_pdf'])){
					$pdf->setHeaderFont ( array (
					$setname[$setting]['2']['header_font_pdf'],
							'',
					$header_font_size
					) );
				}
				if(isset($setname[$setting]['2']['header_font_pdf'])){
					$pdf->setHeaderFont ( array (
					$setname[$setting]['2']['header_font_pdf'],
					'',
					$header_font_size
					) );
				}
				 	
				$pdf->SetDefaultMonospacedFont ( PDF_FONT_MONOSPACED );
				if (isset($setname[$setting]['1'] ['margin_left']) && !empty($setname[$setting]['1'] ['margin_left'])) {
					$pdf->SetLeftMargin ( $setname[$setting]['1'] ['margin_left'] );
				} else {
					$pdf->SetLeftMargin ( PDF_MARGIN_LEFT );
				}
					
				if (isset($setname[$setting]['1']['margin_right'] )) {
					$pdf->SetRightMargin ( $setname[$setting]['1']['margin_right']);
				} else {
					$pdf->SetRightMargin ( PDF_MARGIN_RIGHT );
				}
					
				if (isset($setname[$setting]['1'] ['margin_top'] )) {
					$pdf->SetTopMargin ( $setname[$setting]['1'] ['margin_top'] );
				} else {
					$pdf->SetTopMargin ( PDF_MARGIN_TOP );
				}
				if ((isset($setname[$setting]['2'] ['logomTop']) )) {
					$pdf->SetHeaderMargin ( $setname[$setting]['2'] ['logomTop'] );
				} else {
					$pdf->SetHeaderMargin ( PDF_MARGIN_HEADER );
				}
					 
				if (isset($setname[$setting] ['footer_font_margin'] )) {
					$pdf->SetFooterMargin ( $setname[$setting]['footer_font_margin'] );
					// set auto page breaks
					$pdf->SetAutoPageBreak ( TRUE,  $setname[$setting] ['footer_font_margin']  );
				} else {
					$pdf->SetFooterMargin ( '10' );
					// set auto page breaks
					$pdf->SetAutoPageBreak ( TRUE, '10' );
				}
					
				// set image scale factor
					
				if ( isset($setname[$setting]['1']['image_scale']) && !empty($setname[$setting]['1']['image_scale']) && $setname[$setting]['1']['image_scale'] > 0) {
					$pdf->setImageScale ( $setname[$setting]['1']['image_scale'] );
				} else {
					$pdf->setImageScale ( PDF_IMAGE_SCALE_RATIO );
				}
					
				// set default font subsetting mode
				$pdf->setFontSubsetting ( true );
				if(isset($setname[$setting]['1']['content_font_pdf'])){
					$pdf->SetFont ( $setname[$setting]['1']['content_font_pdf'], '', $setname[$setting]['1']['content_font_size'], '', true );

				}
				 
				if (! empty ( $setname[$setting]['5']['bullet_img_url'] )) {
					
					$temp = $setname[$setting]['5']['bullet_img_url'];
					$temp = end ( ( explode ( '/', $temp ) ) );
					$temp = end ( ( explode ( '.', $temp ) ) );
					$listsymbol = 'img|' . $temp . '|' . $setname[$setting]['5']['custom_image_width'] . '|' . $setname[$setting]['5']['custom_image_height'] . '|' . $setname[$setting]['5']['bullet_img_url'];
					$pdf->setLIsymbol ( $listsymbol );
				}
				// Add a page
				// This method has several options, check the source code documentation for more information.
					
				if (isset($setname[$setting]['4']['fontStretching']) && !empty($setname[$setting]['4']['fontStretching'])) {
					$pdf->setFontStretching($setname[$setting]['4']['fontStretching']);
				}
				if (isset($setname[$setting]['4']['fontSpacig']) && !empty($setname[$setting]['4']['fontSpacig'])) {
					$pdf->setFontSpacing($setname[$setting]['4']['fontSpacig']);
				}else{
					$pdf->setFontSpacing('0');

				}
				$page_format = array();
				if (isset($setname[$setting]['4']['set_rotation']) && !empty($setname[$setting]['4']['set_rotation'])) {
					$page_format['Rotate'] = $setname[$setting]['4']['set_rotation'];
				} else {
					$page_format['Rotate'] = 0;
				}

				if(isset($setname[$setting]['1']['page_orientation'])){
					$pdf->AddPage($setname[$setting]['1']['page_orientation'], $page_format, false, false);

				}
				$html = '';
				if (isset ( $setname[$setting]['1']['CustomCSS_option'] ))
				{
					$html = '<style>' . $setname[$setting]['1']['Customcss'] . '</style>';
				}
				$html .= "<body>";
				
				if (isset ( $setname[$setting]['0']['author_name'] ) and !$setname[$setting]['0']['author_name'] == '') 
				{	
					$html .= '<p><strong>'.__("Author","wp-ultimate-pdf").': </strong>' . __("Author","wp-ultimate-pdf"). '</p>';
				}
				
				if (isset ($setname[$setting]['0']['post_categ'] ))
				{
					$html .= '<p><strong>'.__("Category","wp-ultimate-pdf").': </strong>' . __("Category","wp-ultimate-pdf") . '</p>';
				}
				
				if (isset ( $setname[$setting]['0']['post_tags'] )) 
				{
					$html .= '<p><strong>'.__("Tagged as","wp-ultimate-pdf").': </strong>';
					$html .= '<a href=" ">' . 'tag1,tag2' . '</a>';
					$html .= '</p>';	
				}
				// Display date if set in config
				if (isset ( $setname[$setting]['0']['post_date'] )) {
					
					$html .= '<p><strong>'.__("Date","wp-ultimate-pdf").': </strong>' . '21-05-2016' . '</p>';
				}
				
				// Set some content to print
				$html .= '<h1>' . html_entity_decode ( $pdf_title, ENT_QUOTES ) . '</h1>';
					
				// Display featured image if set in config on page/post
					
				if (isset ( $setname[$setting]['0']['featured'] ))
				{
					$html.='<img src="'.MWBPDF_GEN_URL.'/asset/images/logo.png">';
				}
					
				$post_content = '<p>'.__("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.","wp-ultimate-pdf").'</p>
								<h2>'.__("This Is An H2 Tag",'wp-ultimate-pdf').'</h2>
								<p>'.__('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','wp-ultimate-pdf').' </p>
								<p><img width="700px" height="250px" alt="" src="'.MWBPDF_GEN_URL.'/asset/images/wp-ultimate-pdf-banner-blog-768x256.jpg"></p>
								<ul style="padding-left: 1em; text-indent: -1em;">
								<li>'.__('First Item','wp-ultimate-pdf').'</li>
								<li>'.__('Second Item','wp-ultimate-pdf').'</li>
								<li>'.__('Third Item','wp-ultimate-pdf').'</li>
								</ul>
								<ol style="padding-left: 1em; text-indent: -1em;">
								<li>'.__('First Item','wp-ultimate-pdf').'</li>
								<li>'.__('Second Item','wp-ultimate-pdf').'</li>
								<li>'.__('Third Item','wp-ultimate-pdf').'</li>
								</ol>';
				
				$html .= htmlspecialchars_decode ( htmlentities ( $post_content, ENT_NOQUOTES, 'UTF-8', false ), ENT_NOQUOTES );
				
				$html .="</body>";
				
				$dom = new simple_html_dom ();
				$dom->load ( $html );
					
				foreach ($dom->find ('img') as $e)
				{
					// Start Image check by om
					$exurl = ''; // external streams
					$imsize = FALSE;
					$file = $e->src;
					// check if we are passing an image as file or string
					if ($file [0] === '@')
					{
						// image from string
						$imgdata = substr ( $file, 1 );
					}
					else
					{ // image file
						if ($file {0} === '*')
						{
							// image as external stream
							$file = substr ( $file, 1 );
							$exurl = $file;
						}
						// check if is local file
						if (! @file_exists ( $file ))
						{
							// encode spaces on filename (file is probably an URL)
							$file = str_replace ( ' ', '%20', $file );
						}
						if (@file_exists ( $file ))
						{
							// get image dimensions
							$imsize = @getimagesize ( $file );
						}
						if($imsize === FALSE)
						{
							$imgdata = TCPDF_STATIC::fileGetContents ( $file );
						}
					}
					if (isset ( $imgdata ) and ($imgdata !== FALSE) and (strpos ( $file, '__tcpdf_img' ) === FALSE))
					{
						// check Image size
						$imsize = @getimagesize ( $file );
					}
					if ($imsize === FALSE)
					{
						$e->outertext = '';
					}
					else
					{
						// End Image Check
						if (preg_match ( '/alignleft/i', $e->class ))
						{
							$imgalign = 'left';
						}
						elseif (preg_match ( '/alignright/i', $e->class ))
						{
							$imgalign = 'right';
						}
						elseif (preg_match ( '/aligncenter/i', $e->class ))
						{
							$imgalign = 'center';
							$htmlimgalign = 'middle';
						}
						else
						{
							$imgalign = 'none';
						}
							
						$e->class = null;
						$e->align = $imgalign;
						if (isset ( $htmlimgalign ))
						{
							$e->style = 'float:' . $htmlimgalign;
						}
						else
						{
							$e->style = 'float:' . $imgalign;
						}
							
						if (strtolower ( substr ( $e->src, - 4 ) ) == '.svg')
						{
							$e->src = null;
							$e->outertext = '<div style="text-align:' . $imgalign . '">[ SVG: ' . $e->alt . ' ]</div><br/>';
						}
						else
						{
							$e->outertext = '<div style="text-align:' . $imgalign . '">' . $e->outertext . '</div>';
						}
					}
				}
				$html = $dom->save ();

				$dom->clear ();

				$pdf->setFormDefaultProp ( array (
						'lineWidth' => 1,
						'borderStyle' => 'solid',
						'fillColor' => array (
								255,
								255,
								200
						),
						'strokeColor' => array (
								255,
								128,
								128
						)
				) );

				// Print text using writeHTML


				$pdf->writeHTML ($html, true, 0, true, 0 );
				 
				 
				if (isset ( $setname[$setting]['3']['add_watermark'] ))
				{
					$no_of_pages = $pdf->getNumPages ();
					for($i = 1; $i <= $no_of_pages; $i ++)
					{
						$pdf->setPage ( $i );
						// Get the page width/height
						$myPageWidth = $pdf->getPageWidth ();
						$myPageHeight = $pdf->getPageHeight ();
							
						// Find the middle of the page and adjust.
						$myX = ($myPageWidth / 2) - 75;
						$myY = ($myPageHeight / 2) + 25;
							
						// Set the transparency of the text to really light
						$pdf->SetAlpha ( 0.09 );
							
						// Rotate 45 degrees and write the watermarking text
						$pdf->StartTransform ();
						$rotate_degr = isset ( $setname[$setting]['3']['rotate_water'] ) ? $setname[$setting]['3']['rotate_water'] : '45';
						$pdf->Rotate ( $rotate_degr, $myX, $myY );
						$water_font = isset ( $setname[$setting]['3']['water_font'] ) ? $setname[$setting]['3']['water_font'] : 'courier';
										$pdf->SetFont ( $water_font, "", 30 );
						$watermark_text = isset ( $setname[$setting]['3']['watermark_text'] ) ? $setname[$setting]['3']['watermark_text'] : '';
										$pdf->Text ( $myX, $myY, $watermark_text );
						$pdf->StopTransform ();
						$pdf->SetAlpha ( 1 );
					}
				}
				if (isset ( $setname[$setting]['3']['add_watermark_image'] ))
				{
					if (! empty ( $setname[$setting]['3']['background_img_url'] ))
					{
						$no_of_pages = $pdf->getNumPages ();
						for($i = 1; $i <= $no_of_pages; $i ++)
						{
							$pdf->setPage ( $i );
							$myPageWidth = $pdf->getPageWidth ();
							$myPageHeight = $pdf->getPageHeight ();
							$myX = ($myPageWidth / 2) - 50; // WaterMark Positioning
							$myY = ($myPageHeight / 2) - 40;
							$ImageT = isset ( $setname[$setting]['3']['water_img_t'] ) ? $setname[$setting]['3']['water_img_t'] : '';
							// Set the transparency of the text to really light
							$pdf->SetAlpha ( $ImageT );
					
							// Rotate 45 degrees and write the watermarking text
							$pdf->StartTransform ();
							$ImageW = isset ( $setname[$setting]['3']['water_img_h'] ) ? $setname[$setting]['3']['water_img_h'] : '';
							$ImageH = isset ( $setname[$setting]['3']['water_img_w'] ) ? $setname[$setting]['3']['water_img_w'] : '';
					
							$watermark_img = isset ( $setname[$setting]['3']['background_img_url'] ) ? $setname[$setting]['3']['background_img_url'] : '';
							$pdf->Image ( $watermark_img, $myX, $myY, $ImageW, $ImageH, '', '', '', true, 150 );
							$pdf->StopTransform ();
							$pdf->SetAlpha ( 1 );
						}
					}
				}

				$pdf->Output ( $filePath, 'I');
				exit;
			 }

			if($setting=='Default Settings')
			{
				$file = MWBPDF_GEN_PATH.'/asset/previewpdf/default-post.pdf';
				header("Content-Type: application/force-download");
				header("Content-Type: application/pdf");
				header("Content-Disposition:inline; filename=".urlencode($file));
				header("Content-Description: File Transfer");
				header("Content-Length: " . filesize($file));
				header('Accept-Ranges: bytes');
				readfile($file);
				exit;
			}
		}
	  
		 /**
		  * Show notification (Feed)
		  * @name mwbpdf_genadmin_notice_success()
		  * @author makewebbetter <webmaster@makewebbetter.com>
		  * @link: http://www.makewebbetter.com/
		  */
		 
		 function mwbpdf_genadmin_notice_success()
		 {
		 	$mwb_notice = get_option(MWBPDF_GEN_PREFIX.'feed','hide');
		 	if($mwb_notice != "hide")
		 	{
		 		$admin_name = '';
		 		$admin_email = get_option( 'admin_email', null );
		 		$admin_details = get_user_by('email', $admin_email);
		 
		 		if(isset($admin_details->data))
		 		{
		 			if(isset($admin_details->data->display_name))
		 			{
		 				$admin_name = $admin_details->data->display_name;
		 			}
		 		}
		 
		 		global $wp_version;
		 		$domain_info = array();
		 		$mwb_plugin_info = array();
		 		$mwb_info_response = array();
		 		$mwb_installed_plugin = '';
		 		$mwb_all_plugins = get_option('mwb_all_plugins', false);
		 		$mwb_all_plugins = json_decode($mwb_all_plugins,true);
		 
		 		if(isset($mwb_all_plugins))
		 		{
		 			if(is_array($mwb_all_plugins))
		 			{
		 				foreach($mwb_all_plugins as $key=>$mwb_plugin)
		 				{
		 					if(isset($mwb_plugin['Author']))
		 					{
		 						if($mwb_plugin['AuthorURI'] == 'http://makewebbetter.com')
		 						{
		 							$name = $mwb_plugin['Name'];
		 							$version = $mwb_plugin['Version'];
		 							$mwb_plugin_arr[$name]['release_version'] = $version;
		 							$mwb_plugin_arr[$name]['parent_product_name'] = null;
		 							$mwb_installed_plugin .= $name.':'.$version.'~';
		 						}
		 					}
		 				}
		 			}
		 		}
		 			
		 		$mwb_installed_plugin = rtrim($mwb_installed_plugin,'~');
		 			
		 		$domain_info['domain_name'] = $_SERVER['HTTP_HOST'];
		 		$domain_info['plateform'] = 'wordpress';
		 		$domain_info['edition'] = '';
		 		$domain_info['version'] = $wp_version;
		 		$domain_info['php_version'] = phpversion();
		 		$domain_info['feed_types'] = "INSTALLED_UPDATE,UPDATE_RELEASE,NEW_RELEASE,PROMO,INFO";
		 		$domain_info['admin_name'] = $admin_name;
		 		$domain_info['admin_email'] = $admin_email;
		 		$domain_info['installed_extensions_by_makewebbetter'] = $mwb_installed_plugin;
		 		$domain_info['installed_modules'] = $mwb_plugin_arr;
		 
		 		$curl = curl_init();
		 		curl_setopt_array($curl, array(
		 		CURLOPT_RETURNTRANSFER => 1,
		 		CURLOPT_URL => 'http://makewebbetter.com/licensing/log/post',
		 		CURLOPT_USERAGENT => 'makewebbetter',
		 		CURLOPT_POST => 1,
		 		CURLOPT_POSTFIELDS => urldecode(http_build_query($domain_info))
		 		));
		 
		 
		 		$res = curl_exec($curl);
		 		curl_close($curl);
		 
		 		libxml_use_internal_errors(true);
		 		$xml=simplexml_load_string($res);
		 
		 		if(isset($xml->channel))
		 		{
		 			if(isset($xml->channel->item ))
		 			{
		 				?>
 				     	<div class="updated woocommerce-message wc-connect">
 				     	<?php 
 						$mwb_updates = $xml->channel->item;
 				        foreach($mwb_updates as $mwb_update)
 				        {
 						?>
 							<p><a href="<?php echo $mwb_update->link?>"><?php echo $mwb_update->title?></a></p>
 						<?php 
 						}
 						?>
 					 	</div>
	 				<?php
	 				}		
		 		}	
		 	}
		 }
		 
		 function mwbpdf_genadmin_notices()
		 {
		 	delete_option('mwb_feed'); // the option name : "mwb_feed" is same for all extension
		 	delete_option(MWBPDF_GEN_PREFIX.'feed');  // the option name : "mwb_sadc_feed" must be unique according to extension
		 }
		 
		/**
		 * adds meta box over post/product/page edit form
		 * @name post_meta_box
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		
		function post_meta_box($posttype,$post)
		{
			 
			$this->options = get_option ( MWBPDF_GEN_PREFIX );
			if( isset($this->options['settingname']) && ( $this->options['settingname']!='') )
			{
    			add_meta_box( 'Meta_Fields', __( 'Meta Fields For Post To PDF', 'wp-ultimate-pdf' ), array($this,'meta_field_func'),$posttype,'advanced');			
			}
		}
		
		/**
		 * generates meta field html over post/page/product edit form
		 * @name meta_field_func
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		
		function meta_field_func()
		{
			global $post;
			$this->options = get_option ( MWBPDF_GEN_PREFIX );
			$post_custom=get_post_custom($post->ID);
			 
			unset($post_custom['_edit_last']);
			unset($post_custom['_edit_lock']);
			unset($post_custom['_thumbnail_id']);
			foreach ($post_custom as $key=>$val)
			{
	 	 
				?>
				<p class='test'>   
					<p class='metakey'>     	  				
				   		 <input name="<?=MWBPDF_GEN_PREFIX.'['.$post->post_type.']'.'['.$key.']'?>" <?php if( isset($this->options['6'][$post->post_type][$key]) && ( $this->options['6'][$post->post_type][$key]==1) ) : echo "Checked ='checked'"; endif; ?> readonly type="checkbox" class='chk' id='<?php echo $key;?>'/><label class='metalabel'><?php echo $key;?></label>
           			 	<!--  <input type="text" name="<?=MWBPDF_GEN_PREFIX.'['.$post->post_type.']'.'['.$key.'name]'?>" value="<?=( isset ( $this->options['6'][$post->post_type][$key.'name'] ) ? $this->options['6'][$post->post_type][$key.'name'] : ' ' );?>" id='<?php echo $key.'name'?>' class='txt'/>	              					
             --> 	</p>
				<?php 			
			}
			?>
			<br/>
			<!-- <input type="button" name='savemeta' value="savemeta" class='savemeta button button-primary button-large'/>	 -->
		<?php		
		}
		
		/**
		 * handles ajax request to save text field value of meta field
		 * @name savemetafunction
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function savemetafunction()
		{
			 
			$opt = get_option('ptp_settings');
			$settingname=$this->options['settingname'];
			if(array_key_exists($settingname,$opt))
			{
				
				$opt[$settingname]['settingname']=$settingname;
				$opt[$settingname]['6'][$_POST['txtid']]=sanitize_text_field($_POST['txtval']);
				 
				update_option(MWBPDF_GEN_PREFIX,array_replace($this->options,$opt[$settingname]));
				
			}
		}
		
		/**
		 * handles ajax request to save check box value
		 * @name savemeta_function
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		
		function savemeta_function()
		{ 
			$opt = get_option('ptp_settings');
			$settingname=$this->options['settingname'];
			if(array_key_exists($settingname,$opt))
			{
				$opt[$settingname]['settingname']=$settingname;
				$opt[$settingname]['6'][$_POST['chkid']]=sanitize_text_field($_POST['chkval']);
				update_option(MWBPDF_GEN_PREFIX,array_replace($this->options,$opt[$settingname]));
				
			} 
		} 
		
		/**
		 * handles ajax request for unchecked values
		 * @name unsave_meta
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		
		function unsave_meta()
		{
		    $opt = get_option('ptp_settings');
			$settingname=$this->options['settingname'];
			if(array_key_exists($settingname,$opt))
			{
				$opt[$settingname]['settingname']=$settingname;
				$opt[$settingname]['6'][$_POST['chkid']]='';
				update_option(MWBPDF_GEN_PREFIX,array_replace($this->options,$opt[$settingname]));
			} 
			 
		}
		
		/**
		 * removes settings on delete setting click
		 * @name remove
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function remove()
		{
			$arr=get_option('ptp_settings');
			if(array_key_exists($_POST['data'],$arr))
			{
				unset($arr[$_POST['data']]);
				update_option('ptp_settings',$arr);
				
				$default = array();
				
				$default = array();
				$default['front_end'] = '1';
				$default['show'] = 'guestuser';
				$default['availability'] = 'public';
				$default['admin_panel'] = 1;
				$default['0']['featured'] = 1;
				$default['0']['cache_updation_sch'] = 'none';
				$default['0']['mwb_file_name'] = 'post_name';
				$default['0']['author_name'] = '';
				$default['0']['content_position'] = 'left';
				$default['0']['content_placement'] = 'after';
				$default['0']['link_button'] = 'default';
				$default['0']['custon_link_url'] = '';
				$default['1']['image_scale'] = 1.25;
				$default['1']['margin_top'] = 27;
				$default['1']['margin_left'] = 15;
				$default['1']['margin_right'] = 15;
				$default['1']['page_size'] = 'LETTER';
				$default['1']['page_orientation'] = 'P';
				$default['1']['unitmeasure'] = 'mm';
				$default['1']['content_font_pdf'] = 'helvetica';
				$default['1']['content_font_size'] = 12;
				$default['1']['custom_title'] = '';
				$default['1']['Customcss'] = '';
				$default['2']['page_header'] = 'upload-image';
				$default['2']['logo_img_url'] = MWBPDF_GEN_URL.'/asset/images/default-logo.png';
				$default['2']['image_factor'] = 15;
				$default['2']['logomTop'] = 10;
				$default['2']['header_font_pdf'] = 'helvetica';
				$default['2']['header_font_size'] = 10;
				$default['custom_footer_option'] = 1;
				$default['custom_footer'] = '';
				$default['footer_font_pdf'] = 'helvetica';
				$default['footer_font_size'] = 10;
				$default['footer_cell_width'] = 0;
				$default['footer_min_height'] = 0;
				$default['footer_lcornerX'] = 15;
				$default['footer_font_lcornerY'] = 290;
				$default['footer_font_margin'] = 10;
				$default['footer_cell_fill'] = 1;
				$default['footer_align'] = '';
				$default['footer_cell_auto_padding'] = 1;
				$default['3']['add_watermark'] = 1;
				$default['3']['rotate_water'] = 45;
				$default['3']['watermark_text'] = 'Default Watermark';
				$default['3']['water_img_h'] = '';
				$default['3']['water_img_w'] = '';
				$default['3']['water_img_t'] = 0.5;
				$default['3']['background_img_url'] = '';
				$default['5']['bullet_img_url'] = '';
				$default['5']['custom_image_height'] = 2;
				$default['5']['custom_image_width'] = 3;
				$default['4']['custom_font_for_body'] = '';
				$default['4']['set_rotation'] = 0;
				$default['4']['fontStretching'] = 100;
				$default['4']['fontSpacig'] = 0;
				$default['6']['post']['post'] = 1;
				$default['6']['page']['page'] = 1;
				$default['6']['product']['product'] = 1;
				$default['6']['mwb_wcswr_from_datename'] = '';
				$default['6']['mwb_wcswr_to_datename'] = '';
				$default['6']['mwb_wcswr_from_timename'] = '';
				$default['6']['mwb_wcswr_to_timename'] = '';
				$default['6']['_product_attributesname'] = '';
				$default['6']['_stock_statusname'] = '';
				$default['6']['_downloadablename'] = '';
				$default['6']['_virtualname'] = '';
				$default['6']['_min_variation_pricename'] = '';
				$default['6']['_max_variation_pricename'] = '';
				$default['6']['_min_price_variation_idname'] = '';
				$default['6']['_max_price_variation_idname'] = '';
				$default['6']['_min_variation_regular_pricename'] = '';
				$default['6']['_max_variation_regular_pricename'] = '';
				$default['6']['_min_regular_price_variation_idname'] = '';
				$default['6']['_max_regular_price_variation_idname'] = '';
				$default['6']['_min_variation_sale_pricename'] = '';
				$default['6']['_max_variation_sale_pricename'] = '';
				$default['6']['_min_sale_price_variation_idname'] = '';
				$default['6']['_max_sale_price_variation_idname'] = '';
				$default['6']['_pricename'] = '';
				$default['6']['_default_attributesname'] = '';
				$default['6']['_visibilityname'] = '';
				$default['6']['total_salesname'] = '';
				$default['6']['_purchase_notename'] = '';
				$default['6']['_featuredname'] = '';
				$default['6']['_weightname'] = '';
				$default['6']['_lengthname'] = '';
				$default['6']['_widthname'] = '';
				$default['6']['_heightname'] = '';
				$default['6']['_skuname'] = '';
				$default['6']['_regular_pricename'] = '';
				$default['6']['_sale_pricename'] = '';
				$default['6']['_sale_price_dates_fromname'] = '';
				$default['6']['_sale_price_dates_toname'] = '';
				$default['6']['_sold_individuallyname'] = '';
				$default['6']['_manage_stockname'] = '';
				$default['6']['_backordersname'] = '';
				$default['6']['_stockname'] = '';
				$default['6']['_upsell_idsname'] = '';
				$default['6']['_crosssell_idsname'] = '';
				$default['6']['_product_versionname'] = '';
				$default['6']['_product_image_galleryname'] = '';
				$default['6']['_wc_rating_countname'] = '';
				$default['6']['_wc_review_countname'] = '';
				$default['6']['_wc_average_ratingname'] = '';
				$default['6']['_themex_hiddenname'] = '';
				$default['6']['_themex_ratingname'] = '';
				$default['6']['_themex_salesname'] = '';
				$default['6']['_themex_admirersname'] = '';
				$default['6']['_themex_ratename'] = '';
				$default['6']['_themex_aboutname'] = '';
				$default['6']['_themex_policyname'] = '';

				update_option ( MWBPDF_GEN_PREFIX, $default );
			}
			
		}
		
		/**
		 * adds a shortcode textbox over posts add/edit section
		 * @name custom_html_post
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 */
		function custom_html_post($post)
		{
			?>
			<p class="btnfl"><label for="shortcode"><b><?php _e('Shortcode For PDF','wp-ultimate-pdf');?></b></label>
			<input type="text" class="shortcode" value='[MWB_PDF_GEN]'/></p>
			<?php 	
			return $post;
		}
		
		/**
		 * Add custom plugin row meta
		 * @name ptp_custom_plugin_row_meta
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param array $links
		 * @param string $file
		 */
		function ptp_custom_plugin_row_meta( $links, $file ) 
		{
			if ( strpos( $file, 'wp-ultimate-pdf.php' ) !== false ) 
			{
				$new_links = array(
						''
				);
				$links = array_replace( $links, $new_links );
			}
			return $links;
		}
		/**
		 * loads text domain
		 * @name load_textdomain
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function load_textdomain() 
		{
			$domain = "wp-ultimate-pdf";
			$locale = apply_filters ( 'plugin_locale', get_locale (), $domain );
			load_textdomain ( $domain, MWBPDF_GEN_PATH . '/languages/' . $domain . '-' . $locale . '.mo' );
			load_plugin_textdomain ( 'wp-ultimate-pdf', false, plugin_basename ( dirname ( __FILE__ ) ) . '/languages' );
		}
	
		/**
		 * Step 2: handle the custom Bulk Action
		 *
		 * Based on the post http://wordpress.stackexchange.com/questions/29822/custom-bulk-action
		 * @name ptp_custom_bulk_action
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_custom_bulk_action() 
		{
			global $typenow;
			$post_type = $typenow;
			if (isset ( $this->options['6'][$post_type][$post_type] )) {
				// get the action
				$wp_list_table = _get_list_table ( 'WP_Posts_List_Table' ); // depending on your resource type this could be WP_Users_List_Table, WP_Comments_List_Table, etc
				$action = $wp_list_table->current_action ();
				$allowed_actions = array (
						"export" 
				);
				if (! in_array ( $action, $allowed_actions ))
					return;
					// security check
				check_admin_referer ( 'bulk-posts' );
				// make sure ids are submitted. depending on the resource type, this may be 'media' or 'ids'
				if (isset ( $_REQUEST ['post'] )) {
					$post_ids = array_map ( 'intval', $_REQUEST ['post'] );
				}
				if (empty ( $post_ids ))
					return;
					// this is based on wp-admin/edit.php
				$sendback = remove_query_arg ( array (
						'exported',
						'untrashed',
						'deleted',
						'ids' 
				), wp_get_referer () );
				if (! $sendback)
					$sendback = admin_url ( "edit.php?post_type=$post_type" );
				$pagenum = $wp_list_table->get_pagenum ();
				$sendback = add_query_arg ( 'paged', $pagenum, $sendback );
				switch ($action) {
					case 'export' :
						$exported = 0;
						foreach ( $post_ids as $post_id ) {
							// add posts to in an array to convert into pdf
							$exported ++;
						}
						if ($exported)
							$this->ptp_generate_pdf_bulk_posts ( $post_ids );
						
						$sendback = add_query_arg ( array (
								'exported' => $exported,
								'ids' => join ( ',', $post_ids ) 
						), $sendback );
						break;
					
					default :
						return;
				}
				$sendback = remove_query_arg ( array (
						'action',
						'action2',
						'tags_input',
						'post_author',
						'comment_status',
						'ping_status',
						'_status',
						'post',
						'bulk_edit',
						'post_view' 
				), $sendback );
				
				wp_redirect ( $sendback );
				exit ();
			}
		}
		
		/**
		 * Step 3: display an admin notice on the Posts page after exporting
		 * @name ptp_custom_bulk_admin_notices
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_custom_bulk_admin_notices() 
		{
			global $post_type, $pagenow;
			
			if ($pagenow == 'edit.php' && isset ( $this->options['6'][$post_type][$post_type] ) && isset ( $_REQUEST ['exported'] ) && ( int ) $_REQUEST ['exported']) 
			{
				$message = sprintf ( _n ( 'Post exported.', '%s posts exported.', $_REQUEST ['exported'] ), number_format_i18n ( $_REQUEST ['exported'] ) );
				echo "<div class=\"updated\"><p>{$message}</p></div>";
			}
		}
		
		/**
		 * Sends postid to function ptp_generate_pdf_file_bulk and generates output
		 * @name  ptp_generate_pdf_bulk_posts
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http:// makewebbetter.com
		 * @param array $post_ids
		 *an array of postIDs of which to export into pdf
		 */
		function ptp_generate_pdf_bulk_posts($post_ids) 
		{
			$filePath = MWB_CACHE_DIR . '/' . 'bulk' . '.pdf';
			$fileMime = 'pdf';
			$fileName = 'bulk.pdf';
			$this->ptp_generate_pdf_file_bulk ( $post_ids );
			$output = $this->output_Post_to_pdf_file ( $filePath, $fileName, $fileMime );
		}
		
		/**
		 * Generates pdf of an array of posts
		 * @name ptp_generate_pdf_file_bulk
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param array $post_ids        	
		 */
		function ptp_generate_pdf_file_bulk($post_ids) 
		{
			 
			// include only once class library
			if (! class_exists ( 'TCPDF' )) 
			{
				require_once MWBPDF_GEN_PATH . '/tcpdf_min/tcpdf.php';
			}
			if (! class_exists ( 'pdfheader' )) 
			{
				require_once MWBPDF_GEN_PATH . '/Content/pdf_headercontent.php';
			}
			if (! class_exists ( 'simple_html_dom_node' )) 
			{
				require_once MWBPDF_GEN_PATH . '/Htmlstructure/Htmlstructure.php';
			}
			$filePath = MWB_CACHE_DIR . '/' . 'bulk' . '.pdf';
			require_once MWBPDF_GEN_PATH . '/export/adminbulk.php';
			 
		}
		
		/**
		 * Adds exportpdf link for post type page and custom types
		 * @name ptp_add_export_to_pdf_option_hier
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param array $actions        	
		 * @param int $post        	
		 * @return array|string
		 */
		function ptp_add_export_to_pdf_option_hier($actions, $post) 
		{

			if (isset ( $this->options ['admin_panel'] ))
			{
				
				if (! isset ( $this->options['6'][$post->post_type][$post->post_type] ))
					return $actions;
				$url = esc_url ( add_query_arg ( 'format', 'pdf', get_permalink ( $post->ID ) ) );
				$actions ['exportpdf'] = '<a href="' . $url . '" title="' . esc_attr(__ ( 'Export to pdf', 'wp-ultimate-pdf' ) ). '">' . __ ( 'Exportpdf', 'wp-ultimate-pdf' ) . '</a>';
				
				
			}
			
			return $actions;
		}
		
		/**
		 * adds exportpdf link for post type post
		 * @name ptp_add_export_to_pdf_option_nhier
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param array $actions        	
		 * @param int $post        	
		 * @return array|string
		 */
		function ptp_add_export_to_pdf_option_nhier($actions, $post) 
		{
			if (isset ( $this->options ['admin_panel'] )) 
			{
				if (! isset ( $this->options['6'][$post->post_type][$post->post_type] ))
					return $actions;
				$url = esc_url ( add_query_arg ( 'format', 'pdf', get_permalink ( $post->ID ) ) );
				$actions ['exportpdf'] = '<a href="' . $url . '" title="' . esc_attr(__ ( 'Export to pdf', 'wp-ultimate-pdf' ) ). '">' . __ ( 'Exportpdf', 'wp-ultimate-pdf' ) . '</a>';
			}
			return $actions;
		}
		
		function ptp_init_theme_method() 
		{
			add_thickbox ();
		}
		
		/**
		 * Adds an div for popup to guest users
		 * @name ptp_hook_div_for_guest
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_hook_div_for_guest() 
		{
			$html = "<div style='float:left;padding:20px 20px 20px 20px;'><h4>".__('Enter your email Address','wp-ultimate-pdf')."</h4>";
			$html .= '<input type="text" style="margin-top:10px" name="useremailID" id="useremailID"><input type="hidden" id="emailpostID">';
			$html .= "<input id='guest_email' style='margin-top:10px' class='button-primary' type='submit' name='email_submit' value='submit'></div>";
			$output = '<div id="examplePopup1" style="display:none;">' . $html . '</div>';
			echo $output;
		}
		
		/**
		 * enqueue scripts for ajax requests
		 * @name ptp_mwbpdf_gen_theme_scripts
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_mwbpdf_gen_theme_scripts() 
		{
			wp_enqueue_style ( 'mwbpdf_genfrontend', MWBPDF_GEN_URL . '/asset/css/front_end.css' );
			
			wp_enqueue_script ( 'ajaxsave', MWBPDF_GEN_URL.'/asset/js/ajaxsave.js', array (
					'jquery' 
			) );
			wp_localize_script ( 'ajaxsave', 'postajaxsave', array (
					'ajax_url' => admin_url ( 'admin-ajax.php' ),
					'baseUrl'  => MWBPDF_GEN_URL,

			) );
		}
		
		/**
		 * enqueue custom script for admin option pages
		 * @name ptp_enqueue_custom_script
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_enqueue_custom_script() 
		{
			wp_enqueue_style ( 'mwbpdf_genadminstyle', MWBPDF_GEN_URL . '/asset/css/admin.css', false, '1.0', 'all' );
			$post_export = null;
			if (isset ( $this->options ['admin_panel'] )) {
				global $post_type;
				if (isset ( $this->options['6'][$post_type][$post_type] )) {
					$post_export = true;
				}
			}
				
			wp_enqueue_script ( 'bulk', MWBPDF_GEN_URL . '/asset/js/bulk.js' );
			$translation_array = array(
					'txt' => __( 'Export', 'wp-ultimate-pdf' ),
					'alert_remove_txt' => __( 'Are you sure you want to delete this setting', 'wp-ultimate-pdf' ),
					'post_export' => $post_export,
					'reset_nonce' => wp_create_nonce( 'wp-reset-nonce' ),
					'alert_saved_apply' => __('Are you sure ? because this will reset your current setting','wp-ultimate-pdf'),
					'not_delete_default_setting' => __('Sorry! You can not delete default setting','wp-ultimate-pdf'),
					'deleted_successfully' => __('Setting Deleted Successfuly','wp-ultimate-pdf'),
					'select_setting_name' => __('Please select a setting name', 'wp-ultimate-pdf'),
					'enter_setting_name' => __('Please enter a setting name','wp-ultimate-pdf')
			);
			wp_localize_script ( 'bulk', 'bulk_obj', $translation_array);
		}
		
		/**
		 * checks for update on admin init
		 * @name ptp_on_admin_Init
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_on_admin_Init() 
		{
			/*register_setting (MWBPDF_GEN_PREFIX.'_options', MWBPDF_GEN_PREFIX, array (
					$this,
					'ptp_on_update_options' 
			) );*/
			register_setting(MWBPDF_GEN_PREFIX .'_display_option',MWBPDF_GEN_PREFIX,array($this,
					'ptp_on_update_options'));
			register_setting(MWBPDF_GEN_PREFIX .'_general_option',MWBPDF_GEN_PREFIX,array($this,
					'ptp_on_update_options'));
			register_setting(MWBPDF_GEN_PREFIX .'_body_option',MWBPDF_GEN_PREFIX,array($this,
					'ptp_on_update_options'));
			register_setting(MWBPDF_GEN_PREFIX .'header_option',MWBPDF_GEN_PREFIX,array($this,
					'ptp_on_update_options'));
			register_setting(MWBPDF_GEN_PREFIX .'advance_option',MWBPDF_GEN_PREFIX,array($this,
					'ptp_on_update_options'));
			register_setting(MWBPDF_GEN_PREFIX .'watermark_option',MWBPDF_GEN_PREFIX,array($this,
					'ptp_on_update_options'));
			register_setting(MWBPDF_GEN_PREFIX .'bulletstyle_option',MWBPDF_GEN_PREFIX,array($this,
					'ptp_on_update_options'));
			register_setting(MWBPDF_GEN_PREFIX .'postmeta_option',MWBPDF_GEN_PREFIX,array($this,
					'ptp_on_update_options'));
			add_action ( 'admin_enqueue_scripts', array (
					$this,
					'ptp_enqueue_custom_script' 
			) );
 
		}

		/**
		 * print notices.
		 * 
		 * @since 1.0.0
		*/

		function upg_print_notices($notices=array()){ 
			if(count($notices)){
				foreach($notices as $notice_array){

					$message = isset($notice_array['message']) ? esc_html($notice_array['message']) : '';
					$classes = isset($notice_array['classes']) ? esc_attr($notice_array['classes']) : 'error is-dismissable';
					if(!empty($message)){ ?>
						 <div class="xyz <?php echo $classes;?>">
						 	<p><?php echo $message;?></p>
						 </div>
					<?php 	
					}
				}
			}
		}
		
		/**
		 * Reset PDF Setting currently updating
		 * @name ptp_on_update_options
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param int $post        	
		 * @return int
		 */
		function ptp_on_update_options($option) 
		{
		 	
			if(!empty($option['settingname'])) 
			{
				 
				$ptpsettings = array();
				$ptpsettings = get_option('ptp_settings');
				$array_key_value=(array_keys($ptpsettings));
				$is_new_setting_name='true';
				foreach ($array_key_value as $key => $value) {
					if($value==$option['settingname']){
						$is_new_setting_name='false';
					} 
				}
				if(isset($is_new_setting_name) && !empty($is_new_setting_name) && ($is_new_setting_name=='true')){
					$default = $this->ptp_deafult_setting();
					unset($default['custom_footer_option']);
					unset($default['custom_footer']);
					unset($default['footer_font_pdf']);
					unset($default['footer_font_size']);
					unset($default['footer_cell_width']);
					unset($default['footer_min_height']);
					unset($default['footer_lcornerX']);
					unset($default['footer_font_lcornerY']);
					unset($default['footer_font_margin']);
					unset($default['footer_cell_fill']);
					unset($default['footer_align']);
					unset($default['footer_cell_auto_padding']);
					 
					$array_new_value_with_default_settings=array_replace($default,$option);
					 
					$ptpsettings[$option['settingname']]= $array_new_value_with_default_settings;
					update_option ( 'ptp_settings', $ptpsettings );
				}else{
 
					if(isset($option)){
						if( !isset($option['short'])){
							 unset($ptpsettings[$option['settingname']]['short']);

						}
						
						if( !isset($option['front_end'])){
							unset($ptpsettings[$option['settingname']]['front_end']);

						}
						 
						if (!isset($option['showemailfield'])) {
							unset($ptpsettings[$option['settingname']]['showemailfield']);
							 
						}
						if (!isset($option['admin_panel'])) {
							unset($ptpsettings[$option['settingname']]['admin_panel']);
							 
						}
						$array_merge_value=array_replace($ptpsettings[$option['settingname']],$option);
					    $ptpsettings[$option['settingname']]= $array_merge_value;
						 
					}
	 
					update_option ( 'ptp_settings', $ptpsettings );
				} 
				$notice['message'] = __('Settings Saved and Applied Successfully !','wp-advanced-pdf');
				$notice['classes'] = "notice notice-success";
				$validation_notice[] = $notice;
				if(!session_id()){
				session_start();
					
				}
				$_SESSION['mwb_notice'] = $validation_notice;
				return $ptpsettings[$option['settingname']];
			  
			}
			else if(!empty($_POST['settings'])){
 
				$ptpsettings = array();
				$ptpsettings = get_option('ptp_settings');
				 
				if(array_key_exists($_POST['settings'], $ptpsettings)){
					$index=$option['index'];

					if(isset($index)){
						$ptpsettings[$_POST['settings']][$index]=$option;	
					}
					else{
						$ptpsettings[$_POST['settings']]= $option;
					}

				}
				else{
					$ptpsettings[$_POST['settings']] = $option;
				}
				 
				update_option ( 'ptp_settings', $ptpsettings );
				 
			}
			$notice['message'] = __('Settings Saved Successfully !','wp-advanced-pdf');
			$notice['classes'] = "notice notice-success";
			$validation_notice[] = $notice;
			if(!session_id()){
			session_start();
				
			}
			$_SESSION['mwb_notice'] = $validation_notice;
	 		if(isset($_POST['settings']) &&  isset($ptpsettings[$_POST['settings']]) && !empty($ptpsettings[$_POST['settings']])){
				return $ptpsettings[$_POST['settings']];

	 		}else{
	 			return $option;
	 		}
		}
		
		/**
		 * adds settings link
		 * @name ptp_action_links_handler
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_action_links_handler($action_links) 
		{
			$settings_link = '<a href="admin.php?page=wp-ultimate-pdf-generator">'.__('Settings','wp-ultimate-pdf').'</a>';
			array_unshift ( $action_links, $settings_link );
			return $action_links;
		}
		
		/**
		 * attach option page to submenu
		 * @name ptp_option_page_handler
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_option_page_handler() 
		{
			 require (MWBPDF_GEN_PATH . '/Content/pdf_content.php');
		}
		
		/**
		 * generates default settings
		 * @name ptp_deafult_setting
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_deafult_setting() 
		{
			$default = array();
			$default = array();
			$default['front_end'] = '1';
			$default['show'] = 'guestuser';
			$default['availability'] = 'public';
			$default['admin_panel'] = 1;
			$default['0']['featured'] = 1;
			$default['0']['cache_updation_sch'] = 'none';
			$default['0']['mwb_file_name'] = 'post_name';
			$default['0']['author_name'] = '';
			$default['0']['content_position'] = 'left';
			$default['0']['content_placement'] = 'after';
			$default['0']['link_button'] = 'default';
			$default['0']['custon_link_url'] = '';
			$default['1']['image_scale'] = 1.25;
			$default['1']['margin_top'] = 27;
			$default['1']['margin_left'] = 15;
			$default['1']['margin_right'] = 15;
			$default['1']['page_size'] = 'LETTER';
			$default['1']['page_orientation'] = 'P';
			$default['1']['unitmeasure'] = 'mm';
			$default['1']['content_font_pdf'] = 'helvetica';
			$default['1']['content_font_size'] = 12;
			$default['1']['custom_title'] = '';
			$default['1']['Customcss'] = '';
			$default['2']['page_header'] = 'upload-image';
			$default['2']['logo_img_url'] = MWBPDF_GEN_URL.'/asset/images/default-logo.png';
			$default['2']['image_factor'] = 15;
			$default['2']['logomTop'] = 10;
			$default['2']['header_font_pdf'] = 'helvetica';
			$default['2']['header_font_size'] = 10;
			$default['custom_footer_option'] = 1;
			$default['custom_footer'] = '';
			$default['footer_font_pdf'] = 'helvetica';
			$default['footer_font_size'] = 10;
			$default['footer_cell_width'] = 0;
			$default['footer_min_height'] = 0;
			$default['footer_lcornerX'] = 15;
			$default['footer_font_lcornerY'] = 290;
			$default['footer_font_margin'] = 10;
			$default['footer_cell_fill'] = 1;
			$default['footer_align'] = '';
			$default['footer_cell_auto_padding'] = 1;
			$default['3']['add_watermark'] = 1;
			$default['3']['rotate_water'] = 45;
			$default['3']['watermark_text'] = 'Default Watermark';
			$default['3']['water_img_h'] = '';
			$default['3']['water_img_w'] = '';
			$default['3']['water_img_t'] = 0.5;
			$default['3']['background_img_url'] = '';
			$default['5']['bullet_img_url'] = '';
			$default['5']['custom_image_height'] = 2;
			$default['5']['custom_image_width'] = 3;
			$default['4']['custom_font_for_body'] = '';
			$default['4']['set_rotation'] = 0;
			$default['4']['fontStretching'] = 100;
			$default['4']['fontSpacig'] = 0;
			$default['6']['post']['post'] = 1;
			$default['6']['page']['page'] = 1;
			$default['6']['product']['product'] = 1;
			$default['6']['mwb_wcswr_from_datename'] = '';
			$default['6']['mwb_wcswr_to_datename'] = '';
			$default['6']['mwb_wcswr_from_timename'] = '';
			$default['6']['mwb_wcswr_to_timename'] = '';
			$default['6']['_product_attributesname'] = '';
			$default['6']['_stock_statusname'] = '';
			$default['6']['_downloadablename'] = '';
			$default['6']['_virtualname'] = '';
			$default['6']['_min_variation_pricename'] = '';
			$default['6']['_max_variation_pricename'] = '';
			$default['6']['_min_price_variation_idname'] = '';
			$default['6']['_max_price_variation_idname'] = '';
			$default['6']['_min_variation_regular_pricename'] = '';
			$default['6']['_max_variation_regular_pricename'] = '';
			$default['6']['_min_regular_price_variation_idname'] = '';
			$default['6']['_max_regular_price_variation_idname'] = '';
			$default['6']['_min_variation_sale_pricename'] = '';
			$default['6']['_max_variation_sale_pricename'] = '';
			$default['6']['_min_sale_price_variation_idname'] = '';
			$default['6']['_max_sale_price_variation_idname'] = '';
			$default['6']['_pricename'] = '';
			$default['6']['_default_attributesname'] = '';
			$default['6']['_visibilityname'] = '';
			$default['6']['total_salesname'] = '';
			$default['6']['_purchase_notename'] = '';
			$default['6']['_featuredname'] = '';
			$default['6']['_weightname'] = '';
			$default['6']['_lengthname'] = '';
			$default['6']['_widthname'] = '';
			$default['6']['_heightname'] = '';
			$default['6']['_skuname'] = '';
			$default['6']['_regular_pricename'] = '';
			$default['6']['_sale_pricename'] = '';
			$default['6']['_sale_price_dates_fromname'] = '';
			$default['6']['_sale_price_dates_toname'] = '';
			$default['6']['_sold_individuallyname'] = '';
			$default['6']['_manage_stockname'] = '';
			$default['6']['_backordersname'] = '';
			$default['6']['_stockname'] = '';
			$default['6']['_upsell_idsname'] = '';
			$default['6']['_crosssell_idsname'] = '';
			$default['6']['_product_versionname'] = '';
			$default['6']['_product_image_galleryname'] = '';
			$default['6']['_wc_rating_countname'] = '';
			$default['6']['_wc_review_countname'] = '';
			$default['6']['_wc_average_ratingname'] = '';
			$default['6']['_themex_hiddenname'] = '';
			$default['6']['_themex_ratingname'] = '';
			$default['6']['_themex_salesname'] = '';
			$default['6']['_themex_admirersname'] = '';
			$default['6']['_themex_ratename'] = '';
			$default['6']['_themex_aboutname'] = '';
			$default['6']['_themex_policyname'] = '';
			return $default;
		}
		
		/**
		 * sets default values in plugin activate
		 * @name ptp_set_default_on_activate
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_set_default_on_activate() 
		{
			// set dafault option on activate
			$default = $this->ptp_deafult_setting();
			
			if (! get_option ( MWBPDF_GEN_PREFIX )) 
			{
				add_option ( MWBPDF_GEN_PREFIX, $default );
			}
			// create directory and move logo to upload directory
			if (! file_exists ( WP_CONTENT_DIR . '/uploads' )) 
			{
				@mkdir ( WP_CONTENT_DIR . '/uploads' );
			}
		}
		
		/**
		 * generates pdf file of current post
		 * @name ptp_generate_post_to_pdf
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @return boolean
		 */
		function ptp_generate_post_to_pdf() 
		{
		 
			global $post;
			if( isset($post)) {

			
				if(http_response_code()!='404') {
					if (! isset ( $this->options['6'][$post->post_type][$post->post_type] ))
						return false;
					if ('pdf' == (isset ( $_GET ['format'] ) ? $_GET ['format'] : null)) {
						global $post;
						$post = get_post ();
						$content = $post->the_content;
						if ($this->options['0']['mwb_file_name'] == 'post_name') {
							$filePath = MWB_CACHE_DIR . '/' . $post->post_name . '.pdf';
							$fileName = $post->post_name . '.pdf';
						} else {
							$filePath = MWB_CACHE_DIR . '/' . $post->ID . '.pdf';
							$fileName = $post->ID . '.pdf';
						}
						$fileMime = 'pdf';
						if (! isset ( $this->options['0']['includefromCache'] )) {
							$this->generate_post_to_pdf_file ( $post->ID );
						} else {
							if (! file_exists ( $filePath )) {
								$this->generate_post_to_pdf_file ( $post->ID );
							}
						}
						$output = $this->output_Post_to_pdf_file ( $filePath, $fileName, $fileMime );
					}
				}
			}
			  
		}
		
		/**
		 * checks whether pdf for the post is in cache or not
		 * @name ptp_email_pdf
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param int $postID        	
		 * @param string $useremailID        	
		 * @return boolean
		 */
		function ptp_email_pdf($postID, $useremailID) 
		{
		 
			$post = get_post ( $postID );
			$content = $post->post_content;
			if ($this->options['0']['mwb_file_name'] == 'post_name') {
				$filePath = MWB_CACHE_DIR . '/' . $post->post_name . '.pdf';
				$fileName = $post->post_name . '.pdf';
			} else {
				$filePath = MWB_CACHE_DIR . '/' . $post->ID . '.pdf';
				$fileName = $post->ID . '.pdf';
			}
			$fileMime = 'pdf';
			if (! isset ( $this->options['0']['includefromCache'] )) {
				$this->generate_pdf_file_email ( $post->ID, $useremailID );
			} else {
				if (! file_exists ( $filePath )) {
					$this->generate_pdf_file_email ( $post->ID, $useremailID );
				}
			}
				 
		}
		
		/**
		 * generate output of pdf generated
		 * @name output_Post_to_pdf_file
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param string $pdffile        	
		 * @param string $file_name        	
		 * @param string $mimepdftype        	
		 * @return boolean
		 */
		function output_Post_to_pdf_file($pdffile, $file_name, $mimepdftype = 'application/pdf') 
		{

			if (! is_readable ( $pdffile ))
				return false;
			$size = filesize ( $pdffile );
			$file_name = rawurldecode ( $file_name );
			$known_mime_types = array (
					"pdf" => "application/pdf",
					"txt" => "text/plain",
					"html" => "text/html",
					"htm" => "text/html",
					"exe" => "application/octet-stream",
					"zip" => "application/zip",
					"doc" => "application/msword",
					"xls" => "application/vnd.ms-excel",
					"ppt" => "application/vnd.ms-powerpoint",
					"gif" => "image/gif",
					"png" => "image/png",
					"jpeg" => "image/jpg",
					"jpg" => "image/jpg",
					"php" => "text/plain" 
			);

			if ($mimepdftype == '') {
				$file_extension = strtolower ( substr ( strrchr ( $pdffile, "." ), 1 ) );
				if (array_key_exists ( $file_extension, $known_mime_types )) {
					$mimepdftype = $known_mime_types [$file_extension];
				} else {
					$mimepdftype = "application/force-download";
				}
			}else{
				$mimepdftype = 'application/pdf';
			}
 
			@ob_end_clean ();
			
			if (ini_get ( 'zlib.output_compression' ))
				ini_set ( 'zlib.output_compression', 'Off' );
			header ( 'Content-Type: ' . $mimepdftype );
			header ( 'Content-Disposition: attachment; filename="' . $file_name . '"' );
			header ( "Content-Transfer-Encoding: binary" );
			header ( 'Accept-Ranges: bytes' );
			
			header ( "Cache-control: private" );
			header ( 'Pragma: private' );
			header ( "Expires: tue, 26 aug 2015 05:00:00 GMT" );
			
			if (isset ( $_SERVER ['HTTP_RANGE'] )) {
				list ( $a, $range ) = explode ( "=", $_SERVER ['HTTP_RANGE'], 2 );
				list ( $range ) = explode ( ",", $range, 2 );
				list ( $range, $range_end ) = explode ( "-", $range );
				$range = intval ( $range );
				if (! $range_end) {
					$range_end = $size - 1;
				} else {
					$range_end = intval ( $range_end );
				}
				$new_length = $range_end - $range + 1;
				header ( "HTTP/1.1 206 Partial Content" );
				header ( "Content-Length: $new_length" );
				header ( "Content-Range: bytes $range-$range_end/$size" );
			} else {
				$new_length = $size;
				header ( "Content-Length: " . $size );
			}
			
			$chunksize = 1 * (1024 * 1024);
			$bytes_send = 0;
			if ($pdffile = fopen ( $pdffile, 'r' )) {
				if (isset ( $_SERVER ['HTTP_RANGE'] ))
					fseek ( $pdffile, $range );
				while ( ! feof ( $pdffile ) && (! connection_aborted ()) && ($bytes_send < $new_length) ) {
					$buffer = fread ( $pdffile, $chunksize );
					print ($buffer) ;
					flush ();
					$bytes_send += strlen ( $buffer );
				}
				fclose ( $pdffile );
			} else
				return false;
			return true;
		}
		
		/**
		 * generate pdf of a post and send email to guest user
		 * @name generate_pdf_file_email
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param int $postID        	
		 * @param string $useremailID        	
		 */
		function generate_pdf_file_email($postID, $useremailID) 
		{
			$post = get_post ( $postID );
			$content = $post->post_content;
			if (! class_exists ( 'TCPDF' )) {
				require_once MWBPDF_GEN_PATH . '/tcpdf_min/tcpdf.php';
			}
			if (! class_exists ( 'pdfheader' )) {
				require_once MWBPDF_GEN_PATH . '/Content/pdf_headercontent.php';
			}
			if (! class_exists ( 'simple_html_dom_node' )) {
				require_once MWBPDF_GEN_PATH . '/Htmlstructure/Htmlstructure.php';
			}
			$post->post_content = apply_filters ( 'the_post_export_content', $post->post_content );
			$post->post_content = wpautop ( $post->post_content );
			$post->post_content = do_shortcode ( $post->post_content );
			
			if ( isset($this->options['0']['mwb_file_name']) && !empty($this->options['0']['mwb_file_name']) && $this->options['0']['mwb_file_name'] == 'post_name') {
				$filePath = MWB_CACHE_DIR . '/' . $post->post_name . '.pdf';
			} else {
				$filePath = MWB_CACHE_DIR . '/' . $post->ID . '.pdf';
			}
			// new PDF document
			if (isset ( $this->options['1']['page_size'] )) {
				$pagesize = ($this->options['1']['page_size']);
			} else {
				$pagesize = PDF_PAGE_FORMAT;
			}
			if (isset ( $this->options['1']['unitmeasure'] )) {
				$unit = ($this->options['1']['unitmeasure']);
			} else {
				$unit = PDF_UNIT;
			}
			if (isset ( $this->options['1']['page_orientation'] )) {
				$orientation = ($this->options['1']['page_orientation']);
			} else {
				$orientation = PDF_PAGE_ORIENTATION;
			}
			
			$pdf = new CUSTOMPDF ( $orientation, $unit, $pagesize, true, 'UTF-8', false );
			// information about doc
			$pdf->SetCreator ( 'Post to PDF plugin by makewebbetter with ' . PDF_CREATOR );
			$pdf->SetAuthor ( get_bloginfo ( 'name' ) );
			if (! empty ( $this->options['1']['custom_title'] )) {
				$pdf_title = $this->options['1']['custom_title'];
			} else {
				$pdf_title = $post->post_title;
			}
			
			// logo width calculation
			if (isset ( $this->options['2']['page_header'] ) and ($this->options['2']['page_header']) == 'upload-image' and !empty ( $this->options['2']['logo_img_url'] )) {
				
				if ($this->options['2']['page_header'] == "upload-image") {
					$logoImage_url = $this->options['2']['logo_img_url'];
				}
				$infologo = getimagesize ( $logoImage_url );
				if (isset ( $this->options['2']['image_factor'] )) {
					$logo_width = ( int ) (($this->options['2']['image_factor'] * $infologo [0]) / $infologo [1]);
				} else {
					$logo_width = ( int ) ((12 * $infologo [0]) / $infologo [1]);
				}
				// for PHP 5.4 or below set default header data
				if (version_compare ( phpversion (), '5.4.0', '<' )) {
					$pdf->SetHeaderData ( $logoImage_url, $logo_width, html_entity_decode ( get_bloginfo ( 'name' ), ENT_COMPAT | ENT_QUOTES ), html_entity_decode ( get_bloginfo ( 'description' ) . "\n" . home_url (), ENT_COMPAT | ENT_QUOTES ), $header_text_color, $header_line_color );
				} else {
					$pdf->SetHeaderData ( $logoImage_url, $logo_width, html_entity_decode ( get_bloginfo ( 'name' ), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES ), html_entity_decode ( get_bloginfo ( 'description' ) . "\n" . home_url (), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES ), $header_text_color, $header_line_color );
				}
			}
			if (isset ( $this->options['2']['page_header'] ) and ($this->options['2']['page_header']) == 'None') {
				$pdf->setPrintHeader(false);
			}
			// set header and footer fonts
			if ( isset($this->options['2']['header_font_size']) && !empty($this->options['2']['header_font_size']) && ($this->options['2']['header_font_size']) > 0) {
				$header_font_size = $this->options['2']['header_font_size'];
			} else {
				$header_font_size = 10;
			}
			if ( isset($this->options ['footer_font_size']) && !empty($this->options ['footer_font_size']) && ($this->options ['footer_font_size']) > 0) {
				$footer_font_size = $this->options ['footer_font_size'];
			} else {
				$footer_font_size = 10;
			}
			$pdf->setHeaderFont ( array (
					$this->options['2']['header_font_pdf'],
					'',
					$header_font_size 
			) );
			$pdf->setFooterFont ( array (
					$this->options ['footer_font_pdf'],
					'',
					$footer_font_size 
			) );
			
			$pdf->SetDefaultMonospacedFont ( PDF_FONT_MONOSPACED );
			
			if (isset($this->options['1']['margin_left']) ) {
				$pdf->SetLeftMargin ( $this->options['1']['margin_left'] );
			} else {
				$pdf->SetLeftMargin ( PDF_MARGIN_LEFT );
			}
			
			if (isset($this->options ['margin_right'] )) {
				$pdf->SetRightMargin ( $this->options['1']['margin_right'] );
			} else {
				$pdf->SetRightMargin ( PDF_MARGIN_RIGHT );
			}
			
			if (isset($this->options['1']['margin_top'] )) {
				$pdf->SetTopMargin ( $this->options['1']['margin_top'] );
			} else {
				$pdf->SetTopMargin ( PDF_MARGIN_TOP );
			}
			if (isset($this->options['2']['logomTop'])) {
				$pdf->SetHeaderMargin ( $this->options['2']['logomTop'] );
			} else {
				$pdf->SetHeaderMargin ( PDF_MARGIN_HEADER );
			}
			
			if (isset($this->options ['footer_font_margin'] )) {
				$pdf->SetFooterMargin ( $this->options ['footer_font_margin'] );
				// set auto page breaks
				$pdf->SetAutoPageBreak ( TRUE,  $this->options ['footer_font_margin']  );
			} else {
				$pdf->SetFooterMargin ( PDF_MARGIN_FOOTER );
				// set auto page breaks
				$pdf->SetAutoPageBreak ( TRUE, PDF_MARGIN_FOOTER );
			}
			
			if ($this->options['1']['image_scale'] > 0) {
				$pdf->setImageScale ( $this->options['1']['image_scale'] );
			} else {
				$pdf->setImageScale ( PDF_IMAGE_SCALE_RATIO );
			}
			
			// set default font subsetting mode
			$pdf->setFontSubsetting ( true );
			
			$pdf->SetFont ( $this->options['1']['content_font_pdf'], '', $this->options['1']['content_font_size'], '', true );
			
			if (! empty ( $this->options['5']['bullet_img_url'] )&& isset( $this->options['5']['bullet_img_url'] )) {
				$temp = $this->options['5']['bullet_img_url'];
				$temp = end ( explode ( '/', $temp ) );
				$temp = end ( explode ( '.', $temp ) );
				$listsymbol = 'img|' . $temp . '|' . $this->options['5']['custom_image_width'] . '|' . $this->options['5']['custom_image_height'] . '|' . $this->options['5']['bullet_img_url'];
				$pdf->setLIsymbol ( $listsymbol );
			}
			
			// Add a page
			// This method has several options, check the source code documentation for more information.
			
			if ($this->options['4']['fontStretching']) {
				$pdf->setFontStretching($this->options['4']['fontStretching']);
			}else{
				$pdf->setFontStretching('100');

			}
			if (isset($this->options['4']['fontSpacig']) && !empty($this->options['4']['fontSpacig'])) {
				$pdf->setFontSpacing($this->options['4']['fontSpacig']);
			}else{
				$pdf->setFontSpacing('0');

			}
			$page_format = array();
			if (isset($this->options['4']['set_rotation']) && !empty($this->options['4']['set_rotation'])) {
				$page_format['Rotate'] = $this->options['4']['set_rotation'];
			} else {
				$page_format['Rotate'] = 0;
			}
			$pdf->AddPage($this->options['1']['page_orientation'], $page_format, false, false);
			$html = '';
			if (isset ( $this->options['1']['CustomCSS_option'] )) {
				$html = '<style>' . $this->options['1']['Customcss'] . '</style>';
			}
			$html .= "<body>";
			$html .= "<h1 style=\"text-align:center\">". html_entity_decode ( $pdf_title, ENT_QUOTES ) ."</h1>";
			
			$this->options=get_option(MWBPDF_GEN_PREFIX);
			if (!empty ( $this->options['1']['rtl_support'] )) {
				// set some language dependent data:
				$lg = Array();
				$lg['a_meta_charset'] = 'UTF-8';
				$lg['a_meta_dir'] = 'rtl';
				$lg['a_meta_language'] = 'fa';
				$lg['w_page'] = 'page';
					
				// set some language-dependent strings (optional)
				$pdf->setLanguageArray($lg);
				$pdf->setRTL(true);
			}
			if (isset ( $this->options['0']['author_name'] ) and ! $this->options ['author_name'] == '') {
				$author_id = $post->post_author;
				$author_meta_key = $this->options['0']['author_name'];
				$author = get_user_meta ( $author_id );
				$html .= '<p><strong>'.__('Author','wp-ultimate-pdf').': </strong>' . $author [$author_meta_key] [0] . '</p>';
			}
						
			if (isset ( $this->options['0']['post_categ'] ))
			{
				$categories = get_the_category ( $post->ID );
				if ($categories) 
				{
					$html .= '<p><strong>'.__('Categories','wp-ultimate-pdf').': </strong>' . $categories [0]->cat_name . '</p>';
				}
			}
			 
			$posts = get_post_custom($postID);

			
			if (isset ( $this->options['0']['post_tags'] )) {
				$tags = get_the_tags ( $post->the_tags );
				if ($tags) {
					$html .= '<p><strong>'.__('Tagged as','wp-ultimate-pdf').': </strong>';
					foreach ( $tags as $tag ) {
						$tag_link = get_tag_link ( $tag->term_id );
						$html .= '<a href="' . $tag_link . '">' . $tag->name . '</a>';
						if (next ( $tags )) {
							$html .= ', ';
						}
					}
					$html .= '</p>';
				}
			}
			 
			// Set some content to print
		 
			// Display featured image if set in config on page/post
			
			if (isset ( $this->options['0']['featured'] )) 
			{
				if (has_post_thumbnail ( $post->ID )) 
				{
					$html .= get_the_post_thumbnail ( $post->ID );
				}
			}
			
			$post_content = $post->post_content;
			if (empty ( $post->post_content )) 
			{
				$post_content = isset ( $this->options['4']['docEntryTpl'] ) ? $this->options['4']['docEntryTpl'] : '';
			}
			
			$html .= htmlspecialchars_decode ( htmlentities ( $post_content, ENT_NOQUOTES, 'UTF-8', false ), ENT_NOQUOTES );


			// Display date if set in config
			if (isset ( $this->options['0']['post_date'] )) {
				$newDate = date ( "d-m-Y", strtotime ( $post->post_date ) );
				$html .= '<p><strong>'.__('Date','wp-ultimate-pdf').': </strong>' . $newDate . '</p>';
			}
			$arr=get_post_custom($postID);	
			unset($arr['_edit_last']);
			unset($arr['_edit_lock']);
			unset($arr['_thumbnail_id']);
			 
			$post_type_val=get_post_type($postID);
			foreach ($arr as $key=>$val)
			{ 
			
				if( isset($this->options['6'][$post_type_val][$key]) && !empty($this->options['6'][$post_type_val][$key]) && ($this->options['6'][$post_type_val][$key]==1))
				{ 
				 
					if(isset($this->options['6'][$post_type_val][$key.'name']) && !empty($this->options ['6'][$post_type_val][$key.'name']) && ($this->options ['6'][$post_type_val][$key.'name'] != " ")  && ($this->options ['6'][$post_type_val][$key.'name'] != " ") )
					{  
						if( (null !==(get_post_meta($postID,$key,true))) && (!is_array(get_post_meta($postID,$key,true))) )
						{
							 
							if (function_exists('get_fields') && class_exists('acf')) {
				
								$field = get_fields($postID);
			 					if( (array_key_exists($key, $field)) ){
			 						 continue;
			 					 
			 					}
								 
							}
							 
							$html .= '<p><strong>'.$this->options['6'][$post_type_val][$key.'name'].':'. '</strong>' . get_post_meta($postID,$key,true) . '</p>';
						}
					}
					 
				}
				
			}
 
			/*
			  Setting for displaying ACF field details only
			*/
 
			if (function_exists('get_fields') && class_exists('acf')) {
			
				$field = get_fields($postID);

				 
				if( $field )
				{
					foreach( $field as $field_name => $value )
					{	
						 
						if( !(( isset($this->options['6'][$post_type_val][$field_name]) && ( $this->options['6'][$post_type_val][$field_name]== 1 ) ) || ( isset($this->options['6'][$post_type_val]['_'.$field_name]) && ( $this->options['6'][$post_type_val]['_'.$field_name]== 1 ) )) ){
							continue;
						}
					 
						$field = get_field_object($field_name , $postID );
					 
						//do not need to display password
						if(isset($field['type']) && ($field['type'] == "password")){
							continue;
						}
					 	//coding to display ACF field image
						if ( isset($field['type']) && ($field['type'] == 'image')) 
						{ 	
							
							if($field['save_format']=='id' && $field["label"] != '' && $field["value"] != "" ){
								$html .= '<p><strong>'.$field["label"] .':</strong><p>';
								$html .=  wp_get_attachment_image( $field["value"] , 'thumbnail');
							}elseif( $field['save_format']=='url' ){

								if( $field["label"] != '' && $field["value"] != ""){

									$html .= '<p><strong>'.$field["label"] .':</strong><a href=" '.$field["value"].'" >'.$field ['value'].'</a><p>';
									 
								}

							}elseif( $field['save_format'] == 'object' && $field["label"] != '' && $field["value"]['id'] != ""){
								 
								if( $field["label"] != '' && $field["value"]['id'] != ""){
								 
									$html .= $field["label"] .':';
									$html .=  wp_get_attachment_image( $field["value"]['id'] , 'thumbnail');
								}
 
							}
					 
							continue;

						}

						//coding for displaying Taxonomy fields

						if( isset($field['type'] ) && ($field['type'] == 'taxonomy')){
							 continue;
						}

					 
						//coding for displaying Google map texual address
						if( isset($field['type'] ) && ($field['type'] == 'google_map')){
							if( $field["label"] != '' && $field["value"]['address'] != ""){

								$html .= '<p><strong>'.$field["label"].': </strong>' . $field["value"]['address'] . '</p> ';
							}
							continue;
						}

						//coding to display Date Picker
						if(isset($field['type']) && ($field['type']=='date_picker')){

							if( $field["label"] != '' && $field["value"] != "" && ($field['date_format'] == 'yymmdd')){
								//convert the date string in proper format
								$newstr = substr_replace($field["value"], ' : ', 4, 0);
								$newstr = substr_replace($newstr, ' : ',9 , 0);

								$html .= '<p><strong>'.$field["label"].': </strong>' . $newstr . '</p> ';
							}
							continue;
						}
						//coding to display radio button details
						if( !is_array($value) && ($field['type'] == "radio") && (isset($field['choices']))){
							if( $field["label"] != '' && $field["value"] != ""){
								$html .= '<p><strong>'.$field["label"].': </strong>' . $field["value"] . '</p> ';
							}
							continue;
						}

						//coding to display links for product and any post details
						if(isset($field) && ($field['type'] == 'page_link')){
							if(is_array($field['value']) && !empty($field['value'])){
								$html .= '<p><strong>'.$field["label"].': </strong></p> ';

								foreach ($field['value'] as $current_url_value) {

									if(isset($current_url_value) && ($current_url_value != '')){
										$html .=  '<p><a href="'.$current_url_value.'">'.$current_url_value.'</a><p>'  ;
									}
								}
							}else{
								if(($field["label"] !='') && ($field['value'] != "")){
									$html .= '<p><strong>'.$field["label"].': </strong></p> ';

									$html .=  '<p><a href="'.$field['value'].'">'.$field['value'].'</a><p>'  ;
								}
								 
							}
							continue;
						}

						//coding to handle post data Object
						if(isset($field) && ( $field['type'] == 'post_object' ) && (!empty($field['value'])) ){
							$acf_fetched_post_value=$field['value'];
							if( isset($acf_fetched_post_value) && !empty($acf_fetched_post_value) && is_array($acf_fetched_post_value)){
								$html .= '<p><strong>'.$field["label"].': </strong></p> ';
								foreach ($acf_fetched_post_value as $acf_fetched_one_post_value) {
									
									  if( (get_permalink( $acf_fetched_one_post_value->ID) != '') && ( get_the_title( $acf_fetched_one_post_value->ID) != '' ) ){
										
									 	 $html .= '<p>'.$acf_fetched_one_post_value->post_type.'  -->  '.'<a href="'.get_permalink( $acf_fetched_one_post_value->ID).'">'.get_the_title( $acf_fetched_one_post_value->ID).'</a></p>';
									 }
								}


							}else{
								 if( (get_permalink( $acf_fetched_post_value->ID) != '') && ( get_the_title( $acf_fetched_post_value->ID) != '' ) ){
									$html .= '<p><strong>'.$field["label"].': </strong></p> ';
								 	
								 	$html .= '<p>'.$acf_fetched_post_value->post_type.'  -->  '.'<a href="'.get_permalink( $acf_fetched_post_value->ID).'">'.get_the_title( $acf_fetched_post_value->ID).'</a></p>';
								 }
							}
							 
							continue;
						}

						//cofing to display text, textarea, email, (password need to block), any field returning simple string
						if(!is_array($value) && (!isset($field['choices'])) && !is_object($value)){
							if( $field["label"] != '' && $field["value"] != ""){
								$html .= '<p><strong>'.$field["label"].': </strong>' . $field["value"] . '</p> ';
							}

						}

						//cofing to display radio button, select and checkbox details
						if(is_array($field) && isset($field['choices']) && !empty($field['choices']) && ($field['type'] != "radio")){
							$html .= '<p><strong>'.$field["label"].': </strong></p> ';

							foreach ($field['value'] as $selected_value) {
								if(isset($field['choices'][ $selected_value ]) && ($field['choices'][ $selected_value ] != ''))

									$html .= '<p>' . $field['choices'][ $selected_value ] . '</p> ';

							}
						}


					}
				}
			}

			/*
			  ACF field details end
			*/
 
			$html .="</body>";
			$dom = new simple_html_dom ();
			$dom->load ( $html );
			
			foreach ( $dom->find ( 'img' ) as $e ) {
				// Start Image check by om
				$exurl = ''; // external streams
				$imsize = FALSE;
				$file = $e->src;
				// check if we are passing an image as file or string
				if ($file [0] === '@') {
					// image from string
					$imgdata = substr ( $file, 1 );
				} else { // image file
					if ($file {0} === '*') {
						// image as external stream
						$file = substr ( $file, 1 );
						$exurl = $file;
					}
					// check if is local file
					if (! @file_exists ( $file )) {
						// encode spaces on filename (file is probably an URL)
						$file = str_replace ( ' ', '%20', $file );
					}
					if (@file_exists ( $file )) {
						// get image dimensions
						$imsize = @getimagesize ( $file );
					}
					if ($imsize === FALSE) {
						$imgdata = TCPDF_STATIC::fileGetContents ( $file );
					}
				}
				if (isset ( $imgdata ) and ($imgdata !== FALSE) and (strpos ( $file, '__tcpdf_img' ) === FALSE)) {
					// check Image size
					$imsize = @getimagesize ( $file );
				}
				if ($imsize === FALSE) {
					$e->outertext = '';
				} else {
					// End Image Check
					if (preg_match ( '/alignleft/i', $e->class )) {
						$imgalign = 'left';
					} elseif (preg_match ( '/alignright/i', $e->class )) {
						$imgalign = 'right';
					} elseif (preg_match ( '/aligncenter/i', $e->class )) {
						$imgalign = 'center';
						$htmlimgalign = 'middle';
					} else {
						$imgalign = 'none';
					}
					
					$e->class = null;
					$e->align = $imgalign;
					if (isset ( $htmlimgalign )) {
						$e->style = 'float:' . $htmlimgalign;
					} else {
						$e->style = 'float:' . $imgalign;
					}
					
					if (strtolower ( substr ( $e->src, - 4 ) ) == '.svg') {
						$e->src = null;
						$e->outertext = '<div style="text-align:' . $imgalign . '">[ SVG: ' . $e->alt . ' ]</div><br/>';
					} else {
						$e->outertext = '<div style="text-align:' . $imgalign . '">' . $e->outertext . '</div>';
					}
				}
			}
			$html = $dom->save ();
			$dom->clear ();
			$pdf->setFormDefaultProp ( array (
					'lineWidth' => 1,
					'borderStyle' => 'solid',
					'fillColor' => array (
							255,
							255,
							200 
					),
					'strokeColor' => array (
							255,
							128,
							128 
					) 
			) );
			
			// Print text using writeHTML
			$pdf->writeHTML ( $html, true, 0, true, 0 );
			if (isset ( $this->options['3']['add_watermark'] )) {
				$no_of_pages = $pdf->getNumPages ();
				for($i = 1; $i <= $no_of_pages; $i ++) {
					$pdf->setPage ( $i );
					
					// Get the page width/height
					$myPageWidth = $pdf->getPageWidth ();
					$myPageHeight = $pdf->getPageHeight ();
					
					// Find the middle of the page and adjust.
					$myX = ($myPageWidth / 2) - 75;
					$myY = ($myPageHeight / 2) + 25;
					
					// Set the transparency of the text to really light
					$pdf->SetAlpha ( 0.09 );
					
					// Rotate 45 degrees and write the watermarking text
					$pdf->StartTransform ();
					$rotate_degr = isset ( $this->options['3']['rotate_water'] ) ? $this->options['3']['rotate_water'] : '45';
					$pdf->Rotate ( $rotate_degr, $myX, $myY );
					$water_font = isset ( $this->options['3']['water_font'] ) ? $this->options['3']['water_font'] : 'courier';
					$pdf->SetFont ( $water_font, "", 30 );
					$watermark_text = isset ( $this->options['3']['watermark_text'] ) ? $this->options['3']['watermark_text'] : '';
					$pdf->Text ( $myX, $myY, $watermark_text );
					$pdf->StopTransform ();
					
					// Reset the transparency to default
					$pdf->SetAlpha ( 1 );
				}
			}

			if (isset ( $this->options['3']['add_watermark_image'] )) {
				if (! empty ( $this->options['3']['background_img_url'] )) {
					$no_of_pages = $pdf->getNumPages ();
					for($i = 1; $i <= $no_of_pages; $i ++) {
						$pdf->setPage ( $i );
						
						$myPageWidth = $pdf->getPageWidth ();
						$myPageHeight = $pdf->getPageHeight ();
						$myX = ($myPageWidth / $myPageWidth) - 50; // WaterMark Positioning
						$myY = ($myPageHeight / $myPageHeight) - 40;
						$ImageT = isset ( $this->options['3']['water_img_t'] ) ? $this->options['3']['water_img_t'] : '';
						// Set the transparency of the text to really light
						$pdf->SetAlpha ( $ImageT );
						
						// Rotate 45 degrees and write the watermarking text
						$pdf->StartTransform ();
						$ImageW = isset ( $this->options['3']['water_img_h'] ) ? $this->options['3']['water_img_h'] : '';
						$ImageH = isset ( $this->options['3']['water_img_w'] ) ? $this->options['3']['water_img_w'] : '';
						
						$watermark_img = isset ( $this->options['3']['background_img_url'] ) ? $this->options['3']['background_img_url'] : '';
						$pdf->Image ( $watermark_img, $myX, $myY, $ImageW, $ImageH, '', '', '', true, 150 );
						
						$pdf->StopTransform ();
						
						// Reset the transparency to default
						$pdf->SetAlpha ( 1 );
					}
				}
			}
			// cache directory
			if (! is_dir ( MWB_CACHE_DIR )) {
				mkdir ( MWB_CACHE_DIR, 0755, true );
			}
			$pdf->Output ( $filePath, 'F' );
			 
			$to = $useremailID;
			$from = 'wp-Ultimate-Pdf-Generator';
			$subject = "Here is your pdf attachment";
			$headers = "from: $from ";
			$message = 'Please download attached PDF ';
			$title=get_the_title($postID);
			$type=get_post_type($postID);
			if (wp_mail ( $to, $subject, $message, $headers = '', $attachments = array (
					$filePath 
			) )) {
				$this->export_installdata($to,'guest',$title,$type);
				$response = array( 'SENT' => true );
				wp_send_json($response);
			} else {
				$response = array( 'NOTSENT' => true );
				wp_send_json($response);
			}
		}
		
		/**
		 * generate pdf file of a post given as parameter
		 * @name generate_post_to_pdf_file
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param int $postID        	
		 */
		function generate_post_to_pdf_file($postID) 
		{
			$logo_width = '';
			$post = get_post ( $postID );
			$title=get_the_title($postID);
			$type=get_post_type($postID);
		 	
			if (! class_exists ( 'TCPDF' )) {
				require_once MWBPDF_GEN_PATH . '/tcpdf_min/tcpdf.php';
			}
			if (! class_exists ( 'pdfheader' )) {
				require_once MWBPDF_GEN_PATH . '/Content/pdf_headercontent.php';
			}
			if (! class_exists ( 'simple_html_dom_node' )) {
				require_once MWBPDF_GEN_PATH . '/Htmlstructure/Htmlstructure.php';
			}
			$post->post_content = apply_filters ( 'the_post_export_content', $post->post_content );
			$post->post_content = wpautop ( $post->post_content );
			$post->post_content = do_shortcode ( $post->post_content );
			if ($this->options['0']['mwb_file_name'] == 'post_name') {
				$filePath = MWB_CACHE_DIR . '/' . $post->post_name . '.pdf';
			} else {
				$filePath = MWB_CACHE_DIR . '/' . $post->ID . '.pdf';
			}
			
			// new PDF document
			if (isset ( $this->options[ '1' ][ 'page_size' ] )) {
				$pagesize = ($this->options['1']['page_size']);
			} else {
				$pagesize = PDF_PAGE_FORMAT;
			}
			if (isset ( $this->options['1']['unitmeasure'] )) {
				$unit = ($this->options['1']['unitmeasure']);
			} else {
				$unit = PDF_UNIT;
			}
			if (isset ( $this->options['1']['page_orientation'] )) {
				$orientation = ($this->options['1']['page_orientation']);
			} else {
				$orientation = PDF_PAGE_ORIENTATION;
			}
			
			$pdf = new CUSTOMPDF ( $orientation, $unit, $pagesize, true, 'UTF-8', false );
			// information about doc
			$pdf->SetCreator ( 'Post to PDF plugin by makewebbetter with ' . PDF_CREATOR );
			$pdf->SetAuthor ( get_bloginfo ( 'name' ) );
			if (! empty ( $this->options['1']['custom_title'] )) {
				$pdf_title = $this->options['1']['custom_title'];
			} else {
				$pdf_title = $post->post_title;
			}			
			// logo width calculation
			if (isset ( $this->options['2']['page_header'] ) and ($this->options['2']['page_header']) != 'None' and !empty ( $this->options['2']['logo_img_url'] )) {
				
				if ($this->options['2']['page_header'] == "upload-image") {
					$logoImage_url = $this->options['2']['logo_img_url'];
				}
				$infologo = getimagesize ( $logoImage_url );
				if (isset ( $this->options['2']['image_factor'] )) {
					$logo_width = ( int ) (($this->options['2']['image_factor'] * $infologo [0]) / $infologo [1]);
				} else {
					$logo_width = ( int ) ((12 * $infologo [0]) / $infologo [1]);
				}
				// for PHP 5.4 or below set default header data
				if (version_compare ( phpversion (), '5.4.0', '<' )) {
					$pdf->SetHeaderData ( $logoImage_url, $logo_width, html_entity_decode ( get_bloginfo ( 'name' ), ENT_COMPAT | ENT_QUOTES ), html_entity_decode ( get_bloginfo ( 'description' ) . "\n" . home_url (), ENT_COMPAT | ENT_QUOTES ), $header_text_color, $header_line_color );
				} else {
					$pdf->SetHeaderData ( $logoImage_url, $logo_width, html_entity_decode ( get_bloginfo ( 'name' ), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES ), html_entity_decode ( get_bloginfo ( 'description' ) . "\n" . home_url (), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES )  );
				}
			}
			if (isset ( $this->options['2']['page_header'] ) and ($this->options['2']['page_header']) == 'None') {
				$pdf->setPrintHeader(false);
			}
			// set header and footer fonts
			if (($this->options['2']['header_font_size']) > 0) {
				$header_font_size = $this->options['2']['header_font_size'];
			} else {
				$header_font_size = 10;
			}
			if (isset($this->options['footer_font_size']) && !empty($this->options['footer_font_size']) && ($this->options['footer_font_size']) > 0) {
				$footer_font_size = $this->options ['footer_font_size'];
			} else {
				$footer_font_size = 10;
			}
			$header_font_pdf_value_dis = isset ( $this->options['2']['header_font_pdf'] ) ? $this->options['2']['header_font_pdf'] : 'helvetica';

			$pdf->setHeaderFont ( array (
					$header_font_pdf_value_dis,
					'',
					$header_font_size 
			) );
		 
			$header_font_pdf_value_dis = isset ( $this->options ['footer_font_pdf'] ) ? $this->options ['footer_font_pdf'] : 'helvetica';

			$pdf->setFooterFont ( array (
					$header_font_pdf_value_dis ,
					'',
					$footer_font_size 
			) );
			
			$pdf->SetDefaultMonospacedFont ( PDF_FONT_MONOSPACED );
			
			if (isset($this->options['1']['margin_left'])) {
				$pdf->SetLeftMargin ( $this->options['1']['margin_left'] );
			} else {
				$pdf->SetLeftMargin ( PDF_MARGIN_LEFT );
			}
			
			if (isset($this->options['1']['margin_right'] )) {
				$pdf->SetRightMargin ( $this->options['1']['margin_right'] );
			} else {
				$pdf->SetRightMargin ( PDF_MARGIN_RIGHT );
			}
			
			if (isset($this->options['1']['margin_top'] )) {
				$pdf->SetTopMargin ( $this->options ['1']['margin_top'] );
			} else {
				$pdf->SetTopMargin ( PDF_MARGIN_TOP );
			}
			if ((isset($this->options['2']['logomTop']) )) {
				$pdf->SetHeaderMargin ( $this->options['2']['logomTop'] );
			} else {
				$pdf->SetHeaderMargin ( PDF_MARGIN_HEADER );
			}
			
			if (isset($this->options['footer_font_margin'] )) {
				$pdf->SetFooterMargin ( $this->options ['footer_font_margin'] );
				// set auto page breaks
				$pdf->SetAutoPageBreak ( TRUE,  $this->options ['footer_font_margin']  );
			} else {
				$pdf->SetFooterMargin ( PDF_MARGIN_FOOTER );
				// set auto page breaks
				$pdf->SetAutoPageBreak ( TRUE, PDF_MARGIN_FOOTER );
			}
			// set image scale factor
			if ($this->options['1']['image_scale'] > 0) {
				$pdf->setImageScale ( $this->options['1']['image_scale'] );
			} else {
				$pdf->setImageScale ( PDF_IMAGE_SCALE_RATIO );
			}
			
			// set default font subsetting mode
			$pdf->setFontSubsetting ( true );
			
 			$pdf->SetFont ( $this->options['1']['content_font_pdf'], '', $this->options['1']['content_font_size'], '', true );
			
			if (  isset( $this->options['5']['bullet_img_url'] ) && ! empty ( $this->options['5']['bullet_img_url'] )) {

				$temp = $this->options['5']['bullet_img_url'];
				$temp = end ( ( explode ( '/', $temp ) ) );
				$temp = end ( ( explode ( '.', $temp ) ) );
				$listsymbol = 'img|' . $temp . '|' . $this->options['5']['custom_image_width'] . '|' . $this->options['5']['custom_image_height'] . '|' . $this->options['5']['bullet_img_url'];
				$pdf->setLIsymbol ( $listsymbol );
			}
			// Add a page
			// This method has several options, check the source code documentation for more information.			
			
			if ($this->options['4']['fontStretching']) {
				$pdf->setFontStretching($this->options['4']['fontStretching']);
			}
			if ($this->options['4']['fontSpacig']) {
				$pdf->setFontSpacing($this->options['4']['fontSpacig']);
			}
			$page_format = array();
			if (isset($this->options['4']['set_rotation']) && !empty($this->options['4']['set_rotation'])) {
				$page_format['Rotate'] = $this->options['4']['set_rotation'];
			} else {
				$page_format['Rotate'] = 0;
			}
			$page_orientation_value_dis = isset ( $this->options['1']['page_orientation'] ) ? $this->options['1']['page_orientation'] : 'P';

			$pdf->AddPage($page_orientation_value_dis, $page_format, false, false);
        	$html = '';
			if (isset ( $this->options['1']['CustomCSS_option'] )){
				$html = '<style>' . $this->options['1']['Customcss'] . '</style>';
			}
			$html .= "<body>";
			
			$this->options=get_option(MWBPDF_GEN_PREFIX);
			if (!empty ( $this->options['1']['rtl_support'] )) {
				// set some language dependent data:
				$lg = Array();
				$lg['a_meta_charset'] = 'UTF-8';
				$lg['a_meta_dir'] = 'rtl';
				$lg['a_meta_language'] = 'fa';
				$lg['w_page'] = 'page';
					
				// set some language-dependent strings (optional)
				$pdf->setLanguageArray($lg);
				$pdf->setRTL(true);
			}
			if (isset ( $this->options['0']['author_name'] ) and ! $this->options['0']['author_name'] == '') {
				$author_id = $post->post_author;
				$author_meta_key = $this->options['0']['author_name'];
				$author = get_user_meta ( $author_id );
				$html .= '<p><strong>'.__('Author','wp-ultimate-pdf').': </strong>' . $author [$author_meta_key] [0] . '</p>';
			}
				
			if (isset ( $this->options['0']['post_categ'] )) 
			{
				$categories = get_the_category ( $post->ID );
				if ($categories) {
					$html .= '<p><strong>'.__('Categories','wp-ultimate-pdf').': </strong>' . $categories [0]->cat_name . '</p>';
				}
			}
		 
			$posts = get_post_custom($postID);
 
			if (isset ( $this->options['0']['post_tags'] )) {
				$tags = get_the_tags ( $post->the_tags );
				if ($tags) {
					$html .= '<p><strong>'.__('Tagged as','wp-ultimate-pdf').': </strong>';
					foreach ( $tags as $tag ) {
						$tag_link = get_tag_link ( $tag->term_id );
						$html .= '<a href="' . $tag_link . '">' . $tag->name . '</a>';
						if (next ( $tags )) {
							$html .= ', ';
						}
					}
					$html .= '</p>';
				}
			}
			$post_type_val=get_post_type($postID);
			
			if( $post_type_val == 'product') {
				
				$html = $this->get_product_html_body( $postID, $html, $pdf_title, $post, $post_type_val);
			}
			else {

				// Set some content to print
				$html .= "<h1 style=\"text-align:center\">". html_entity_decode ( $pdf_title, ENT_QUOTES ) ."</h1>";
				 
				 
				// Display featured image if set in config on page/post
				
				if (isset ( $this->options['0']['featured'] )) 
				{
					if (has_post_thumbnail ( $post->ID )) 
					{
						$html .= get_the_post_thumbnail ( $post->ID );
					}
				}
				$post_template = isset ( $this->options['1']['post_tmplt'] ) ? $this->options['1']['post_tmplt'] : 'tmplt1';
				if( $post_template == 'tmplt1') {
					$html = $this->get_post_html_body( $postID, $html, $pdf_title, $post, $post_type_val);
				}
				elseif($post_template == 'tmplt2') {
					$pdf->writeHTML ( $html, true, 0, true, 0 );
					$pdf->setEqualColumns(2, 84);
					$html = $this->get_post_html_body( $postID, '', $pdf_title, $post, $post_type_val);
				}
				elseif($post_template == 'tmplt3') {
					$pdf->writeHTML ( $html, true, 0, true, 0 );
					$pdf->setEqualColumns(3, 57);
					$html = $this->get_post_html_body( $postID, '', $pdf_title, $post, $post_type_val);
				}
				
			}
			
			/*
				Setting for displaying ACF field details only
			*/

			if (function_exists('get_fields') && class_exists('acf')) {
			
				$field = get_fields($postID);
  
				if( $field )
				{
					foreach( $field as $field_name => $value )
					{	
						 
						if( !(( isset($this->options['6'][$post_type_val][$field_name]) && ( $this->options['6'][$post_type_val][$field_name]== 1 ) ) || ( isset($this->options['6'][$post_type_val]['_'.$field_name]) && ( $this->options['6'][$post_type_val]['_'.$field_name]== 1 ) )) ){
							continue;
						}
					 
						$field = get_field_object($field_name , $postID );
					 
						//do not need to display password
						if(isset($field['type']) && ($field['type'] == "password")){
							continue;
						}
						//coding to display ACF field image
						if ( isset($field['type']) && ($field['type'] == 'image')) 
						{ 	
							
							if($field['save_format']=='id' && $field["label"] != '' && $field["value"] != "" ){
								$html .= '<p><strong>'.$field["label"] .':</strong><p>';
								$html .=  wp_get_attachment_image( $field["value"] , 'thumbnail');
							}elseif( $field['save_format']=='url' ){

								if( $field["label"] != '' && $field["value"] != ""){

									$html .= '<p><strong>'.$field["label"] .':</strong><a href=" '.$field["value"].'" >'.$field ['value'].'</a><p>';
									 
								}

							}elseif( $field['save_format'] == 'object' && $field["label"] != '' && $field["value"]['id'] != ""){
								 
								if( $field["label"] != '' && $field["value"]['id'] != ""){
								 
									$html .= $field["label"] .':';
									$html .=  wp_get_attachment_image( $field["value"]['id'] , 'thumbnail');
								}
 
							}
					 
							continue;

						}
						  
						//coding for displaying Taxonomy fields

						if( isset($field['type'] ) && ($field['type'] == 'taxonomy')){
							 continue;
						}

					 
						//coding for displaying Google map texual address
						if( isset($field['type'] ) && ($field['type'] == 'google_map')){
							if( $field["label"] != '' && $field["value"]['address'] != ""){

								$html .= '<p><strong>'.$field["label"].': </strong>' . $field["value"]['address'] . '</p> ';
							}
							continue;
						}

						//coding to display Date Picker
						if(isset($field['type']) && ($field['type']=='date_picker')){

							if( $field["label"] != '' && $field["value"] != "" && ($field['date_format'] == 'yymmdd')){
								//convert the date string in proper format
								$newstr = substr_replace($field["value"], ' : ', 4, 0);
								$newstr = substr_replace($newstr, ' : ',9 , 0);

								$html .= '<p><strong>'.$field["label"].': </strong>' . $newstr . '</p> ';
							}
							continue;
						}
						//coding to display radio button details
						if( !is_array($value) && ($field['type'] == "radio") && (isset($field['choices']))){
							if( $field["label"] != '' && $field["value"] != ""){
								$html .= '<p><strong>'.$field["label"].': </strong>' . $field["value"] . '</p> ';
							}
							continue;
						}

						//coding to display links for product and any post details
						if(isset($field) && ($field['type'] == 'page_link')){
							if(is_array($field['value']) && !empty($field['value'])){
								$html .= '<p><strong>'.$field["label"].': </strong></p> ';

								foreach ($field['value'] as $current_url_value) {

									if(isset($current_url_value) && ($current_url_value != '')){
										$html .=  '<p><a href="'.$current_url_value.'">'.$current_url_value.'</a><p>'  ;
									}
								}
							}else{
								if(($field["label"] !='') && ($field['value'] != "")){
									$html .= '<p><strong>'.$field["label"].': </strong></p> ';

									$html .=  '<p><a href="'.$field['value'].'">'.$field['value'].'</a><p>'  ;
								}
								 
							}
							continue;
						}

						//coding to handle post data Object
						if(isset($field) && ( $field['type'] == 'post_object' ) && (!empty($field['value'])) ){
							$acf_fetched_post_value=$field['value'];
							if( isset($acf_fetched_post_value) && !empty($acf_fetched_post_value) && is_array($acf_fetched_post_value)){
								$html .= '<p><strong>'.$field["label"].': </strong></p> ';
								foreach ($acf_fetched_post_value as $acf_fetched_one_post_value) {
									
									  if( (get_permalink( $acf_fetched_one_post_value->ID) != '') && ( get_the_title( $acf_fetched_one_post_value->ID) != '' ) ){
										
									 	 $html .= '<p>'.$acf_fetched_one_post_value->post_type.'  -->  '.'<a href="'.get_permalink( $acf_fetched_one_post_value->ID).'">'.get_the_title( $acf_fetched_one_post_value->ID).'</a></p>';
									 }
								}


							}else{
								 if( (get_permalink( $acf_fetched_post_value->ID) != '') && ( get_the_title( $acf_fetched_post_value->ID) != '' ) ){
									$html .= '<p><strong>'.$field["label"].': </strong></p> ';
								 	
								 	$html .= '<p>'.$acf_fetched_post_value->post_type.'  -->  '.'<a href="'.get_permalink( $acf_fetched_post_value->ID).'">'.get_the_title( $acf_fetched_post_value->ID).'</a></p>';
								 }
							}
							 
							continue;
						}

						//cofing to display text, textarea, email, (password need to block), any field returning simple string
						if(!is_array($value) && (!isset($field['choices'])) && !is_object($value)){
							if( $field["label"] != '' && $field["value"] != ""){
								$html .= '<p><strong>'.$field["label"].': </strong>' . $field["value"] . '</p> ';
							}

						}

						//cofing to display radio button, select and checkbox details
						if(is_array($field) && isset($field['choices']) && !empty($field['choices']) && ($field['type'] != "radio")){
							$html .= '<p><strong>'.$field["label"].': </strong></p> ';

							foreach ($field['value'] as $selected_value) {
								if(isset($field['choices'][ $selected_value ]) && ($field['choices'][ $selected_value ] != ''))

									$html .= '<p>' . $field['choices'][ $selected_value ] . '</p> ';

							}
						}


					}
				}
			}

			/*
			  ACF field details end
			*/
 
			$html .="</body>";
			$dom = new simple_html_dom ();
			$dom->load ( $html );
			
			foreach ($dom->find ('img') as $e) 
			{
				// Start Image check by om
				$exurl = ''; // external streams
				$imsize = FALSE;
				$file = $e->src;
				// check if we are passing an image as file or string
				if ($file [0] === '@') 
				{
					// image from string
					$imgdata = substr ( $file, 1 );
				} 
				else 
				{ // image file
					if ($file {0} === '*') 
					{
						// image as external stream
						$file = substr ( $file, 1 );
						$exurl = $file;
					}
					// check if is local file
					if (! @file_exists ( $file )) 
					{
						// encode spaces on filename (file is probably an URL)
						$file = str_replace ( ' ', '%20', $file );
					}
					if (@file_exists ( $file )) 
					{
						// get image dimensions
						$imsize = @getimagesize ( $file );
					}
					if($imsize === FALSE) 
					{
						$imgdata = TCPDF_STATIC::fileGetContents ( $file );
					}
				}
				if (isset ( $imgdata ) and ($imgdata !== FALSE) and (strpos ( $file, '__tcpdf_img' ) === FALSE)) 
				{
					// check Image size
					$imsize = @getimagesize ( $file );
				}
				if ($imsize === FALSE) 
				{
					$e->outertext = '';
				} 
				else 
				{
					// End Image Check
					if (preg_match ( '/alignleft/i', $e->class )) 
					{
						$imgalign = 'left';
					} 
					elseif (preg_match ( '/alignright/i', $e->class )) 
					{
						$imgalign = 'right';
					}
					elseif (preg_match ( '/aligncenter/i', $e->class )) 
					{
						$imgalign = 'center';
						$htmlimgalign = 'middle';
					} 
					else 
					{
						$imgalign = 'none';
					}
					
					$e->class = null;
					$e->align = $imgalign;
					if (isset ( $htmlimgalign )) 
					{
						$e->style = 'float:' . $htmlimgalign;
					} 
					else 
					{
						$e->style = 'float:' . $imgalign;
					}
					
					if (strtolower ( substr ( $e->src, - 4 ) ) == '.svg') 
					{
						$e->src = null;
						$e->outertext = '<div style="text-align:' . $imgalign . '">[ SVG: ' . $e->alt . ' ]</div><br/>';
					} 
					else 
					{
						$e->outertext = '<div style="text-align:' . $imgalign . '">' . $e->outertext . '</div>';
					}
				}
			}
			/*parsing dom element and passing null action attribute if action attribute is not set */
			foreach ($dom->find ('form') as $e)
			{
				if(!isset($e->attr['action']))
				{
 					$e->action = '';
				}
			} 
			$html = $dom->save ();

			
			$dom->clear ();
			$pdf->setFormDefaultProp ( array (
					'lineWidth' => 1,
					'borderStyle' => 'solid',
					'fillColor' => array (
							255,
							255,
							200 
					),
					'strokeColor' => array (
							255,
							128,
							128 
					) 
			) );
			// Print text using writeHTML
			$pdf->writeHTML ( $html, true, 0, true, 0 );
			if (isset ( $this->options['3']['add_watermark'] )) 
			{
				$no_of_pages = $pdf->getNumPages ();
				for($i = 1; $i <= $no_of_pages; $i ++) 
				{
					$pdf->setPage ( $i );
					// Get the page width/height
					$myPageWidth = $pdf->getPageWidth ();
					$myPageHeight = $pdf->getPageHeight ();
					
					// Find the middle of the page and adjust.
					$myX = ($myPageWidth / 2) - 75;
					$myY = ($myPageHeight / 2) + 25;
					
					// Set the transparency of the text to really light
					$pdf->SetAlpha ( 0.09 );
					
					// Rotate 45 degrees and write the watermarking text
					$pdf->StartTransform ();
					$rotate_degr = isset ( $this->options['3']['rotate_water'] ) ? $this->options['3']['rotate_water'] : '45';
					$pdf->Rotate ( $rotate_degr, $myX, $myY );
					$water_font = isset ( $this->options['3']['water_font'] ) ? $this->options['3']['water_font'] : 'courier';
					$pdf->SetFont ( $water_font, "", 30 );
					$watermark_text = isset ( $this->options['3']['watermark_text'] ) ? $this->options['3']['watermark_text'] : '';
					$pdf->Text ( $myX, $myY, $watermark_text );
					$pdf->StopTransform ();

					// Reset the transparency to default
					$pdf->SetAlpha ( 1 );
				}
			}
			if (isset ( $this->options['3']['add_watermark_image'] )) 
			{
				if (! empty ( $this->options['3']['background_img_url'] )) 
				{
					$no_of_pages = $pdf->getNumPages ();
					for($i = 1; $i <= $no_of_pages; $i ++) 
					{
						$pdf->setPage ( $i );
						$myPageWidth = $pdf->getPageWidth ();
						$myPageHeight = $pdf->getPageHeight ();
						$myX = ($myPageWidth / 2) - 50; // WaterMark Positioning
						$myY = ($myPageHeight / 2) - 40;
						$ImageT = isset ( $this->options['3']['water_img_t'] ) ? $this->options['3']['water_img_t'] : '';
						// Set the transparency of the text to really light
						$pdf->SetAlpha ( $ImageT );
						
						// Rotate 45 degrees and write the watermarking text
						$pdf->StartTransform ();
						$ImageW = isset ( $this->options['3']['water_img_h'] ) ? $this->options['3']['water_img_h'] : '';
						$ImageH = isset ( $this->options['3']['water_img_w'] ) ? $this->options['3']['water_img_w'] : '';
						
						$watermark_img = isset ( $this->options['3']['background_img_url'] ) ? $this->options['3']['background_img_url'] : '';
						$pdf->Image ( $watermark_img, $myX, $myY, $ImageW, $ImageH, '', '', '', true, 150 );
						
						$pdf->StopTransform ();
						
						// Reset the transparency to default
						$pdf->SetAlpha ( 1 );
					}
				}
			}
			// cache directory
			if (! is_dir ( MWB_CACHE_DIR )) 
			{
				mkdir ( MWB_CACHE_DIR, 0755, true );
			}
			$pdf->Output ( $filePath, 'F' );
			global $current_user;
			if(is_user_logged_in())	
			{
				 wp_get_current_user(); 
				 if (isset($this->options['settingname'])) {
					$this->options['settingname'];
				}else{
					$this->options['settingname']='defaul';
				}
				$this->export_installdata ( $current_user->user_email, $current_user->user_nicename,$title,$type,$this->options['settingname']);
			}
			if(!is_user_logged_in())
			{	
				if (isset($this->options['settingname'])) {
					$this->options['settingname'];
				}else{
					$this->options['settingname']='defaul';
				}
				$user='guest';	
				$mail='';
				$this->export_installdata ($mail,$user,$title,$type,$this->options['settingname']);
			}
		}
		public function get_post_html_body( $postID, $html, $pdf_title, $post, $post_type_val ) {
			$post_content = $post->post_content;
			
			if (empty ( $post->post_content )) 
			{
				$post_content = isset ( $this->options['4']['docEntryTpl'] ) ? $this->options['4']['docEntryTpl'] : '';
			}
			
			$html .= htmlspecialchars_decode ( htmlentities ( $post_content, ENT_NOQUOTES, 'UTF-8', false ), ENT_NOQUOTES );


			// Display date if set in config
			if (isset ( $this->options['0']['post_date'] )) {
				$newDate = date ( "d-m-Y", strtotime ( $post->post_date ) );
				$html .= '<p><strong>'.__('Date','wp-ultimate-pdf').': </strong>' . $newDate . '</p>';
			}
			$arr=get_post_custom($postID);	
			unset($arr['_edit_last']);
			unset($arr['_edit_lock']);
			unset($arr['_thumbnail_id']);
			 
			
			foreach ($arr as $key=>$val)
			{ 
			
				if( isset($this->options['6'][$post_type_val][$key]) && !empty($this->options['6'][$post_type_val][$key]) && ($this->options['6'][$post_type_val][$key]==1))
				{ 
				 
					if(isset($this->options['6'][$post_type_val][$key.'name']) && !empty($this->options ['6'][$post_type_val][$key.'name']) && ($this->options ['6'][$post_type_val][$key.'name'] != " ")  && ($this->options ['6'][$post_type_val][$key.'name'] != " ") )
					{  
						if( (null !==(get_post_meta($postID,$key,true))) && (!is_array(get_post_meta($postID,$key,true))) )
						{
							 
							if (function_exists('get_fields') && class_exists('acf')) {
				
								$field = get_fields($postID);
			 					if( (array_key_exists($key, $field)) ){
			 						 continue;
			 					 
			 					}
							}
							$html .= '<p><strong>'.$this->options['6'][$post_type_val][$key.'name'].':'. '</strong>' . get_post_meta($postID,$key,true) . '</p>';
						}
					}
				}
			}
			return $html;
		}

		public function get_product_html_body( $postID, $html, $pdf_title, $post, $post_type_val ) {
			
			
			// Display featured image if set in config on page/post
			$product_template = isset ( $this->options['1']['product_tmplt'] ) ? $this->options['1']['product_tmplt'] : 'tmplt1';
			if( $product_template == 'tmplt1') {
				// Set some content to print
				$html .= "<h1 style=\"text-align:center\">". html_entity_decode ( $pdf_title, ENT_QUOTES ) ."</h1>";
				 
				 
				// Display featured image if set in config on page/post
				
				if (isset ( $this->options['0']['featured'] )) 
				{
					if (has_post_thumbnail ( $post->ID )) 
					{
						$html .= get_the_post_thumbnail ( $post->ID );
					}
				}
				
				$post_content = $post->post_content;
				if (empty ( $post->post_content )) 
				{
					$post_content = isset ( $this->options['4']['docEntryTpl'] ) ? $this->options['4']['docEntryTpl'] : '';
				}
				
				$html .= htmlspecialchars_decode ( htmlentities ( $post_content, ENT_NOQUOTES, 'UTF-8', false ), ENT_NOQUOTES );


				// Display date if set in config
				if (isset ( $this->options['0']['post_date'] )) {
					$newDate = date ( "d-m-Y", strtotime ( $post->post_date ) );
					$html .= '<p><strong>'.__('Date','wp-ultimate-pdf').': </strong>' . $newDate . '</p>';
				}
				$arr=get_post_custom($postID);	
				unset($arr['_edit_last']);
				unset($arr['_edit_lock']);
				unset($arr['_thumbnail_id']);
				 
				
				foreach ($arr as $key=>$val)
				{ 
				
					if( isset($this->options['6'][$post_type_val][$key]) && !empty($this->options['6'][$post_type_val][$key]) && ($this->options['6'][$post_type_val][$key]==1))
					{ 
					 
						if(isset($this->options['6'][$post_type_val][$key.'name']) && !empty($this->options ['6'][$post_type_val][$key.'name']) && ($this->options ['6'][$post_type_val][$key.'name'] != " ")  && ($this->options ['6'][$post_type_val][$key.'name'] != " ") )
						{  
							if( (null !==(get_post_meta($postID,$key,true))) && (!is_array(get_post_meta($postID,$key,true))) )
							{
								 
								if (function_exists('get_fields') && class_exists('acf')) {
					
									$field = get_fields($postID);
				 					if( (array_key_exists($key, $field)) ){
				 						 continue;
				 					 
				 					}
								}
								$html .= '<p><strong>'.$this->options['6'][$post_type_val][$key.'name'].':'. '</strong>' . get_post_meta($postID,$key,true) . '</p>';
							}
						}
					}
				}
			}
			else {
				$product_pmeta = isset ( $this->options['1']['pro_pmeta'] ) ? $this->options['1']['pro_pmeta'] : 4;
				$html1="";
				if (isset ( $this->options['0']['featured'] )) 
				{
					if (has_post_thumbnail ( $post->ID )) 
					{
						$html1 .= get_the_post_thumbnail ( $post->ID );
					}
				}
				$html2="";
				// Set some content to print
				$html2 .= "<h1 style=\"text-align:center\">". html_entity_decode ( $pdf_title, ENT_QUOTES ) ."</h1>";
				 
				 
				
				$post_content = $post->post_content;
				if (empty ( $post->post_content )) 
				{
					$post_content = isset ( $this->options['4']['docEntryTpl'] ) ? $this->options['4']['docEntryTpl'] : '';
					$html2 .= htmlspecialchars_decode ( htmlentities ( $post_content, ENT_NOQUOTES, 'UTF-8', false ), ENT_NOQUOTES );
				}
				else {
					$post_excerpt = $post->post_excerpt;
					$html2 .= htmlspecialchars_decode ( htmlentities ( $post_excerpt, ENT_NOQUOTES, 'UTF-8', false ), ENT_NOQUOTES );
				}
				
				
				
				$arr=get_post_custom($postID);	
				unset($arr['_edit_last']);
				unset($arr['_edit_lock']);
				unset($arr['_thumbnail_id']);
				 
				$arr_count = count($arr);
				$count = 0;
				$flag = false;
				$counter = 0;
				foreach ($arr as $key=>$val)
				{ 
					
					if( isset($this->options['6'][$post_type_val][$key]) && !empty($this->options['6'][$post_type_val][$key]) && ($this->options['6'][$post_type_val][$key]==1))
					{ 				 
						if(isset($this->options['6'][$post_type_val][$key.'name']) && !empty($this->options ['6'][$post_type_val][$key.'name']) && ($this->options ['6'][$post_type_val][$key.'name'] != " ")  && ($this->options ['6'][$post_type_val][$key.'name'] != " ") )
						{  
							if( (null !==(get_post_meta($postID,$key,true))) && (!is_array(get_post_meta($postID,$key,true))) )
							{							 
								if (function_exists('get_fields') && class_exists('acf')) {
					
									$field = get_fields($postID);
				 					if( (array_key_exists($key, $field)) ){
				 						 continue;			 					 
				 					}
								}

								if( $arr_count > $product_pmeta ) {
									$count++;
									$flag = true;
								}	
								$html2 .= '<p><strong>'.$this->options['6'][$post_type_val][$key.'name'].':'. '</strong>' . get_post_meta($postID,$key,true) . '</p>';
							}
						}
					}
					$counter++;	
					if( $count == $product_pmeta) {
						break;
					}
				}
				
				$html .= '<table cellpadding="2" cellspacing="2"><tr><td>';
				$html3="";
				if( $product_template == 'tmplt2') {
					$html3 = $html2."</td><td>".$html1;
				}
				elseif( $product_template == 'tmplt3') {
					$html3 = $html1."</td><td>".$html2;
				}
				$html .= $html3;
				$html .= "</td></tr></table>";
				$html .= "<h2>".__("Description",'wp-ultimate-pdf')."</h2>";
				$html .= htmlspecialchars_decode ( htmlentities ( $post_content, ENT_NOQUOTES, 'UTF-8', false ), ENT_NOQUOTES );
				// Display date if set in config
				if (isset ( $this->options['0']['post_date'] )) {
					$newDate = date ( "d-m-Y", strtotime ( $post->post_date ) );
					$html .= '<p><strong>'.__('Date','wp-ultimate-pdf').': </strong>' . $newDate . '</p>';
				}
				if( $flag ) {
					$f = 0;
					foreach ($arr as $key=>$val)
					{ 
						$f++;
						if($f <= $counter) {
							continue;
						}
						if( isset($this->options['6'][$post_type_val][$key]) && !empty($this->options['6'][$post_type_val][$key]) && ($this->options['6'][$post_type_val][$key]==1))
						{ 				 
							if(isset($this->options['6'][$post_type_val][$key.'name']) && !empty($this->options ['6'][$post_type_val][$key.'name']) && ($this->options ['6'][$post_type_val][$key.'name'] != " ")  && ($this->options ['6'][$post_type_val][$key.'name'] != " ") )
							{  
								if( (null !==(get_post_meta($postID,$key,true))) && (!is_array(get_post_meta($postID,$key,true))) )
								{							 
									if (function_exists('get_fields') && class_exists('acf')) {
						
										$field = get_fields($postID);
					 					if( (array_key_exists($key, $field)) ){
					 						 continue;			 					 
					 					}
									}
									$html .= '<p><strong>'.$this->options['6'][$post_type_val][$key.'name'].':'. '</strong>' . get_post_meta($postID,$key,true) . '</p>';
								}
							}
						}
					}
				}
			}
			
		//	print_r($html);die;
			return $html;
 
		}
		
		/**
		 * adds a link button for selected post types by admin
		 * this function adds different link for different sinerios
		 * adds a checks whether to add link for the post type or not
		 * @name ptp_add_pdf_link
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param string $content        	
		 * @return string
		 */
		function ptp_add_pdf_link($content) 
		{

			if (! isset ( $this->options['front_end'] ) ) {

				return $content;
			}
 
			$button = $this->ptp_pdf_icon();
			if(isset( $this->options['0']['content_placement'] )){
				if ('beforeandafter' == $this->options['0']['content_placement']) 
				{
					$content = '<div style="min-height: 30px;">' . $button . '</div>' . $content . '<div style="min-height: 30px;">' . $button . '</div>';
				}
				elseif ('after' == $this->options['0']['content_placement']) 
				{
					$content = $content . '<div style="min-height: 30px;">' . $button . '</div>';
				} 
				else 
				{
					$content = '<div style="min-height: 30px;">' . $button . '</div>' . $content;
				}
			}	
			return $content;
		}
		
		/**
		* Function to show nothing when shortcode is inactive
		*/
		function ptp_pdf_icon_no_shortcode(){
			//silence is golden
		}

		/**
		 * specifies the link to attach to pdf links
		 * @name ptp_pdf_icon
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @return void|boolean|string
		 */
		function ptp_pdf_icon() 
		{
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
				 if ((!isset ( $this->options['front_end'] )) || is_cart() || is_checkout() || is_shop()) {

					return;
				}
			}
			 
			if ('pdf' == (isset ( $_GET ['format'] ) ? $_GET ['format'] : null)) 
			{

				return;
			}
			global $post;
		 	
			if (! isset ( $this->options['6'][$post->post_type][$post->post_type] ))
				return false;
			
			if ( isset($this->options['0']['link_button']) && !empty($this->options['0']['link_button']) && $this->options['0']['link_button'] == 'default'){
				$linkURL = MWBPDF_GEN_URL . '/asset/images/pdf.png';
			}
			
			else if (!empty ( $this->options['0']['custon_link_url'] )) 
			{
				$linkURL = $this->options['0']['custon_link_url'];
			} 
			else 
			{
				$linkURL = MWBPDF_GEN_URL . '/asset/images/pdf.png';
			}
			if (isset ( $this->options['0']['content_position'] )) 
			{
				if ( $this->options['0']['content_position'] == 'left') 
				{
					$style = 'style="float: left;max-width: 50px;"';
				} 
				else if ( $this->options['0']['content_position'] == 'right') 
				{
					$style = 'style="float: right;max-width: 50px;"';
				}
				else if ( $this->options['0']['content_position'] == 'center') 
				{
					$style = 'style="margin: auto;max-width: 50px;"';
				} 
				else 
				{
					$style = 'max-width: 50px;';
				}
			}
			else 
				{
					$style = 'max-width: 50px;';
				}
			global $post;
			
			if(isset($this->options['show']) && $this->options['show']=='loggedinuser')
			{
				if(isset($this->options['showemailfield']) && $this->options['showemailfield']=='shownonloggedinemail')
				{
					if (is_user_logged_in ())
					{
						if (! is_singular ())
						{
							return '<a target="_blank" rel="noindex,nofollow" href="' . esc_url ( add_query_arg ( 'format', 'pdf', get_permalink ( $post->ID ) ) ) . '" title="'.__('Download PDF','wp-ultimate-pdf').'"><img ' . $style . ' alt="'.__('Download PDF','wp-ultimate-pdf').'" src="' . $linkURL . '"></a>';
						}
						else
						{
							return '<a target="_blank" rel="noindex,nofollow" href="' . esc_url ( add_query_arg ( 'format', 'pdf' ) ) . '" title="'.__('Download PDF','wp-ultimate-pdf').'"><img  width="50px"  ' . $style . ' alt="'.__('Download PDF','wp-ultimate-pdf').'" src="' . $linkURL . '"></a>';
						}
					}
					else
					{
						if (! is_singular ())
						{
							$html = '<input id="' . $post->ID . '" ' . $style . ' src="' . $linkURL . '" alt="#TB_inline?height=230&amp;width=400&amp;inlineId=examplePopup1" title="'.__('Export pdf to your Email','wp-ultimate-pdf').'" class="thickbox export-pdf" type="image" value="'.__('Export to PDF','wp-ultimate-pdf').'" />';
						}
						else
						{
							$html = '<input id="' . $post->ID . '" ' . $style . ' src="' . $linkURL . '" alt="#TB_inline?height=230&amp;width=400&amp;inlineId=examplePopup1" title="'.__('Export pdf to your Email','wp-ultimate-pdf').'" class="thickbox export-pdf" type="image" value="'.__('Export to PDF','wp-ultimate-pdf').'" />';
						}
						return $html;
					}
				}else{
					if (is_user_logged_in ())
					{
						if (! is_singular ())
						{
							return '<a target="_blank" rel="noindex,nofollow" href="' . esc_url ( add_query_arg ( 'format', 'pdf', get_permalink ( $post->ID ) ) ) . '" title="'.__('Download PDF','wp-ultimate-pdf').'"><img ' . $style . ' alt="'.__('Download PDF','wp-ultimate-pdf').'" src="' . $linkURL . '"></a>';
						}
						else
						{
							return '<a target="_blank" rel="noindex,nofollow" href="' . esc_url ( add_query_arg ( 'format', 'pdf' ) ) . '" title="'.__('Download PDF','wp-ultimate-pdf').'"><img  width="50px"  ' . $style . ' alt="'.__('Download PDF','wp-ultimate-pdf').'" src="' . $linkURL . '"></a>';
						}
					}
					else
					{
						$html = "";
					}
				}
			}
			else if($this->options['show']=='guestuser')
			{
				if (! is_singular ())
				{
					return '<a target="_blank" rel="noindex,nofollow" href="' . esc_url ( add_query_arg ( 'format', 'pdf', get_permalink ( $post->ID ) ) ) . '" title="'.__('Download PDF','wp-ultimate-pdf').'"><img ' . $style . ' alt="'.__('Download PDF','wp-ultimate-pdf').'" src="' . $linkURL . '"></a>';
				}
				else
				{
					return '<a target="_blank" rel="noindex,nofollow" href="' . esc_url ( add_query_arg ( 'format', 'pdf' ) ) . '" title="'.__('Download PDF','wp-ultimate-pdf').'"><img  width="50px"  ' . $style . ' alt="'.__('Download PDF','wp-ultimate-pdf').'" src="' . $linkURL . '"></a>';
				}
			}
		}
		
		/**
		 * adds custom interval for cache updation
		 * can add multiple schedules
		 * @name ptp_pdfscheduleduration
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param array $schedules        	
		 * @return multitype:NULL Ambigous <string, mixed>
		 */
		function ptp_pdfscheduleduration($schedules) 
		{
			if (isset ( $this->options['0']['cache_updation_sch'] ) && $this->options['0']['cache_updation_sch'] != 'none') 
			{
				$schedules ['cacheinterval'] = array (
						'interval' => $this->options ['0'] ['cache_updation_sch'],
						'display' => __ ( 'PDF Cache Schedule Interval', 'wp-ultimate-pdf' ) 
				);
				update_option ( 'cron_cacheinterval', $this->options['0']['cache_updation_sch'] );
			}
			return $schedules;
		}
		
		/**
		 * schedule an event to update a cache if cache updation is set by admin
		 * checks if already scheduled cache updation
		 * @name ptp_cronstarter_activation
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_cronstarter_activation() 
		{
			if (isset ( $this->options['0']['cache_updation_sch'] ) && $this->options['0'] ['cache_updation_sch'] != 'none') 
			{
				if (! wp_next_scheduled ( 'schedulecacheupdate' )) 
				{
					wp_schedule_event ( time (), 'cacheinterval', 'schedulecacheupdate' );
				}
			}
		}
		
		/**
		 * deactive scheduled event on plugin deactivation
		 * @name ptp_cronstarter_deactivate
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 */
		function ptp_cronstarter_deactivate() 
		{
			// find out when the last event was scheduled
			$timestamp = wp_next_scheduled ( 'schedulecacheupdate' );
			// unschedule previous event if any
			wp_unschedule_event ( $timestamp, 'schedulecacheupdate' );
		}
		
		/**
		 * updates cache
		 * called by scheduled event
		 * @name ptp_update_cache
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_update_cache() 
		{
			global $wpdb;
			$interval = get_option ( 'cron_cacheinterval_prev' );
			if (empty ( $interval )) 
			{
				$interval = get_option ( 'cron_cacheinterval' );
				update_option ( 'cron_cacheinterval_prev', $interval );
			}
			$post_status = 'publish';
			$date_today = date ( "l" );
			$date_from = date ( "Y-m-d H:i:s", strtotime ( $date_today ) - $interval );
			$post_types = get_post_types ( array (
					'public' => true 
			), 'names' );
			$post_types_all;
			foreach ( $post_types as $post_type ) 
			{
				if (isset ( $this->options['6'][$post_type][$post_type] ))
					$post_types_all .= "'" . $post_type . "',";
			}
			$post_types_all = trim ( $post_types_all, ',' );
			$post_ids = $wpdb->get_col ( $wpdb->prepare ( "SELECT ID FROM $wpdb->posts WHERE post_type IN ( {$post_types_all} ) AND  post_status = %s AND post_date >= %s AND post_date <= %s ", $post_status, $date_from, $date_today ) );
			foreach ( $post_ids as $post_id ) 
			{
				$this->generate_post_to_pdf_file ( $post_id );
			}
		}
		
		/**
		 * hooked for send email to guest users
		 * @name postajax_exportandmail_handle
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function postajax_exportandmail_handle() 
		{
			 
			if (defined ( 'DOING_AJAX' ) && DOING_AJAX) 
			{
				$mwb_email_to = is_email ( sanitize_text_field ( $_POST ['mwb_email_to'] ) );
				$postID = sanitize_text_field ( $_POST ['postid'] );
				if ($mwb_email_to) 
				{
					$this->ptp_email_pdf ( $postID, $mwb_email_to );
				} else {
					$response = array( 'INVALIDEMAIL' => true );
					wp_send_json($response);
				}
			}
			 
		}
		
		/**
		 * creates a table to collect record of pdf generated
		 * @name ptp_export_install
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_export_install() 
		{
			global $wpdb;
			$table_name = $wpdb->prefix . "ExporttoPDFRecord";
			$charset_collate = $wpdb->get_charset_collate ();
			$sql = "CREATE TABLE $table_name (
		   id mediumint(9) NOT NULL AUTO_INCREMENT,
		   title varchar(100) NOT NULL,
		   type varchar(100) NOT NULL,
		   emailid varchar(100) NOT NULL,
		   userName varchar(100) NOT NULL,
		   exportdate datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		   useripaddr varchar(100) NOT NULL,
		   UNIQUE KEY id (id)
		   ) $charset_collate;";
			
			require_once (ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta ( $sql );
			add_option ( "Export_db_version", "1.0" );
		}
		
		/**
		 * put data in the table
		 * @name export_installdata
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 * @param string $emailid        	
		 * @param string $userName        	
		 */
		function export_installdata($emailid, $userName ='guest',$title,$type) 
		{
			// Add some data to table
			global $wpdb;
			$table_name = $wpdb->prefix . "ExporttoPDFRecord";
			$REMOTE_ADDR = $_SERVER ['REMOTE_ADDR'];
			if (! empty ($_SERVER ['X_FORWARDED_FOR'] )) 
			{
				$X_FORWARDED_FOR = explode ( ',', $_SERVER ['X_FORWARDED_FOR'] );
				if (! empty ( $X_FORWARDED_FOR )) 
				{
					$REMOTE_ADDR = trim ( $X_FORWARDED_FOR [0] );
				}
			}			
			/*
			 * Some php environments will use the $_SERVER['HTTP_X_FORWARDED_FOR'] variable to capture visitor address information.
			 */
			elseif (! empty ( $_SERVER ['HTTP_X_FORWARDED_FOR'] )) 
			{
				$HTTP_X_FORWARDED_FOR = explode ( ',', $_SERVER ['HTTP_X_FORWARDED_FOR'] );
				if (! empty ( $HTTP_X_FORWARDED_FOR ))
				{
					$REMOTE_ADDR = trim ( $HTTP_X_FORWARDED_FOR [0] );
				}
			}
			$userIPAddress = preg_replace ( '/[^0-9a-f:\., ]/si', '', $REMOTE_ADDR );
			$wpdb->insert ( $table_name, array (
					'title'=>$title,
					'type'=>$type,
					'emailid' => $emailid,
					'userName' => $userName,
					'exportdate' => current_time ( 'mysql' ),
					'useripaddr' => $userIPAddress
			) );
		}
		
		/**
		 * add custom fonts
		 * @name ptp_add_custom_font
		 * @author makewebbetter <webmaster@makewebbetter.com>
		 * @link http://makewebbetter.com
		 */
		function ptp_add_custom_font() 
		{			
			if ( ! current_user_can( 'manage_options' ) ) 
			{
				return wp_send_json_error( 'You are not allow to do this.' );
			}
			if(isset($_FILES)) {
				if (! class_exists ( 'TCPDF' )) {
					require_once MWBPDF_GEN_PATH . '/tcpdf_min/tcpdf.php';
				}
				$file = $_FILES[0]['tmp_name'];
				$file_name = basename($_FILES[0]['name'], ".ttf");
				if(file_exists($file)) {
					$fontname = TCPDF_FONTS::addTTFfont($file, 'TrueTypeUnicode', '', 96);
				}
				if($fontname) {
					$fonts = get_option('ptp_custom_fonts', array());
					$fonts[$file_name]=$fontname;
					update_option('ptp_custom_fonts', $fonts);
					$fonts = array($fontname, $file_name);
					wp_send_json_success( $fonts );
				}
				else {
					wp_send_json_error( __('Error occured due to wrong file you have added or there may be permission error.','wp-ultimate-pdf') );
				}
			}
		}
	}
	$mwbpdf = new mwbpdf ();
}
?>