<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class mbw_log_list extends WP_List_Table {

	/** Class constructor */
	public function __construct() 
	{
		parent::__construct( [
				'singular' => __( 'Record', 'wp-ultimate-pdf' ), 
				'plural'   => __( 'Records', 'wp-ultimate-pdf' ),
				'ajax'     => false 
				] );
	}

	/**
	 * gets record from db
	 * @name get_records
	 * @author makewebbetter <webmaster@makewebbetter.com> 
	 * @link http://makewebbetter.com
	 */
	public static function get_records( $per_page = 5, $page_number = 1 ) {
		global $wpdb;
		$sql = "SELECT id,title, emailid,userName,type,exportdate from ". $wpdb->prefix ."ExporttoPDFRecord";
		$sql.=' order by exportdate desc';
		$sql .= " LIMIT $per_page";
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;
		$result = $wpdb->get_results( $sql, 'ARRAY_A' );
		return $result;
	}	
	/**
	 * delete record from db
	 * @name delete_record
	 * @author makewebbetter <webmaster@makewebbetter.com>
	 * @link http://makewebbetter.com
	 */
	public static function delete_record($id){
		global $wpdb;
		$wpdb->delete($wpdb->prefix. "ExporttoPDFRecord",['id'=>$id],['%d']);
	}	
	/**
	 * count records
	 * @name record_count
	 * @author makewebbetter <webmaster@makewebbetter.com>
	 * @link http://makewebbetter.com
	 */
	public static function record_count() {
		global $wpdb;
		$sql = "SELECT COUNT(*) FROM ". $wpdb->prefix. "ExporttoPDFRecord";
		return $wpdb->get_var( $sql );
	}
	/**
	 * message when no records available
	 * @name no_items
	 * @author makewebbetter <webmaster@makewebbetter.com>
	 * @link http://makewebbetter.com
	 */
	public function no_items() {
		_e( 'No records avaliable.', 'wp-ultimate-pdf' );
	}
	
	/**
	 * delete record column
	 */
	function column_name( $item ) {
		$title = '<strong>' . $item['name'] . '</strong>';
		return $title;
	}
	/**
	 * delete record from column
	 * @name column_default
	 * @author makewebbetter <webmaster@makewebbetter.com>
	 * @link http://makewebbetter.com 
	 */
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'id':
			case 'emailid':
			case 'userName':
			case 'exportdate':
			case 'useripaddr':
			case 'title':
			case 'type':	
				return $item[ $column_name ];
			default:return print_r( $item, true ); //Show the whole array for troubleshooting purposes
		}
	}
	/**
	 * add checkbox to each row
	 * @name column_cb
	 * @author makewebbetter <webmaster@makewebbetter.com>
	 * @link http://makewebbetter.com
	 */
	function column_cb( $item ) {
		return sprintf('<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['id']);
	}
	
	function column_removelog($item){
		$id=$item['id'];
		return sprintf('<input type="button" value="Remove Log" class="removelg" onclick="removelog('.$id.')" />');
	}
	
	/**
	 * get columns to display table headers
	 * @name get_columns
	 * @author makewebbetter <webmaster@makewebbetter.com>
	 * @link http://makewebbetter.com
	 */
	function get_columns() {
		$columns = [
		'cb'      => '<input type="checkbox" />',
		'title'    => __('Post Name', 'wp-ultimate-pdf'),
		'emailid' => __('User Email', 'wp-ultimate-pdf'),
		'userName'    => __('User Name', 'wp-ultimate-pdf'),
		'type'=>__('Post Type','wp-ultimate-pdf'),
		'exportdate'=>__('Export Date','wp-ultimate-pdf'),
		'removelog'=>__('Remove Log','wp-ultimate-pdf')
		];
		return $columns;
	}
	/**
	 * get bulk action to be performed
	 * @name get_bulk_actions
	 * @author makewebbetter <webmaster@makewebbetter.com>
	 * @link http://makewebbetter.com
	 */
	public function get_bulk_actions() {
		$actions = ['bulk-delete' =>'Delete'];
		return $actions;
	}
	/**
	 * generates table records
	 * @name prepare_items
	 * @author makewebbetter <webmaster@makewebbetter.com>
	 * @link http://makewebbetter.com
	 */
	public function prepare_items() {
		$this->_column_headers = $this->get_column_info();
		/** Process bulk action */
		$this->process_bulk_action();
		$per_page     = $this->get_items_per_page( 'records_per_page', 5 );
		$current_page = $this->get_pagenum();
		$total_items  = self::record_count();
		$this->set_pagination_args( [
				'total_items' => $total_items, 
				'per_page'    => $per_page
				] );
		$this->items = self::get_records( $per_page, $current_page );
	}
	/**
	 * process the chosen bulk action
	 * @name process_bulk_action
	 * @author makewebbetter <webmaster@makewebbetter.com>
	 * @link http://makewebbetter.com
	 */
	public function process_bulk_action() 
	{
		if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )|| ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )) 
		{
			$delete_ids = esc_sql( $_POST['bulk-delete'] );
			foreach ( $delete_ids as $id ) {
				self::delete_record( $id );
			}
		}
	}
}