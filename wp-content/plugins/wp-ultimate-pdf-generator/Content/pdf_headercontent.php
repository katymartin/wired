<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * pdf_headercontent.php
 * 
 * This file contains header functions
 * 
 */
if (! class_exists ( 'TCPDF' )) {
	require_once MWBPDF_GEN_PATH . '/tcpdf/tcpdf.php';
}
if( !isset($this->options['2']['page_header']) || ($this->options['2']['page_header']=="None" )) {
	define('PRINT_HEADER','1');
}
class CUSTOMPDF extends TCPDF {
	/**
	 * General header settings
	 * @name header
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://makewebbetter.com 
	 */
	public function header() {
		if(defined('PRINT_HEADER')) {
			$this->setPrintHeader ( false );
		} else {
			parent::header();
		}
	}

	/**
	 * General Footer settings
	 * @name Footer
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://makewebbetter.com 
	 */
	public function Footer() {
		$options = get_option ( MWBPDF_GEN_PREFIX );
		
		if(isset($options['custom_footer_option'])) {
			if(!empty($options['custom_footer']))
			$this->writeHTMLCell ($options['footer_cell_width'], $options['footer_min_height'], $options['footer_lcornerX'], $options['footer_font_lcornerY'], $options['custom_footer'], '',0, $options['footer_cell_fill'], true, $options['footer_align'], $options['footer_cell_auto_padding']);
				
		} else {
			// call parent footer method for default footer
			parent::Footer();
		}
	}
}