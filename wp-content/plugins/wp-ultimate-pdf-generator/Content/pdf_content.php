<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<div class="wrap">
	 
	<h2> <?php _e('Wp Ultimate Pdf Generator Settings', 'wp-ultimate-pdf'); ?></h2>
	<div class="updated below-h2" id="mwb_pdf_gen_message"></div>
	<div id="gde-tabcontent">
	 
			<div id="gencontent" class="gde-tab gde-tab-active">
				<?php mwbpdf_gen_show_tab('general'); ?>
			</div>
	 
	</div>
</div>
<?php
/**
 * Post Meta settings tab details
 * @name mwbpdf_gen_show_tab
 * @author makewebbetter<webmaster@makewebbetter.com>
 * @link http://makewebbetter.com 
 */
function mwbpdf_gen_show_tab( $name ) {
	if(!session_id()){
 		session_start();
 		 
 	}
	$tabfile = MWBPDF_GEN_PATH . "/libs/mwbpdf-tab-$name.php"; 
	if (file_exists ( $tabfile )) {
		include_once ($tabfile);
	}
}

/**
 * General profile option
 * @name mwbpdf_gen_profile_option
 * @author makewebbetter<webmaster@makewebbetter.com>
 * @link http://makewebbetter.com 
 */
function mwbpdf_gen_profile_option( $option, $value, $label, $helptext = '' ) {
	echo "<option value=\"" . esc_attr ( $value ) . "\"";
	if (! empty ( $helptext )) {
		echo " title=\"" . esc_attr ( $helptext ) . "\"";
	}
	if ($option == $value) {
		echo ' selected="selected"';
	}
	echo ">$label &nbsp;</option>\n";
}

/**
 * General profile checkbox
 * @name mwbpdf_gen_profile_checkbox
 * @author makewebbetter<webmaster@makewebbetter.com>
 * @link http://makewebbetter.com 
 */
function mwbpdf_gen_profile_checkbox($val, $field, $default='1', $wrap='',  $label= '', $br = '', $disabled = false ) {
	if (! empty ( $wrap )) {
		echo '<span id="' . esc_attr ( $wrap ) . '">';
	}
	echo '<input type="checkbox" id="' . esc_attr ( $field ) . '" name="' . esc_attr ( $field ) . '"';
	if (($val == $default) || ($disabled)) {
		echo ' checked="checked"';
	}
	if ($disabled) {
		// used only for dx logging option due to global override in functions.php
		echo ' disabled="disabled"';
	}
	
	echo ' value="' . esc_attr ( $default ) . '"> <label for="' . esc_attr ( $field ) . '">' . htmlentities ( $label ) . '</label>';
	if (! empty ( $br )) {
		echo '<br/>';
	}
	if (! empty ( $wrap )) {
		echo '</span>';
	}
}