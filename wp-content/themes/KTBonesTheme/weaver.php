<?php
/**
 Template Name: Weaver
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?><?php get_header(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>the Urban Weaver | WiRED Properties</title>
	<meta  name="description" content="The Urban Weaver is a currated series of original articles and blogs by WiRED Properties.">
</head>
<body>
	<div id="wrapper">
	<section class="millIntro innerWrap" data-aos="fade-up">
		<h1><?php the_field('page_title') ;?><svg class="accentT" viewbox="0 0 29 32" xmlns="http://www.w3.org/2000/svg">
		<path class="tPath" d="M4.5 3.6V28L25 15.8z"></path></svg></h1><?php
		        if( have_rows('col_call') ):
		        while( have_rows('col_call') ): the_row(); ?>
		<div class="pCols">
			<div>
				<p><?php the_sub_field('pcol_left') ;?></p>
			</div>
			<div>
				<p><?php the_sub_field('pcol_right') ;?></p>
			</div>
		</div><?php endwhile; endif; ?>
	</section>


	<section class="featuredWeaver" data-aos="fade-left">
	<?php
		        $args = array(
		                'post_type' => 'weaver',
		                'post_status' => 'publish',
		                'posts_per_page' => 1,
		                'order' => 'DESC'
		        );
		        $projects_loop = new WP_Query($args);
		        if ($projects_loop->have_posts()):
		                while ($projects_loop->have_posts()):
		                        $projects_loop->the_post();

		                        $title    = get_the_title();
		                        $readM    = get_field('read_m');
		                        $link    = get_the_permalink();
		        ?><?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
		<div class="backImg" data-aos="fade-left" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
			<div class="weInfo">
				<h2 class="issueNo"><?php echo $title;?></h2><a href="<?php echo $link;?>">
				<h4><?php echo $readM;?> ></h4></a>
			</div>
		</div><?php
		                    endwhile;
		                wp_reset_postdata();
		            endif;
		             ?>
	</section>
	<section class="subWeaver" data-aos="fade-up">
		<?php
		        $args = array(
		                'post_type' => 'weaver',
		                'post_status' => 'publish',
		                'posts_per_page' => 2,
		                'offset' => 1,
		                'order' => 'DESC'
		        );
		        $projects_loop = new WP_Query($args);
		        if ($projects_loop->have_posts()):
		                while ($projects_loop->have_posts()):
		                        $projects_loop->the_post();

		                        $title    = get_the_title();
		                        $readM    = get_field('read_m');
		                        $link    = get_the_permalink();
		        ?><?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
		<div class="backImg" data-aos="fade-up" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
			<div class="weInfo">
				<h2 class="issueNo"><?php echo $title;?></h2><a href="<?php echo $link;?>">
				<h4><?php echo $readM;?> ></h4></a>
			</div>
		</div><?php
		                    endwhile;
		                wp_reset_postdata();
		            endif;
		             ?>
	</section>


	<section class="WeaverblogGrid">
		<h1><?php the_field('blog_section_header'); ?></h1>
		<div class="blogGridPosts">
			<?php
			        $args = array(
			                'post_type' => 'weaver',
			                'post_status' => 'publish',
			                'posts_per_page' => 100,
			                'offset' => 3,
			                'order' => 'DESC'
			        );
			        $projects_loop = new WP_Query($args);
			        if ($projects_loop->have_posts()):
			                while ($projects_loop->have_posts()):
			                        $projects_loop->the_post();

			                        $title    = get_the_title();
			                        $readM    = get_field('read_m');
			                        $link    = get_the_permalink();
			        ?><?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
			<div class="backImg" data-aos="fade-up" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
				<div class="weInfo">
					<h2 class="issueNo"><?php echo $title;?></h2><a href="<?php echo $link;?>">
					<h4><?php echo $readM;?> ></h4></a>
				</div>
			</div><?php
			                    endwhile;
			                wp_reset_postdata();
			            endif;
			             ?>

		</div>
	</section>
</div>
<div class="emailSignUp">
	<h3>Sign up to receive the Urban Weaver!</h3>
	<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="https://wiredproperties.us17.list-manage.com/subscribe/post?u=dcf6936ff96b2e07460609f87&amp;id=79e72fd8f6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_dcf6936ff96b2e07460609f87_79e72fd8f6" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->
</div>


</body>
	<?php get_footer(); ?>
</html>
