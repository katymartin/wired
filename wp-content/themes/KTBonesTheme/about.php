<?php
/**
 Template Name: About
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>About | WiRED Properties</title>
	<meta name="description" content="Learn more about our firm, our philosophy and meet the team.">
</head>

<body>
	<div id="wrapper">
	<div class="animationMobile" ><?php include 'cultcomm_mobile.php' ;?> </div>
	<div class="animationDesk"><?php include 'cultcommunity.php' ;?> </div>
<section class="aboutSec" id="greyBack" >
	<div class="innerWrap" data-aos="fade-up">
		<h1><?php the_field('aboutS_title1'); ?><svg class="accentT" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 32"><path class="tPath" d="M4.5 3.6V28L25 15.8z"/></svg></h1>
		<h2 id="orange"><?php the_field('aboutS_subtitle1'); ?></h2>
		<h4 style="font-weight: bold;"><?php the_field('aboutS_callout1'); ?></h4>
		<?php
			if( have_rows('about_copy1') ):
			while( have_rows('about_copy1') ): the_row(); ?>

			<div class="pCols">
				<div>
					<p><?php the_sub_field('left_about') ;?></p>
				</div>
				<div >
					<p><?php the_sub_field('right_about') ;?></p>
				</div>
			</div>

			<?php endwhile; endif; ?>

</div>
</section>
<div class="animationMobile"><?php include 'weaveran_mobile.php' ;?> </div>
<div class="animationDesk"><?php include 'weaveranimation.php' ;?> </div>

<section id="greyBack">
<div class="innerWrap" data-aos="fade-up">
	<h1><?php the_field('aboutS_title2'); ?><svg class="accentT" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 32"><path class="tPath" d="M4.5 3.6V28L25 15.8z"/></svg></h1>
	<h2 id="orange"><?php the_field('aboutS_subtitle2'); ?></h2>
	<h4 style="font-weight: bold;"><?php the_field('aboutS_callout2'); ?></h4>
	<?php
		if( have_rows('about_copy2') ):
		while( have_rows('about_copy2') ): the_row(); ?>

		<div class="pCols">
			<div>
				<p><?php the_sub_field('left_about') ;?></p>
			</div>
			<div >
				<p><?php the_sub_field('right_about') ;?></p>
			</div>
		</div>

		<?php endwhile; endif; ?>
</div>
</div>
</section>

<section class="meetTeam" data-aos="fade-up">
		<div class="topTeamWrap">
			<h1><?php the_field('team_title'); ?><svg class="accentT" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 32"><path class="tPath" d="M4.5 3.6V28L25 15.8z"/></svg></h1>

<!--
		<div class="headshots flex">
			<img  class="flexDiv hShot1" src="< ?php the_field('headshot1'); ?>" />
			<img 	class="flexDiv hShot2 hShot2active" src="< ?php the_field('headshot2'); ?>" />
			<img  class="flexDiv hShot3" src="< ?php the_field('headshot3'); ?>" />
		</div>
-->

	<div class="headshots flex">
		<div  class="flexDiv hShot1" /></div>
		<div 	class="flexDiv hShot2 hShot2active" /></div>
		<div  class="flexDiv hShot3" /></div>
	</div>


		<div class="empTitles flex">
			<div class="flexDiv empT1">
				<h3><?php the_field('employee1'); ?></h3>
				<h5><?php the_field('position1'); ?></h5>
			</div>
			<div class="flexDiv empT2 empActive2">
				<h3><?php the_field('employee2'); ?></h3>
				<h5><?php the_field('position2'); ?></h5>
			</div>
			<div class="flexDiv empT3">
				<h3><?php the_field('employee3'); ?></h3>
				<h5><?php the_field('position3'); ?></h5>
			</div>
		</div>
	</div>



	<div class="bios flex">
		<?php
			if( have_rows('bio_copy1') ):
			while( have_rows('bio_copy1') ): the_row(); ?>

			<div class="pCols">
				<div class="bio1">
					<p><?php the_sub_field('left_bio') ;?></p>
				</div>
				<div class="bio1">
					<p><?php the_sub_field('right_bio') ;?></p>
				</div>
			</div>

			<?php endwhile; endif; ?>

			<?php
				if( have_rows('bio_copy2') ):
				while( have_rows('bio_copy2') ): the_row(); ?>

				<div class="pCols">
					<div class="bio2 bioActive2">
						<p><?php the_sub_field('left_bio') ;?></p>
					</div>
					<div class="bio2 bioActive2">
						<p><?php the_sub_field('right_bio') ;?></p>
					</div>
				</div>

				<?php endwhile; endif; ?>

				<?php
					if( have_rows('bio_copy3') ):
					while( have_rows('bio_copy3') ): the_row(); ?>

					<div class="pCols">
						<div class="bio3">
							<p><?php the_sub_field('left_bio') ;?></p>
						</div>
						<div class="bio3">
							<p><?php the_sub_field('right_bio') ;?></p>
						</div>
					</div>

					<?php endwhile; endif; ?>
</div>




</div>

</section>

<div>
</body>


<?php get_footer(); ?>
