<?php
/*
 * CUSTOM POST TYPE TEMPLATE
 *
 * This is the custom post type post template. If you edit the post type name, you've got
 * to change the name of this template to reflect that name change.
 *
 * For Example, if your custom post type is "register_post_type( 'bookmarks')",
 * then your single template should be single-bookmarks.php
 *
 * Be aware that you should rename 'custom_cat' and 'custom_tag' to the appropiate custom
 * category and taxonomy slugs, or this template will not finish to load properly.
 *
 * For more info: http://codex.wordpress.org/Post_Type_Templates
*/
?>

<?php get_header(); ?>

<div id="wrapper">
<div id="blogPost">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	<div class="backImg" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; " data-aos="fade-up" >
</div>

<div class="weaverContent">
	<div class="weaverTitle">
		<div class="weaveUp ">
			<div class="wM"><?php include 'urbanan_mobile.php' ;?></div>
			<div class="wD"><?php include 'urbananimation.php' ;?></div>
		</div>
		<div class="weaveLow">
		<h1><?php the_title();?></h1>
		<?php echo do_shortcode("[dkpdf-button]"); ?>
		</div>

	</div>
<div class="weaverPosts">
<?php the_content();?>
</div>

<!--
<div class="weaverPosts">
	< ?php
		if( have_rows('weaver_article') ):
		while( have_rows('weaver_article') ): the_row(); ?>

		<div class="wPost">
			<img src="< ?php the_sub_field('image_before'); ?>" alt="< ?php the_sub_field('image_before_alt'); ?>">
			<h2>< ?php the_sub_field('weaver_article_title'); ?></h2>
			<p>< ?php the_sub_field('weaver_article_copy'); ?></p>
			<img src="< ?php the_sub_field('image_after'); ?>" alt="< ?php the_sub_field('image_after_alt'); ?>">
		</div>

	< ?php endwhile; endif; ?>
</div>
-->
</div>

<div class="blogImgs">
	<?php
		if( have_rows('blog_imgs') ):
		while( have_rows('blog_imgs') ): the_row(); ?>
		<div class="bImg" style="background: url('<?php the_sub_field('b_img'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; "></div>
	<?php endwhile; endif; ?>
</div>


</div>




	<?php endwhile; ?>
	<?php endif; ?>
	</div>


<?php get_footer(); ?>
