<?php
/**
 Template Name: Resources
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?><?php get_header(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Resources | WiRED Properties</title>
	<meta content="Check out the Urban Weaver, WiRED Reviews and commentaries, and keep up with our updates and press." name="description">
</head>
<body>
	<div id="wrapper">
	<section class="featuredWeaver" data-aos="fade-left">
		<h1><?php the_field('title_1'); ?></h1><?php
		        $args = array(
		                'post_type' => 'weaver',
		                'post_status' => 'publish',
		                'posts_per_page' => 1,
		                'order' => 'DESC'
		        );
		        $projects_loop = new WP_Query($args);
		        if ($projects_loop->have_posts()):
		                while ($projects_loop->have_posts()):
		                        $projects_loop->the_post();

		                        $title    = get_the_title();
		                        $readM    = get_field('read_m');
		                        $link    = get_the_permalink();
		        ?><?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
		<div class="backImg" data-aos="fade-left" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
			<div class="weInfo">
				<h2 class="issueNo"><?php echo $title;?></h2>
				<a href="<?php echo $link;?>"><h4><?php echo $readM;?> ></h4></a>
			</div>
		</div><?php
		                    endwhile;
		                wp_reset_postdata();
		            endif;
		             ?>


								 <div class="miniSignUp">
									 <div class="flex">
										 <div class="flexDiv">
								 	<h3>Sign up to receive the Urban Weaver!</h3>
										</div>
										<div class="flexDiv">
											<!-- Begin MailChimp Signup Form -->
										<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
										<style type="text/css">
											#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
											/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
											   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
										</style>
										<div id="mc_embed_signup">
										<form action="https://wiredproperties.us17.list-manage.com/subscribe/post?u=dcf6936ff96b2e07460609f87&amp;id=79e72fd8f6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
										    <div id="mc_embed_signup_scroll">
											<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
										    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
										    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_dcf6936ff96b2e07460609f87_79e72fd8f6" tabindex="-1" value=""></div>
										    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
										    </div>
										</form>
										</div>

										<!--End mc_embed_signup-->
							 </div>
								 </div>

	</section>
	<section class="subWeaver" data-aos="fade-up">
		<?php
		        $args = array(
		                'post_type' => 'weaver',
		                'post_status' => 'publish',
		                'posts_per_page' => 2,
		                'offset' => 1,
		                'order' => 'DESC'
		        );
		        $projects_loop = new WP_Query($args);
		        if ($projects_loop->have_posts()):
		                while ($projects_loop->have_posts()):
		                        $projects_loop->the_post();

		                        $title    = get_the_title();
		                        $readM    = get_field('read_m');
		                        $link    = get_the_permalink();
		        ?><?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
		<div class="backImg" data-aos="fade-up" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
			<div class="weInfo">
				<h2 class="issueNo"><?php echo $title;?></h2>
				<a href="<?php echo $link;?>"><h4><?php echo $readM;?> ></h4></a>
			</div>
		</div><?php
		                    endwhile;
		                wp_reset_postdata();
		            endif;
		             ?>


	</section>
	<a class="seeAll" data-aos="zoom-in" href="/weaver/"><h3>See All</h3></a>
	<section class="resSec">
		<h1><?php the_field('title_2'); ?></h1>
		<div class="otherPosts">
			<?php
			                $args = array(
			                        'post_type' => 'reviews',
			                        'post_status' => 'publish',
			                        'posts_per_page' => 2,
			                        'order' => 'DESC'
			                );
			                $projects_loop = new WP_Query($args);
			                if ($projects_loop->have_posts()):
			                        while ($projects_loop->have_posts()):
			                                $projects_loop->the_post();

			                                $title    = get_the_title();
			                                $play     = get_field('play_button');
			                                $playAlt  = get_field('play_alt');
			                                $excerpt  = get_the_excerpt();
			                                $readM    = get_field('read_m');
			                                $link    = get_the_permalink();
			                ?><?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
			<div class="backImg" data-aos="fade-up" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
				<a href="<?php echo $link;?>"><img alt="<?php echo $playAlt; ?>" class="logoPrev" src="<?php echo $play;?>"></a>
				<div class="aInfo">
					<h2><?php echo $title;?></h2>
					<p><?php echo $excerpt;?><a href="<?php echo $link;?>"><?php echo $readM;?> ></a></p>
				</div>
			</div><?php
			                    endwhile;
			                wp_reset_postdata();
			            endif;
			             ?>
		</div>

		<a class="seeAll" data-aos="zoom-in" href="/reviews/"><h3>See All</h3></a>
	</section>



	<section style="margin-bottom: 100px;" class="resSec">
		<h1><?php the_field('title_3'); ?></h1>
		<div class="otherPosts">
			<?php
			                $args = array(
			                        'post_type' => array('press', 'updates'),
			                        'post_status' => 'publish',
			                        'posts_per_page' => 2,
			                        'order' => 'DESC'
			                );
			                $projects_loop = new WP_Query($args);
			                if ($projects_loop->have_posts()):
			                        while ($projects_loop->have_posts()):
			                                $projects_loop->the_post();

			                                $title    = get_the_title();
			                                $play     = get_field('play_button');
			                                $playAlt  = get_field('play_alt');
			                                $excerpt  = get_the_excerpt();
			                                $link     = get_the_permalink();
			                                $extLink  = get_field('cta_link');
			                                $extCTA   = get_field('cta_button');
			                                $updateB  = get_field('update_button');
			                ?><?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
			<div class="backImg" data-aos="fade-up" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
				<a href="<?php echo $link;?>"><img alt="<?php echo $playAlt; ?>" class="logoPrev" src="<?php echo $play;?>"></a>
				<div class="aInfo">
					<h2><?php echo $title;?></h2>
					<p><?php echo $excerpt;?></p>
					<p><a href="<?php echo $extLink;?>" target="_blank"><?php echo $extCTA;?></a></p>
					<p><a href="<?php echo $link;?>"><?php echo $updateB;?></a></p>
				</div>
			</div><?php
			                    endwhile;
			                wp_reset_postdata();
			            endif;
			             ?>
		</div>
		<a class="seeAll" data-aos="zoom-in" href="/updates/"><h3>See All</h3></a>
	</section>

</div>
</body>

	<?php get_footer(); ?>
</html>
