<div class="cultWrap">
	
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1357 469" style="enable-background:new 0 0 1357 469;" xml:space="preserve">

<g>
	<polygon class="otherLine" points="298.5,372.3 298.5,343.3 258.6,343.3 258.6,456.9 298.5,456.9 334,456.9 334,372.3 	"/>
	<line class="otherLine" x1="367" y1="456.9" x2="367" y2="439.4"/>
	<circle class="otherLine" cx="367.2" cy="428.7" r="10.7"/>
	<line class="otherLine" x1="356.2" y1="456.9" x2="356.2" y2="443"/>
	<circle class="cityBlocks" cx="356.4" cy="435.6" r="8.5"/>
	<rect x="897.7" y="442.5" class="cityBlocks" width="5.8" height="14.4"/>
	<line class="cityBlocks" x1="892" y1="442.5" x2="909.2" y2="442.5"/>
	<line class="cityBlocks" x1="910" y1="449.2" x2="918.6" y2="449.2"/>
	<line class="cityBlocks" x1="914.3" y1="449.2" x2="914.3" y2="456.9"/>
	<line class="cityBlocks" x1="884" y1="449.2" x2="892.7" y2="449.2"/>
	<line class="cityBlocks" x1="888.4" y1="449.2" x2="888.4" y2="456.9"/>
	<rect x="959.7" y="442.5" class="cityBlocks" width="5.8" height="14.4"/>
	<line class="cityBlocks" x1="953.9" y1="442.5" x2="971.2" y2="442.5"/>
	<line class="cityBlocks" x1="971.9" y1="449.2" x2="980.5" y2="449.2"/>
	<line class="cityBlocks" x1="976.2" y1="449.2" x2="976.2" y2="456.9"/>
	<line class="cityBlocks" x1="946" y1="449.2" x2="954.6" y2="449.2"/>
	<line class="cityBlocks" x1="950.3" y1="449.2" x2="950.3" y2="456.9"/>
	<line class="cityBlocks" x1="933.6" y1="456.9" x2="933.6" y2="428.8"/>
	<path class="cityBlocks" d="M911,429.7h45.3c0,0-7-17.3-22.6-17.3C917.9,412.4,911,429.7,911,429.7z"/>
	<rect x="1177.7" y="442.5" class="cityBlocks" width="5.8" height="14.4"/>
	<line class="cityBlocks" x1="1172" y1="442.5" x2="1189.2" y2="442.5"/>
	<line class="cityBlocks" x1="1190" y1="449.2" x2="1198.6" y2="449.2"/>
	<line class="cityBlocks" x1="1194.3" y1="449.2" x2="1194.3" y2="456.9"/>
	<line class="cityBlocks" x1="1164" y1="449.2" x2="1172.7" y2="449.2"/>
	<line class="cityBlocks" x1="1168.4" y1="449.2" x2="1168.4" y2="456.9"/>
	<rect x="1239.7" y="442.5" class="cityBlocks" width="5.8" height="14.4"/>
	<line class="cityBlocks" x1="1233.9" y1="442.5" x2="1251.2" y2="442.5"/>
	<line class="cityBlocks" x1="1251.9" y1="449.2" x2="1260.5" y2="449.2"/>
	<line class="cityBlocks" x1="1256.2" y1="449.2" x2="1256.2" y2="456.9"/>
	<line class="cityBlocks" x1="1226" y1="449.2" x2="1234.6" y2="449.2"/>
	<line class="cityBlocks" x1="1230.3" y1="449.2" x2="1230.3" y2="456.9"/>
	<line class="cityBlocks" x1="1213.6" y1="456.9" x2="1213.6" y2="428.8"/>
	<path class="cityBlocks" d="M1191,429.7h45.3c0,0-7-17.3-22.6-17.3S1191,429.7,1191,429.7z"/>
	<rect x="1053" y="355.5" class="cityBlocks" width="36" height="101.4"/>
	<rect x="1089" y="391.5" class="cityBlocks" width="58" height="65"/>
	<rect x="281.5" y="429.1" class="otherLine" width="29" height="27.8"/>
	<rect x="996.4" y="402.1" class="cityBlocks" width="66.6" height="54.7"/>
	<line class="otherLine" x1="235.8" y1="423" x2="235.8" y2="457"/>
	<g>
		<polyline class="otherLine" points="242.8,424.5 242.8,422.5 228.8,422.5 228.8,424.5 		"/>
		<circle class="otherLine" cx="228.2" cy="427" r="2.5"/>
		<circle class="otherLine" cx="243.2" cy="427" r="2.5"/>
	</g>
	<line class="otherLine" x1="157.8" y1="423" x2="157.8" y2="457"/>
	<g>
		<polyline class="otherLine" points="164.8,424.5 164.8,422.5 150.8,422.5 150.8,424.5 		"/>
		<circle class="otherLine" cx="150.2" cy="427" r="2.5"/>
		<circle class="otherLine" cx="165.2" cy="427" r="2.5"/>
	</g>
	<rect x="996.4" y="423.1" class="cityBlocks" width="27.5" height="33.8"/>
	<rect x="1119.4" y="423.1" class="cityBlocks" width="27.5" height="33.8"/>
	<rect x="1057.4" y="364.5" class="cityBlocks" width="11.3" height="27.5"/>
	<rect x="1073.2" y="364.5" class="cityBlocks" width="11.3" height="27.5"/>
	<rect x="63.7" y="281.5" class="otherLine" width="67" height="175"/>
	<rect x="9" y="338.5" class="cityBlocks" width="87" height="118"/>
	<rect x="73.2" y="274.5" class="cityBlocks" width="48" height="7"/>
	<polygon class="cityBlocks" points="82.1,274.5 112.3,274.5 97.6,253.5 	"/>
	<line class="cityBlocks" x1="298.5" y1="364" x2="331.5" y2="364"/>
	<line class="cityBlocks" x1="305.8" y1="364" x2="305.8" y2="372.3"/>
	<line class="cityBlocks" x1="313.1" y1="364" x2="313.1" y2="372.3"/>
	<line class="cityBlocks" x1="320.4" y1="364" x2="320.4" y2="372.3"/>
	<line class="cityBlocks" x1="327.8" y1="364" x2="327.8" y2="372.3"/>
	<rect x="14" y="349.5" class="cityBlocks" width="77" height="25"/>
	<rect x="14" y="379.5" class="cityBlocks" width="77" height="25"/>
	<rect x="14" y="409.5" class="cityBlocks" width="77" height="25"/>
	<rect x="14" y="440.5" class="cityBlocks" width="19" height="10"/>
	<rect x="72" y="440.5" class="cityBlocks" width="19" height="10"/>
	<rect x="38" y="421.5" class="cityBlocks" width="29" height="35"/>
	<rect x="262" y="346.5" class="cityBlocks" width="5.6" height="26"/>
	<rect x="271.3" y="346.5" class="cityBlocks" width="5.6" height="26"/>
	<rect x="280.6" y="346.5" class="cityBlocks" width="5.6" height="26"/>
	<rect x="289.9" y="346.5" class="cityBlocks" width="5.6" height="26"/>
	<line class="cityBlocks" x1="261.2" y1="380.5" x2="330.2" y2="380.5"/>
	<line class="cityBlocks" x1="261.2" y1="383.5" x2="330.2" y2="383.5"/>
	<line class="cityBlocks" x1="261.5" y1="419.5" x2="330.5" y2="419.5"/>
	<line class="cityBlocks" x1="261.5" y1="422.5" x2="330.5" y2="422.5"/>
	<rect x="261.5" y="388.5" class="cityBlocks" width="20.7" height="25.5"/>
	<rect x="267.5" y="432" class="cityBlocks" width="6.5" height="19"/>
	<rect x="319.5" y="432" class="cityBlocks" width="6.5" height="19"/>
	<rect x="285.1" y="388.5" class="cityBlocks" width="20.7" height="25.5"/>
	<rect x="308.8" y="388.5" class="cityBlocks" width="20.7" height="25.5"/>
	<line class="cityBlocks" x1="14" y1="344.5" x2="91" y2="344.5"/>
	<line class="cityBlocks" x1="66.5" y1="295.5" x2="127.5" y2="295.5"/>
	<line class="cityBlocks" x1="66.5" y1="312.9" x2="127.5" y2="312.9"/>
	<line class="cityBlocks" x1="66.5" y1="330.2" x2="127.5" y2="330.2"/>
	<line class="cityBlocks" x1="95.5" y1="347.6" x2="127.5" y2="347.6"/>
	<line class="cityBlocks" x1="95.5" y1="365" x2="127.5" y2="365"/>
	<line class="cityBlocks" x1="95.5" y1="382.4" x2="127.5" y2="382.4"/>
	<line class="cityBlocks" x1="95.5" y1="399.8" x2="127.5" y2="399.8"/>
	<line class="cityBlocks" x1="95.5" y1="417.1" x2="127.5" y2="417.1"/>
	<line class="cityBlocks" x1="95.5" y1="434.5" x2="127.5" y2="434.5"/>
	<circle class="cityBlocks" cx="194.5" cy="447" r="6.5"/>
	<circle class="cityBlocks" cx="188" cy="451.5" r="2"/>
	<circle class="cityBlocks" cx="201" cy="451.5" r="2"/>
	<line class="cityBlocks" x1="998.5" y1="394" x2="1052.5" y2="394"/>
	<line class="cityBlocks" x1="1028.1" y1="360" x2="1028.1" y2="402.3"/>
	<line class="cityBlocks" x1="1036.4" y1="394" x2="1036.4" y2="402.3"/>
	<line class="cityBlocks" x1="1044.8" y1="394" x2="1044.8" y2="402.3"/>
	<line class="cityBlocks" x1="1003.1" y1="402.3" x2="1003.1" y2="360"/>
	<line class="cityBlocks" x1="1011.4" y1="394" x2="1011.4" y2="402.3"/>
	<line class="cityBlocks" x1="1019.8" y1="394" x2="1019.8" y2="402.3"/>
	<line class="cityBlocks" x1="1089.5" y1="383" x2="1144.5" y2="383"/>
	<line class="cityBlocks" x1="1113.9" y1="360" x2="1113.9" y2="391.3"/>
	<line class="cityBlocks" x1="1139.1" y1="360" x2="1139.1" y2="391.3"/>
	<line class="cityBlocks" x1="1130.7" y1="383" x2="1130.7" y2="391.3"/>
	<line class="cityBlocks" x1="1122.3" y1="383" x2="1122.3" y2="391.3"/>
	<line class="cityBlocks" x1="1097.1" y1="383" x2="1097.1" y2="391.3"/>
	<line class="cityBlocks" x1="1105.5" y1="383" x2="1105.5" y2="391.3"/>
	<path class="cityBlocks" d="M1003,361.5c0,0,9,7,25,0"/>
	<path class="cityBlocks" d="M1028,361.5c0,0,9,7,25,0"/>
	<path class="cityBlocks" d="M1089,361.5c0,0,9,7,25,0"/>
	<path class="cityBlocks" d="M1114,361.5c0,0,9,7,25,0"/>
	<path class="cityBlocks" d="M1018,368.5c0,1.7-3,3-3,3s-3-1.3-3-3s1.3-3,3-3S1018,366.8,1018,368.5z"/>
	<path class="cityBlocks" d="M1044,368.5c0,1.7-3,3-3,3s-3-1.3-3-3s1.3-3,3-3S1044,366.8,1044,368.5z"/>
	<path class="cityBlocks" d="M1104,368.5c0,1.7-3,3-3,3s-3-1.3-3-3s1.3-3,3-3S1104,366.8,1104,368.5z"/>
	<path class="cityBlocks" d="M1130,368.5c0,1.7-3,3-3,3s-3-1.3-3-3s1.3-3,3-3S1130,366.8,1130,368.5z"/>
	<circle class="cityBlocks" cx="1317" cy="377.5" r="22"/>
	<circle class="cityBlocks" cx="1333" cy="395.5" r="9"/>
	<line class="cityBlocks" x1="1317" y1="399.5" x2="1317" y2="457.2"/>
	<circle class="cityBlocks" cx="1302" cy="417.9" r="10.9"/>
	<line class="cityBlocks" x1="1302" y1="428.7" x2="1302" y2="457.2"/>
	<line class="cityBlocks" x1="1328.3" y1="403.2" x2="1317" y2="413.3"/>
	<circle class="cityBlocks" cx="1290.5" cy="449" r="7.5"/>
	<circle class="cityBlocks" cx="1280.5" cy="452.5" r="4"/>
	<g>
		<line class="weaveLine" x1="350.5" y1="97.4" x2="1112.5" y2="97.4"/>
		<line class="weaveLine" x1="213.5" y1="110.9" x2="979.5" y2="110.9"/>
		<line class="weaveLine" x1="290.5" y1="124.5" x2="1056.5" y2="124.5"/>
		<polyline class="weaveLine" points="442.9,10.5 442.9,384 509.6,384 		"/>
		<polyline class="weaveLine" points="471,47 471,360 505.7,360 		"/>
		<line class="weaveLine" x1="498.8" y1="29" x2="499" y2="341.6"/>
		<line class="weaveLine" x1="526.8" y1="45" x2="526.8" y2="341.6"/>
		<line class="weaveLine" x1="554.8" y1="73.8" x2="554.8" y2="329.8"/>
		<line class="weaveLine" x1="610.8" y1="73" x2="610.8" y2="303.4"/>
		<line class="weaveLine" x1="666.8" y1="71" x2="666.8" y2="301.5"/>
		<line class="weaveLine" x1="694.7" y1="61.5" x2="694.7" y2="312.8"/>
		<polyline class="weaveLine" points="750.7,60 750.7,297.7 750.7,341.6 		"/>
		<polyline class="weaveLine" points="839.7,14 839.7,384 772.9,384 		"/>
		<polyline class="weaveLine" points="811.5,63 811.5,360 776.8,360 		"/>
		<line class="weaveLine" x1="783.7" y1="72" x2="783.5" y2="341.6"/>
		<line class="weaveLine" x1="867.6" y1="46" x2="867.5" y2="446"/>
		<line class="weaveLine" x1="582.8" y1="64.4" x2="582.8" y2="313.8"/>
		<line class="weaveLine" x1="638.8" y1="67.2" x2="638.8" y2="294.9"/>
		<line class="weaveLine" x1="722.7" y1="73" x2="722.7" y2="327.9"/>
		<line class="weaveLine" x1="414.9" y1="34" x2="415" y2="446"/>
		<rect x="406.4" y="209.6" class="whiteBlock" width="467.6" height="65.5"/>
		<rect x="406.4" y="209.6" class="weaveLine" width="467.6" height="65.5"/>
		<g>
			<path class="weaveGrey" d="M416.7,102.2V81.8h12.6v18.8c0,3.2,1.4,4.7,4.2,4.7s4.2-1.5,4.2-4.7V81.8h12.6v20.4c0,9-7.3,13.8-16.8,13.8
				S416.7,111.2,416.7,102.2z"/>
			<path class="weaveGrey" d="M454.8,81.8h18.9c8.6,0,12.7,5.4,12.7,12.6c0,4.5-2.7,8-6,10.2c0.7,0.5,1.1,1.1,1.4,1.7l4,9.1h-13.2
				l-3.3-7.5c-0.4-0.8-0.7-1.1-1.5-1.1h-0.5v8.6h-12.6V81.8z M470.5,97c2,0,3.1-0.8,3.1-2.6c0-1.9-0.9-2.7-2.9-2.7h-3.3V97H470.5z"
				/>
			<path class="weaveGrey" d="M490.1,81.8h20c6.9,0,10.9,3.4,10.9,9.5c0,3.3-1.3,5.7-3.1,7.1c2.6,1.3,4.3,2.9,4.3,6.8
				c0,6.3-4.4,10.2-11.8,10.2h-20.2V81.8z M506.6,94.7c1.7,0,2.5-0.7,2.5-2.2c0-1.3-0.8-2.1-2.5-2.1h-4.7v4.3H506.6z M506.6,106.9
				c1.7,0,2.5-0.7,2.5-2.2c0-1.3-0.8-2.1-2.5-2.1h-4.7v4.3H506.6z"/>
			<path class="weaveGrey" d="M534.2,81.8h13.1l13,33.6h-13.2l-2.4-6.3h-7.9l-2.4,6.3h-13.2L534.2,81.8z M543.3,100.6l-2.6-6.4l-2.6,6.4
				H543.3z"/>
			<path class="weaveGrey" d="M563.9,81.8h10.6L584,95V81.8h12.6v33.6h-10.7l-9.3-13.2v13.2h-12.6V81.8z"/>
			<path class="weaveGrey" d="M617,81.8h12.3l2.6,12.8l1.9-7.2v-5.7h10.3l3.8,12.9l2.9-12.9H663l-7.8,33.6h-11l-4-13.3l-4.3,13.3h-11
				L617,81.8z"/>
			<path class="weaveGrey" d="M666.6,81.8h26.9v9.1h-14.3v3h12.3v9.4h-12.3v3h14.3v9.1h-26.9V81.8z"/>
			<path class="weaveGrey" d="M708.6,81.8h13.1l13,33.6h-13.2l-2.4-6.3h-7.9l-2.4,6.3h-13.2L708.6,81.8z M717.7,100.6l-2.6-6.4l-2.6,6.4
				H717.7z"/>
			<path class="weaveGrey" d="M731.8,81.8h13.2l6.3,18.7l6.4-18.7h13.2l-13,33.6h-13.1L731.8,81.8z"/>
			<path class="weaveGrey" d="M774.4,81.8h12.6v33.6h-12.6V81.8z"/>
			<path class="weaveGrey" d="M791.8,81.8h10.6l9.4,13.2V81.8h12.6v33.6h-10.7l-9.3-13.2v13.2h-12.6V81.8z"/>
			<path class="weaveGrey" d="M828.1,98.6c0-11.4,7.3-17.3,20.5-17.3c4.3,0,7.6,0.4,10.8,1.5v10.8c-3.2-1.3-6.5-1.6-10-1.6
				c-4.9,0-8.4,1.6-8.4,6.7s2.3,6.7,6.6,6.7c1.2,0,2.1-0.1,3.1-0.4v-2h-5.6v-7.6h16v17.4c-2.9,1.8-7.3,3.3-13.5,3.3
				C835,116,828.1,110.1,828.1,98.6z"/>
		</g>
		<g>
			<path class="weaveGrey" d="M426.9,227h26.3v8.9h-13.9v3h12v9.2h-12v3h13.9v8.9h-26.3V227z"/>
			<path class="weaveGrey" d="M461.8,227h10.4l9.2,12.9V227h12.3v32.9h-10.4l-9.1-12.9v12.9h-12.3V227z"/>
			<path class="weaveGrey" d="M502.9,227h12.9l6.2,18.3l6.2-18.3H541l-12.7,32.9h-12.8L502.9,227z"/>
			<path class="weaveGrey" d="M550.2,227h12.3v32.9h-12.3V227z"/>
			<path class="weaveGrey" d="M572.7,227h18.5c8.4,0,12.4,5.3,12.4,12.3c0,4.4-2.6,7.8-5.8,9.9c0.7,0.5,1.1,1.1,1.4,1.7l3.9,8.9h-12.9
				l-3.2-7.3c-0.4-0.8-0.7-1.1-1.4-1.1h-0.5v8.4h-12.3V227z M588.1,241.9c2,0,3-0.8,3-2.5c0-1.9-0.9-2.7-2.8-2.7h-3.2v5.2H588.1z"/>
			<path class="weaveGrey" d="M611,243.5c0-12.4,6.5-16.9,17.6-16.9s17.6,4.5,17.6,16.9c0,12.4-6.5,16.9-17.6,16.9S611,255.8,611,243.5z
				 M633.6,243.5c0-4.7-1.4-6.5-5-6.5s-5,1.7-5,6.5c0,4.7,1.4,6.5,5,6.5S633.6,248.2,633.6,243.5z"/>
			<path class="weaveGrey" d="M655.4,227h10.4l9.2,12.9V227h12.3v32.9h-10.4l-9.1-12.9v12.9h-12.3V227z"/>
			<path class="weaveGrey" d="M697.5,227h11.7l6.7,13.6l6.6-13.6h11.7v32.9h-12.3v-11.5l-2.9,5.9h-6.3l-2.9-5.9v11.5h-12.3V227z"/>
			<path class="weaveGrey" d="M744.5,227h26.3v8.9h-13.9v3h12v9.2h-12v3h13.9v8.9h-26.3V227z"/>
			<path class="weaveGrey" d="M779.4,227h10.4l9.2,12.9V227h12.3v32.9h-10.4l-9.1-12.9v12.9h-12.3V227z"/>
			<path class="weaveGrey" d="M827.8,235.9h-8.4V227h29.1v8.9h-8.4v24h-12.3V235.9z"/>
		</g>
		<g>
			<path class="weaveOrange" d="M477.9,426c0-11.3,6.7-17.2,19.8-17.2c4.3,0,7.3,0.7,10.5,1.8v10.9c-3.3-1.5-6.3-2.2-9.7-2.2
				c-4.9,0-7.9,1.6-7.9,6.6s3.1,6.6,8,6.6c3.4,0,6.4-0.7,9.7-2.2v10.9c-3.2,1.1-6.2,1.8-10.5,1.8C484.7,443.2,477.9,437.3,477.9,426
				z"/>
			<path class="weaveOrange" d="M510.1,426c0-12.6,6.6-17.2,17.8-17.2c11.3,0,17.8,4.6,17.8,17.2s-6.6,17.2-17.8,17.2
				C516.7,443.2,510.1,438.5,510.1,426z M533,426c0-4.8-1.4-6.6-5.1-6.6c-3.7,0-5.1,1.8-5.1,6.6s1.4,6.6,5.1,6.6
				C531.6,432.5,533,430.8,533,426z"/>
			<path class="weaveOrange" d="M549.1,409.3H561l6.8,13.8l6.7-13.8h11.9v33.3h-12.5v-11.7l-2.9,6h-6.4l-2.9-6v11.7h-12.5V409.3z"/>
			<path class="weaveOrange" d="M592.6,409.3h11.9l6.8,13.8l6.7-13.8h11.9v33.3h-12.5v-11.7l-2.9,6h-6.4l-2.9-6v11.7h-12.5V409.3z"/>
			<path class="weaveOrange" d="M635.9,429.5v-20.2h12.5v18.6c0,3.2,1.4,4.7,4.1,4.7c2.7,0,4.1-1.5,4.1-4.7v-18.6h12.5v20.2
				c0,8.9-7.2,13.7-16.7,13.7C643.1,443.2,635.9,438.4,635.9,429.5z"/>
			<path class="weaveOrange" d="M675.1,409.3h10.5l9.3,13v-13h12.5v33.3h-10.6l-9.3-13v13h-12.5V409.3z"/>
			<path class="weaveOrange" d="M713.7,409.3h12.5v33.3h-12.5V409.3z"/>
			<path class="weaveOrange" d="M738.8,418.3h-8.5v-9h29.5v9h-8.5v24.3h-12.5V418.3z"/>
			<path class="weaveOrange" d="M774.5,430.5l-12.1-21.2h13l5.3,10.6l5.3-10.6h13L787,430.5v12.1h-12.5V430.5z"/>
		</g>
		<g>
			<g>
				<path class="weaveGrey" d="M406.3,153.1h24.5v8.3h-13v2.8H429v8.6h-11.2v11h-11.5V153.1z"/>
				<path class="weaveGrey" d="M434.6,153.1h11.5v30.6h-11.5V153.1z"/>
				<path class="weaveGrey" d="M451.8,153.1H469c7.9,0,11.6,4.9,11.6,11.5c0,4.1-2.4,7.3-5.4,9.3c0.7,0.5,1,1,1.3,1.6l3.6,8.3h-12l-3-6.8
					c-0.3-0.8-0.6-1-1.3-1h-0.5v7.8h-11.5V153.1z M466.1,167c1.9,0,2.8-0.8,2.8-2.4c0-1.8-0.9-2.5-2.6-2.5h-3v4.9H466.1z"/>
				<path class="weaveGrey" d="M485.4,153.1h10.9l6.2,12.7l6.2-12.7h10.9v30.6h-11.5V173l-2.7,5.5h-5.9l-2.7-5.5v10.8h-11.5V153.1z"/>
			</g>
		</g>
		<g>
			<g>
				<path class="weaveGrey" d="M741.8,153.1H760c6.3,0,9.9,3.1,9.9,8.7c0,3-1.1,5.2-2.9,6.5c2.4,1.1,3.9,2.7,3.9,6.2c0,5.8-4,9.3-10.8,9.3
					h-18.4V153.1z M756.9,164.9c1.5,0,2.3-0.7,2.3-2c0-1.2-0.8-1.9-2.3-1.9h-4.3v4H756.9z M756.9,176c1.5,0,2.3-0.7,2.3-2
					c0-1.2-0.8-1.9-2.3-1.9h-4.3v4H756.9z"/>
				<path class="weaveGrey" d="M775.5,171.7v-18.6H787v17.1c0,2.9,1.3,4.3,3.8,4.3c2.5,0,3.8-1.4,3.8-4.3v-17.1h11.5v18.6
					c0,8.2-6.7,12.6-15.3,12.6C782.2,184.3,775.5,179.9,775.5,171.7z"/>
				<path class="weaveGrey" d="M811.6,153.1h11.5v30.6h-11.5V153.1z"/>
				<path class="weaveGrey" d="M828.8,153.1h11.5V175h12.5v8.8h-24V153.1z"/>
				<path class="weaveGrey" d="M856.2,161.4h-7.8v-8.3h27.1v8.3h-7.8v22.4h-11.5V161.4z"/>
			</g>
		</g>
		<g>
			<g>
				<path class="weaveGrey" d="M498.7,372c0-11.3,6.7-17.2,19.8-17.2c4.3,0,7.3,0.7,10.5,1.8v10.9c-3.3-1.5-6.3-2.2-9.7-2.2
					c-4.9,0-7.9,1.6-7.9,6.6s3.1,6.6,8,6.6c3.4,0,6.4-0.7,9.7-2.2v10.9c-3.2,1.1-6.2,1.8-10.5,1.8
					C505.5,389.2,498.7,383.4,498.7,372z"/>
				<path class="weaveGrey" d="M534,375.5v-20.2h12.5v18.6c0,3.2,1.4,4.7,4.1,4.7s4.1-1.5,4.1-4.7v-18.6h12.5v20.2
					c0,8.9-7.2,13.7-16.7,13.7S534,384.4,534,375.5z"/>
				<path class="weaveGrey" d="M573.3,355.4h12.5v23.8h13.6v9.5h-26.1V355.4z"/>
				<path class="weaveGrey" d="M603,364.4h-8.5v-9H624v9h-8.5v24.3H603V364.4z"/>
				<path class="weaveGrey" d="M628.1,355.4h12.5v33.3h-12.5V355.4z"/>
				<path class="weaveGrey" d="M645.8,355.4h13l6.3,18.5l6.3-18.5h13l-12.8,33.3h-13L645.8,355.4z"/>
				<path class="weaveGrey" d="M696,355.4h13l12.8,33.3h-13l-2.4-6.2h-7.8l-2.4,6.2h-13L696,355.4z M705,373.9l-2.5-6.4l-2.5,6.4H705z"/>
				<path class="weaveGrey" d="M726.9,364.4h-8.5v-9h29.5v9h-8.5v24.3h-12.5V364.4z"/>
				<path class="weaveGrey" d="M752.1,355.4h26.6v9h-14.1v3h12.2v9.3h-12.2v3h14.1v9h-26.6V355.4z"/>
			</g>
		</g>
		<g>
			<g>
				<path class="weaveOrange" d="M740.5,44.4h-2l0.5-3.9h6.8V54h-5.3V44.4z M740.1,34.9h5.8v3.9h-5.8V34.9z"/>
				<path class="weaveOrange" d="M748.3,53.4v-3.8c1.3,0.6,3.4,1,4.7,1c1.4,0,2.1,0,2.1-0.5c0-0.7-1.4-0.8-3.3-1.5c-2.5-0.9-3.7-1.8-3.7-4.1
					c0-2.7,2-4.2,6.6-4.2c1.7,0,3.8,0.3,4.9,0.7v3.7c-1.1-0.5-2.6-0.7-3.7-0.7c-1.2,0-2.3,0-2.3,0.6c0,0.6,1.2,0.7,3,1.3
					c3.1,0.9,4,2,4,4.2c0,2.7-2.2,4.3-6.6,4.3C751.6,54.2,749.5,53.9,748.3,53.4z"/>
				<path class="weaveOrange" d="M772.1,49.9c0-2.8,1.9-4.5,5.7-4.5h3.5v-0.3c0-1-0.8-1.5-2.9-1.5c-1.5,0-3.1,0.3-4.6,0.7v-3.2
					c1.4-0.5,3.5-0.9,5.7-0.9c4.3,0,6.9,1.7,6.9,5.4V54h-4.6l-0.3-1.1c-1,0.9-2.4,1.4-4.6,1.4C774.3,54.2,772.1,52.9,772.1,49.9z
					 M781.3,50v-1.7h-2.6c-1.2,0-1.8,0.4-1.8,1.4c0,0.8,0.5,1.4,1.7,1.4S780.8,50.7,781.3,50z"/>
				<path class="weaveOrange" d="M789.1,40.5h5l0.1,1c1-0.7,2.8-1.3,4.7-1.3c3.4,0,4.8,1.6,4.8,5.1V54h-5.3v-8.1c0-1.2-0.5-1.7-1.8-1.7
					c-0.8,0-1.7,0.4-2.3,1V54h-5.3V40.5z"/>
			</g>
		</g>
		<g>
			<g>
				<path class="weaveOrange" d="M536.7,179v-5.2h-1.9v-4.3h1.9v-3l5.8-1.4v4.4h3.4l-0.2,4.3h-3.2v4.8c0,1.2,0.6,1.7,1.9,1.7
					c0.6,0,1.2-0.1,1.8-0.3v3.9c-1.1,0.5-2.3,0.7-3.9,0.7C538.3,184.5,536.7,182.4,536.7,179z"/>
				<path class="weaveOrange" d="M548.1,163.3h5.8v7.1c1-0.7,2.7-1.2,4.4-1.2c4,0,5.8,1.9,5.8,5.8v9.3h-5.9v-8.8c0-1.4-0.5-1.9-1.9-1.9
					c-0.9,0-1.9,0.5-2.5,1.1v9.7h-5.8V163.3z"/>
				<path class="weaveOrange" d="M566.1,179.8c0-3.1,2-4.9,6.2-4.9h3.8v-0.3c0-1.1-0.8-1.7-3.2-1.7c-1.6,0-3.4,0.3-5,0.8v-3.5
					c1.5-0.6,3.9-1,6.2-1c4.7,0,7.6,1.8,7.6,6v9.1h-5.1l-0.3-1.2c-1.1,0.9-2.6,1.5-5,1.5C568.5,184.5,566.1,183.1,566.1,179.8z
					 M576.1,179.9v-1.8h-2.8c-1.3,0-1.9,0.5-1.9,1.5c0,0.9,0.6,1.5,1.9,1.5C574.6,181.1,575.5,180.6,576.1,179.9z"/>
				<path class="weaveOrange" d="M585.3,179v-5.2h-1.9v-4.3h1.9v-3l5.8-1.4v4.4h3.4l-0.2,4.3h-3.2v4.8c0,1.2,0.6,1.7,1.9,1.7
					c0.6,0,1.2-0.1,1.8-0.3v3.9c-1.1,0.5-2.3,0.7-3.9,0.7C587,184.5,585.3,182.4,585.3,179z"/>
				<path class="weaveOrange" d="M607.5,178.8v-9.3h5.8v8.8c0,1.3,0.7,1.9,2,1.9c0.9,0,1.9-0.5,2.5-1.1v-9.7h5.8v14.7h-5.5l-0.2-1.1
					c-1.2,0.8-2.9,1.4-5,1.4C609.5,184.5,607.5,182.5,607.5,178.8z"/>
				<path class="weaveOrange" d="M626.2,183.6v-4.2c1.5,0.7,3.7,1.1,5.2,1.1c1.5,0,2.3,0,2.3-0.6c0-0.7-1.5-0.9-3.6-1.6c-2.7-1-4-2-4-4.5
					c0-2.9,2.2-4.6,7.2-4.6c1.9,0,4.2,0.3,5.3,0.7v4.1c-1.2-0.5-2.9-0.8-4-0.8c-1.3,0-2.5,0-2.5,0.6c0,0.7,1.3,0.8,3.3,1.4
					c3.4,1,4.3,2.1,4.3,4.6c0,3-2.5,4.7-7.2,4.7C629.9,184.5,627.5,184.1,626.2,183.6z"/>
				<path class="weaveOrange" d="M640.7,176.9c0-4.7,2.6-7.7,7.9-7.7c4.8,0,7.7,2.6,7.7,6.8v2.6h-10.2c0.5,1.6,2.1,2.2,4.8,2.2
					c1.7,0,3.6-0.3,4.6-0.8v3.5c-1.5,0.7-3.5,1-6.1,1C644.3,184.5,640.7,182,640.7,176.9z M651.3,175.4v-0.5c0-1.1-0.7-2-2.5-2
					c-2,0-2.7,0.9-2.7,2.5H651.3z"/>
				<path class="weaveOrange" d="M657.3,183.6v-4.2c1.5,0.7,3.7,1.1,5.2,1.1c1.5,0,2.3,0,2.3-0.6c0-0.7-1.5-0.9-3.6-1.6c-2.7-1-4-2-4-4.5
					c0-2.9,2.2-4.6,7.2-4.6c1.9,0,4.2,0.3,5.3,0.7v4.1c-1.2-0.5-2.9-0.8-4-0.8c-1.3,0-2.5,0-2.5,0.6c0,0.7,1.3,0.8,3.3,1.4
					c3.4,1,4.3,2.1,4.3,4.6c0,3-2.5,4.7-7.2,4.7C660.9,184.5,658.5,184.1,657.3,183.6z"/>
				<path class="weaveOrange" d="M684.7,179v-5.2h-1.9v-4.3h1.9v-3l5.8-1.4v4.4h3.4l-0.2,4.3h-3.2v4.8c0,1.2,0.6,1.7,1.9,1.7
					c0.6,0,1.2-0.1,1.8-0.3v3.9c-1.1,0.5-2.3,0.7-3.9,0.7C686.4,184.5,684.7,182.4,684.7,179z"/>
				<path class="weaveOrange" d="M696.1,163.3h5.8v7.1c1-0.7,2.7-1.2,4.4-1.2c4,0,5.8,1.9,5.8,5.8v9.3h-5.9v-8.8c0-1.4-0.5-1.9-1.9-1.9
					c-0.9,0-1.9,0.5-2.5,1.1v9.7h-5.8V163.3z"/>
				<path class="weaveOrange" d="M714.5,176.9c0-4.7,2.6-7.7,7.9-7.7c4.8,0,7.7,2.6,7.7,6.8v2.6h-10.2c0.5,1.6,2.1,2.2,4.8,2.2
					c1.7,0,3.6-0.3,4.6-0.8v3.5c-1.5,0.7-3.5,1-6.1,1C718.1,184.5,714.5,182,714.5,176.9z M725.1,175.4v-0.5c0-1.1-0.7-2-2.5-2
					c-2,0-2.7,0.9-2.7,2.5H725.1z"/>
			</g>
		</g>
		<g>
			<g>
				<path class="weaveOrange" d="M625.4,334.3v-5.6h-2.1V324h2.1v-3.3l6.3-1.5v4.8h3.7l-0.2,4.7h-3.4v5.2c0,1.3,0.6,1.9,2.1,1.9
					c0.7,0,1.3-0.1,2-0.3v4.2c-1.2,0.5-2.5,0.7-4.3,0.7C627.3,340.3,625.4,338,625.4,334.3z"/>
				<path class="weaveOrange" d="M636.3,332c0-5.4,3-8.3,9.3-8.3c6.4,0,9.4,2.9,9.4,8.3c0,5.4-3,8.3-9.4,8.3
					C639.3,340.3,636.3,337.4,636.3,332z M648.6,332c0-2.6-0.9-3.7-2.9-3.7c-2,0-2.9,1.1-2.9,3.7c0,2.6,0.8,3.7,2.9,3.7
					C647.7,335.7,648.6,334.6,648.6,332z"/>
			</g>
		</g>
		<g>
			<g>
				<g>
					<path class="weaveGrey" d="M532.7,20h15.7l2.3,12.7c0.6,3.1,1.1,7.3,1.1,7.3s0.7-4.2,1.4-7.3l3-12.7h12.5l2.4,12.7
						c0.6,3.1,1.1,7.3,1.1,7.3s0.6-4.2,1.2-7.3l2.5-12.7h14.9l-9.7,35.2h-15.3l-3.1-13.3c-0.6-2.8-1.2-6.5-1.2-6.5s-0.6,3.7-1.2,6.5
						l-3.1,13.3h-15.3L532.7,20z"/>
					<path class="weaveGrey" d="M593.6,33.4h14.9v21.8h-14.9V33.4z"/>
					<path class="weaveGrey" d="M593.6,20h14.9v9.8h-14.9V20z"/>
					<path class="weaveGrey" d="M649.7,46.3l3.7,8.9h-15.1l-3.8-9.4c-0.6-1.6-1.5-2.3-3.5-2.3c0.1,0-2.3,0-2.3,0v11.7h-14.9V20H636
						c12,0,16.2,4.1,16.2,12c0,5.4-2.9,8.2-6.9,9.8C647.1,42.4,648.6,43.7,649.7,46.3z M633,35.8c3.5,0,5.1-0.9,5.1-3.7
						c0-2.6-1.6-3.7-4.3-3.7h-5.1v7.4H633z"/>
					<path class="weaveGrey" d="M656.4,20h31.9v9.9h-17v3.4h14.3v8.3h-14.3v3.5H689v9.9h-32.6V20z"/>
					<path class="weaveGrey" d="M693,20h18.8c12.2,0,19.7,5.1,19.7,17.5c0,12-7.5,17.7-19.7,17.7H693V20z M710.5,45.4
						c4.7,0,5.8-1.2,5.8-7.5c0-6.3-1.1-8.1-5.8-8.1h-2.6v15.6H710.5z"/>
				</g>
			</g>
		</g>
		<polyline class="weaveLine" points="544,345.4 639.5,302.9 733.9,347.3 		"/>
		<line class="weaveLine" x1="443" y1="407" x2="443" y2="445.5"/>
		<line class="weaveLine" x1="839.5" y1="407" x2="839.5" y2="445.5"/>
	</g>
	<g>
		<circle class="cityBlocks" cx="1268.3" cy="429.4" r="2.4"/>
		<path class="cityBlocks" d="M1263.5,445.1c0-0.3-0.5-7.8,2.5-10.4c0.7-0.6,1.6-0.9,2.7-0.9c6,0.4,5.2,11.3,5.2,11.3"/>
		<path class="cityBlocks" d="M1266.3,442.3v-7.8V442.3z"/>
		<path class="cityBlocks" d="M1270.7,442.3v-7.7V442.3z"/>
		<line class="cityBlocks" x1="1266.1" y1="442.3" x2="1270.9" y2="442.3"/>
		<line class="cityBlocks" x1="1266.3" y1="442.3" x2="1266.3" y2="456.8"/>
		<line class="cityBlocks" x1="1270.7" y1="441" x2="1270.7" y2="457"/>
	</g>
	<g>
		<circle class="cityBlocks" cx="1107.4" cy="369" r="2"/>
		<path class="cityBlocks" d="M1103.3,382.1c0-0.3-0.4-6.5,2.1-8.7c0.6-0.5,1.3-0.8,2.3-0.7c5,0.3,4.4,9.4,4.4,9.4"/>
		<path class="cityBlocks" d="M1105.7,379.8v-6.5V379.8z"/>
		<path class="cityBlocks" d="M1109.4,379.8v-6.4V379.8z"/>
		<line class="cityBlocks" x1="1105.5" y1="379.8" x2="1109.6" y2="379.8"/>
		<line class="cityBlocks" x1="1105.7" y1="379.8" x2="1105.7" y2="391.8"/>
		<line class="cityBlocks" x1="1109.4" y1="378" x2="1109.4" y2="392"/>
	</g>
	<g>
		<circle class="cityBlocks" cx="1017.4" cy="379" r="2"/>
		<path class="cityBlocks" d="M1013.3,392.1c0-0.3-0.4-6.5,2.1-8.7c0.6-0.5,1.3-0.8,2.3-0.7c5,0.3,4.4,9.4,4.4,9.4"/>
		<path class="cityBlocks" d="M1015.7,389.8v-6.5V389.8z"/>
		<path class="cityBlocks" d="M1019.4,389.8v-6.4V389.8z"/>
		<line class="cityBlocks" x1="1015.5" y1="389.8" x2="1019.6" y2="389.8"/>
		<line class="cityBlocks" x1="1015.7" y1="389.8" x2="1015.7" y2="401.8"/>
		<line class="cityBlocks" x1="1019.4" y1="388" x2="1019.4" y2="402"/>
	</g>
	<g>
		<circle class="cityBlocks" cx="1036.4" cy="383" r="2"/>
		<path class="otherLine" d="M1032.3,396.1c0-0.3-0.4-6.5,2.1-8.7c0.6-0.5,1.3-0.8,2.3-0.7c5,0.3,4.4,9.4,4.4,9.4"/>
		<path class="cityBlocks" d="M1034.7,393.8v-6.5V393.8z"/>
		<path class="cityBlocks" d="M1038.4,393.8v-6.4V393.8z"/>
		<line class="cityBlocks" x1="1034.5" y1="393.8" x2="1038.6" y2="393.8"/>
		<line class="cityBlocks" x1="1034.7" y1="393.5" x2="1034.7" y2="401.9"/>
		<line class="cityBlocks" x1="1038.4" y1="393" x2="1038.4" y2="402"/>
	</g>
	<g>
		<circle class="cityBlocks" cx="985.3" cy="426.7" r="2.7"/>
		<path class="cityBlocks" d="M979.9,443.9c0-0.4-0.6-8.6,2.8-11.5c0.8-0.7,1.8-1,3-0.9c6.6,0.4,5.8,12.4,5.8,12.4"/>
		<path class="cityBlocks" d="M983,440.8v-8.6V440.8z"/>
		<path class="cityBlocks" d="M987.9,440.8v-8.4V440.8z"/>
		<line class="cityBlocks" x1="982.8" y1="440.8" x2="988.1" y2="440.8"/>
		<line class="cityBlocks" x1="983" y1="440.8" x2="983" y2="456.8"/>
		<line class="cityBlocks" x1="987.9" y1="441.1" x2="987.9" y2="457"/>
	</g>
	<g>
		<circle class="cityBlocks" cx="139.4" cy="438.6" r="1.6"/>
		<path class="cityBlocks" d="M136.2,449.1c0-0.2-0.4-5.2,1.7-6.9c0.5-0.4,1.1-0.6,1.8-0.6c4,0.3,3.5,7.5,3.5,7.5"/>
		<path class="cityBlocks" d="M138.1,447.2V442V447.2z"/>
		<path class="cityBlocks" d="M141,447.2v-5.1V447.2z"/>
		<line class="cityBlocks" x1="138" y1="447.2" x2="141.2" y2="447.2"/>
		<line class="cityBlocks" x1="138.1" y1="447.2" x2="138.1" y2="456.9"/>
		<line class="cityBlocks" x1="141" y1="446" x2="141" y2="457"/>
	</g>
	<g>
		<circle class="cityBlocks" cx="147.4" cy="435.9" r="1.9"/>
		<path class="cityBlocks" d="M143.7,447.9c0-0.3-0.4-6,1.9-8c0.5-0.5,1.2-0.7,2.1-0.7c4.6,0.3,4,8.6,4,8.6"/>
		<path class="cityBlocks" d="M145.9,445.7v-6V445.7z"/>
		<path class="cityBlocks" d="M149.2,445.7v-5.9V445.7z"/>
		<line class="cityBlocks" x1="145.7" y1="445.7" x2="149.4" y2="445.7"/>
		<line class="cityBlocks" x1="145.9" y1="445.7" x2="145.9" y2="456.8"/>
		<line class="cityBlocks" x1="149.2" y1="445" x2="149.2" y2="457"/>
	</g>
	<g>
		<circle class="cityBlocks" cx="388.3" cy="431.3" r="2.3"/>
		<path class="cityBlocks" d="M383.8,445.9c0-0.3-0.5-7.3,2.3-9.7c0.7-0.6,1.5-0.9,2.5-0.8c5.6,0.4,4.9,10.5,4.9,10.5"/>
		<path class="cityBlocks" d="M386.5,443.3V436V443.3z"/>
		<path class="cityBlocks" d="M390.6,443.3v-7.1V443.3z"/>
		<line class="cityBlocks" x1="386.3" y1="443.3" x2="390.8" y2="443.3"/>
		<line class="cityBlocks" x1="386.5" y1="443.3" x2="386.5" y2="456.8"/>
		<line class="cityBlocks" x1="390.6" y1="442.4" x2="390.6" y2="457"/>
	</g>
	<g>
		<circle class="cityBlocks" cx="309.4" cy="350.9" r="1.9"/>
		<path class="cityBlocks" d="M305.7,362.9c0-0.3-0.4-6,1.9-8c0.5-0.5,1.2-0.7,2.1-0.7c4.6,0.3,4,8.6,4,8.6"/>
		<path class="cityBlocks" d="M307.9,360.7v-6V360.7z"/>
		<path class="cityBlocks" d="M311.2,360.7v-5.9V360.7z"/>
		<line class="cityBlocks" x1="307.7" y1="360.7" x2="311.4" y2="360.7"/>
		<line class="cityBlocks" x1="307.9" y1="360.7" x2="307.9" y2="371.8"/>
		<line class="cityBlocks" x1="311.2" y1="360" x2="311.2" y2="372"/>
	</g>
	<g>
		<rect x="1229" y="438.5" class="cityBlocks" width="3" height="9"/>
		<path class="otherLine" d="M1231,447.5c0,0,4,0,5,1s1,6,1,6"/>
		<path class="otherLine" d="M1230,439.5c0,0,4,2,8,1"/>
		<circle class="cityBlocks" cx="1230.3" cy="434.7" r="2.2"/>
	</g>
	<g>
		<rect x="888" y="438.5" class="cityBlocks" width="3" height="9"/>
		<path class="otherLine" d="M890,447.5c0,0,4,0,5,1s1,6,1,6"/>
		<path class="otherLine" d="M889,439.5c0,0,4,2,8,1"/>
		<circle class="cityBlocks" cx="889.3" cy="434.7" r="2.2"/>
	</g>
	<g>

			<rect x="1254.3" y="438.5" transform="matrix(-1 -4.304969e-11 4.304969e-11 -1 2511.637 886)" class="cityBlocks" width="3" height="9"/>
		<path class="otherLine" d="M1255.1,447.5c0,0-4,0-5,1s-1,6-1,6"/>
		<line class="otherLine" x1="1255.8" y1="440" x2="1255.8" y2="448"/>
		<circle class="cityBlocks" cx="1255.8" cy="434.7" r="2.2"/>
	</g>
	<g>
		<rect x="948.8" y="438.5" class="cityBlocks" width="3" height="9"/>
		<path class="otherLine" d="M951,447.5c0,0,4,0,5,1s1,6,1,6"/>
		<line class="otherLine" x1="950.3" y1="440" x2="950.3" y2="448"/>
		<circle class="cityBlocks" cx="950.3" cy="434.7" r="2.2"/>
	</g>
	<g>

			<rect x="912.3" y="438.5" transform="matrix(-1 -4.304969e-11 4.304969e-11 -1 1827.6368 886)" class="cityBlocks" width="3" height="9"/>
		<path class="otherLine" d="M913.1,447.5c0,0-4,0-5,1s-1,6-1,6"/>
		<line class="otherLine" x1="913.8" y1="440" x2="913.8" y2="448"/>
		<circle class="cityBlocks" cx="913.8" cy="434.7" r="2.2"/>
	</g>
	<g>

			<rect x="1191.1" y="438.5" transform="matrix(-1 -4.304969e-11 4.304969e-11 -1 2385.2737 886)" class="cityBlocks" width="3" height="9"/>
		<path class="otherLine" d="M1192.1,447.5c0,0-4,0-5,1s-1,6-1,6"/>
		<path class="otherLine" d="M1193.1,439.5c0,0-3.1,3-7.1,2"/>
		<circle class="cityBlocks" cx="1192.8" cy="434.7" r="2.2"/>
	</g>
	<g>
		<rect x="1165.8" y="438.5" class="cityBlocks" width="3" height="9"/>
		<path class="otherLine" d="M1168,447.5c0,0,4,0,5,1s1,6,1,6"/>
		<path class="otherLine" d="M1167.3,440c0,0,1.7,5.5,4.7,5.5"/>
		<circle class="cityBlocks" cx="1167.3" cy="434.7" r="2.2"/>
	</g>
</g>
</svg>
</div>
