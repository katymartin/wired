<?php
/**
 Template Name: Contact
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>


<head>
	<title>Contact Info | WiRED Properties</title>
	<meta name="description" content="Employee contact and location information.">
</head>

<body>

<section class="contactPage">

<div class="secWrapper" >
	<div class="flex">
		<div class=" contactForm2">
			<h1><?php the_field('page_title'); ?><svg class="accentT" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 32"><path class="tPath" d="M4.5 3.6V28L25 15.8z"/></svg></h1>

				<a href="<?php the_field('tel_link'); ?>" target="_blank"><p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;<?php the_field('office_phone'); ?></p></a>
				<a href="<?php the_field('email_link'); ?>"><p><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php the_field('office_email'); ?></p></a>
				<a href="<?php the_field('maps_link'); ?>" target="_blank"><p><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;<?php the_field('address_top'); ?></p>
				<p style="padding-left: 32px; margin-top: 0px;"><?php the_field('address_bottom'); ?></p></a>

		</div>
		<div class=" contactForm2">
			<script type="text/javascript" src="https://form.jotform.us/jsform/72636153904154"></script>

		</div>
	</div>

	<div class="flex contDiv">



		<?php
		  if( have_rows('emp_contact') ):
		  while( have_rows('emp_contact') ): the_row(); ?>

			<div class="contactInfo">
			<div class="yellB">
			<h3><?php the_sub_field('emp_name'); ?></h3>
			<h5><?php the_sub_field('emp_title'); ?></h5>
			</div>
			<a href="<?php the_sub_field('emp_tel_link'); ?>" target="_blank"><p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;<?php the_sub_field('emp_phone'); ?></p></a>
			<a href="<?php the_sub_field('emp_email_link'); ?>"><p><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<?php the_sub_field('emp_email'); ?></p></a>
		</div>

	<?php endwhile; endif; ?>

	</div>
</div>

<div class="emailSignUp">
	<h3>Sign up to receive the Urban Weaver!</h3>
	<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="https://wiredproperties.us17.list-manage.com/subscribe/post?u=dcf6936ff96b2e07460609f87&amp;id=79e72fd8f6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_dcf6936ff96b2e07460609f87_79e72fd8f6" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->
</div>

<div ><?php include 'map.php';?> </div>


</section>

</body>

<div style="display: none;">
<?php get_footer(); ?>
</div>
