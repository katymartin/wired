
<!------------------
  ------------------
      2 Columns
  ------------------
  ------------------>
<section class="contactSec">
<div class="secWrapper">
<h4>Get in touch.</h4>
	<div class="flex">

		<div class="flexDiv contactForm">

			<script type="text/javascript" src="https://form.jotform.us/jsform/72636153904154"></script>
		</div>
		<div class="flexDiv contactInfo">
			<a href="tel:14143750244" target="_blank"><p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;414.375.0244</p></a>


			<a href="mailto:info@WiREDproperties.com"><p><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;info@WiREDproperties.com</p></a>

			<a href="https://www.google.com/maps/dir/''/wired+properties/data=!4m5!4m4!1m0!1m2!1m1!1s0x88051f18fbddef51:0x4d1f2d3256f7e36a?sa=X&ved=0ahUKEwih9rWar77VAhVIwiYKHURKAaIQ9RcIdzAL" target="_blank"><p><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;735&nbsp;N&nbsp;Water&nbsp;Street&nbsp;Suite,&nbsp;1228</p>
			<p style="padding-left: 32px; margin-top: -10px;">Milwaukee, WI 53202</p></a>
		</div>


	</div>

</div>


</section>
