/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/


/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function($) {



  /*******
    MENU
  *******/

  $(document).ready(function () {
    //Menu button on click event
    $('.mobile-nav-button').on('click', function() {
      // Toggles a class on the menu button to transform the burger menu into a cross
      $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(1)" ).toggleClass( "mobile-nav-button__line--1");
      $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(2)" ).toggleClass( "mobile-nav-button__line--2");
      $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(3)" ).toggleClass( "mobile-nav-button__line--3");

      // Toggles a class that slides the menu into view on the screen
      $('.mobileMenu').toggleClass('mobileMenu--open');
      return false;
    });
  });




AOS.init({
        easing: 'ease-in-out-sine'
      });


      /******************
      ******************
            WEAVER
      ******************
      ******************/


      // init controller
      var controller = new ScrollMagic.Controller();

      // loop through all elements
      $('.weaveLine').each(function() {

        // build a tween
        var tween = TweenMax.staggerFrom($(this), 5, {autoAlpha: 0, drawSVG:0});

        // build a scene
        var scene = new ScrollMagic.Scene({
          triggerElement: this, triggerHook: 'onEnter'
        })
        .setTween(tween) // trigger a TweenMax.to tween
        .addTo(controller);

      });



      var controller = new ScrollMagic.Controller();

      // loop through all elements
      $('.weaveOrange, .weaveGrey ').each(function() {

        // build a tween
        var tween = TweenMax.from($(this), 1, {autoAlpha: 0});

        // build a scene
        var scene = new ScrollMagic.Scene({
          triggerElement: this, triggerHook: 'onEnter'
        })
        .setTween(tween) // trigger a TweenMax.to tween
        .addTo(controller);

      });


/******************
******************
      TEAM
******************
******************/


      /**** Click Left  ****/

          $(function() {
            $(".hShot1, .empT1").click(function() {
              $(".hShot1").addClass("hShot1active");
            });
          });

          $(function() {
            $(".hShot1, .empT1").click(function() {
              $(".hShot2").removeClass("hShot2active");
            });
          });

          $(function() {
            $(".hShot1, .empT1").click(function() {
              $(".hShot3").removeClass("hShot3active");
            });
          });

          $(function() {
            $(".hShot1, .empT1").click(function() {
              $(".empT1").addClass("empActive1");
            });
          });

          $(function() {
            $(".hShot1, .empT1").click(function() {
              $(".empT2").removeClass("empActive2");
            });
          });

          $(function() {
            $(".hShot1, .empT1").click(function() {
              $(".empT3").removeClass("empActive3");
            });
          });

          $(function() {
            $(".hShot1, .empT1").click(function() {
              $(".bio1").addClass("bioActive1");
            });
          });

          $(function() {
            $(".hShot1, .empT1").click(function() {
              $(".bio2").removeClass("bioActive2");
            });
          });

          $(function() {
            $(".hShot1, .empT1").click(function() {
              $(".bio3").removeClass("bioActive3");
            });
          });




          /**** Click Center  ****/

              $(function() {
                $(".hShot2, .empT2").click(function() {
                  $(".hShot2").addClass("hShot2active");
                });
              });

              $(function() {
                $(".hShot2, .empT2").click(function() {
                  $(".hShot1").removeClass("hShot1active");
                });
              });

              $(function() {
                $(".hShot2, .empT2").click(function() {
                  $(".hShot3").removeClass("hShot3active");
                });
              });

              $(function() {
                $(".hShot2, .empT2").click(function() {
                  $(".empT2").addClass("empActive2");
                });
              });

              $(function() {
                $(".hShot2, .empT2").click(function() {
                  $(".empT1").removeClass("empActive1");
                });
              });

              $(function() {
                $(".hShot2, .empT2").click(function() {
                  $(".empT3").removeClass("empActive3");
                });
              });

              $(function() {
                $(".hShot2, .empT2").click(function() {
                  $(".bio2").addClass("bioActive2");
                });
              });

              $(function() {
                $(".hShot2, .empT2").click(function() {
                  $(".bio1").removeClass("bioActive1");
                });
              });

              $(function() {
                $(".hShot2, .empT2").click(function() {
                  $(".bio3").removeClass("bioActive3");
                });
              });

              /**** Click Right  ****/

                  $(function() {
                    $(".hShot3, .empT3").click(function() {
                      $(".hShot3").addClass("hShot3active");
                    });
                  });

                  $(function() {
                    $(".hShot3, .empT3").click(function() {
                      $(".hShot1").removeClass("hShot1active");
                    });
                  });

                  $(function() {
                    $(".hShot3, .empT3").click(function() {
                      $(".hShot2").removeClass("hShot2active");
                    });
                  });

                  $(function() {
                    $(".hShot3, .empT3").click(function() {
                      $(".empT3").addClass("empActive3");
                    });
                  });

                  $(function() {
                    $(".hShot3, .empT3").click(function() {
                      $(".empT1").removeClass("empActive1");
                    });
                  });

                  $(function() {
                    $(".hShot3, .empT3").click(function() {
                      $(".empT2").removeClass("empActive2");
                    });
                  });

                  $(function() {
                    $(".hShot3, .empT3").click(function() {
                      $(".bio3").addClass("bioActive3");
                    });
                  });

                  $(function() {
                    $(".hShot3, .empT3").click(function() {
                      $(".bio1").removeClass("bioActive1");
                    });
                  });

                  $(function() {
                    $(".hShot3, .empT3").click(function() {
                      $(".bio2").removeClass("bioActive2");
                    });
                  });


/******************
 ******************
     Draw SVG
 ******************
 ******************/

TweenMax.staggerFrom(".orangePath", 4, {drawSVG:0}, 0.1);
TweenMax.from(".txt-fade", 3, {opacity:0});

// init controller
var controller = new ScrollMagic.Controller();

// loop through all elements
$('.fade-up-head').each(function() {

  // build a tween
  var tween = TweenMax.from($(this), .7, {autoAlpha: 0, y: '-=30', ease:Linear.easeNone});

  // build a scene
  var scene = new ScrollMagic.Scene({
    triggerElement: this, triggerHook: 'onEnter'
  })
  .setTween(tween) // trigger a TweenMax.to tween
  .addTo(controller);

});

// init controller
var controller = new ScrollMagic.Controller();

// loop through all elements
$('.qLine').each(function() {

  // build a tween
  var tween = TweenMax.from($(this), .7, {autoAlpha: 0, drawSVG:0});

  // build a scene
  var scene = new ScrollMagic.Scene({
    triggerElement: this, triggerHook: 'onEnter'
  })
  .setTween(tween) // trigger a TweenMax.to tween
  .addTo(controller);

});

var controller = new ScrollMagic.Controller();

// loop through all elements
$('.fade-in').each(function() {

  // build a tween
  var tween = TweenMax.from($(this), .7, {autoAlpha: 0});

  // build a scene
  var scene = new ScrollMagic.Scene({
    triggerElement: this, triggerHook: 'onEnter'
  })
  .setTween(tween) // trigger a TweenMax.to tween
  .addTo(controller);

});



/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();

function loadGravatars() {
  // set the viewport using the function above
  viewport = updateViewportDimensions();
  // if the viewport is tablet or larger, we load in the gravatars
  if (viewport.width >= 768) {
  jQuery('.comment img[data-gravatar]').each(function(){
    jQuery(this).attr('src',jQuery(this).attr('data-gravatar'));
  });
	}
} // end function



  /*
   * Let's fire off the gravatar function
   * You can remove this if you don't need it
  */
  loadGravatars();


}); /* end of as page load scripts */
