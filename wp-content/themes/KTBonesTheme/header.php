


	<!doctype html>

	<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
	<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
	<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
	<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->



		<head>

		<meta http-equiv="content-type" content="text/html; charset=utf-8">
	  <meta http-equiv="content-language" content="nl">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
	<link href="/wp-content/themes/KTBonesTheme/library/css/aos.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kanit:400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

	<link rel="canonical" href="http://wiredproperties.com" />
	        <meta property="og:url" content="http://wiredproperties.com"/>
	        <meta property="og:title" content="WiRED Properties" />
	        <meta property="og:description" content="WiRED Properties is a Milwaukee based real estate development company working with stakeholders at every level to generate community success." />
	        <meta property="og:type" content="article" />
	        <meta property="og:image" content="http://wiredproperties.com/wp-content/themes/KTBonesTheme/library/images/WiRED_FBpreview.png" />
	        <meta property="og:image:width" content="1200" />
	        <meta property="og:image:height" content="628" />



	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-39015905-30"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-39015905-30');
</script>




			<title><?php wp_title(''); ?></title>

			<?php wp_head(); ?>


	<nav>

		<a class="wiredLogo" href="/" target="_self">
		<svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 351 81"><path class="wiredText" d="M12.2 18.5h22.7l3.3 18.4c.8 4.5 1.6 10.5 1.6 10.5s1-6.1 2.1-10.5l4.4-18.4h18l3.5 18.4c.8 4.5 1.7 10.5 1.7 10.5s.8-6 1.7-10.5l3.6-18.4h21.6L82.2 69.4H60.1l-4.5-19.3c-.9-4.1-1.7-9.4-1.7-9.4s-.9 5.3-1.8 9.4l-4.5 19.3H25.5L12.2 18.5zM100.2 37.9h21.6v31.5h-21.6V37.9zM100.2 18.5h21.6v14.2h-21.6V18.5zM181.3 56.5l5.3 12.8h-21.8l-5.5-13.6c-.9-2.3-2.2-3.3-5-3.3H151v17h-21.6V18.5h32.2c17.4 0 23.5 5.9 23.5 17.4 0 7.8-4.2 11.9-9.9 14.2 2.5.9 4.7 2.7 6.1 6.4zm-24.1-15.1c5.1 0 7.4-1.3 7.4-5.4 0-3.7-2.3-5.4-6.2-5.4H151v10.7h6.2zM191.1 18.5h46.2v14.4h-24.6v4.9h20.7v12.1h-20.7V55h25.6v14.4h-47.2V18.5zM244.1 18.5h27.2c17.7 0 28.4 7.4 28.4 25.3 0 17.4-10.8 25.6-28.5 25.6h-27.1V18.5zm25.3 36.7c6.9 0 8.4-1.7 8.4-10.8 0-9.1-1.6-11.7-8.4-11.7h-3.7v22.6h3.7z"/><g><path class="logoTri" d="M333 27.4l-27.9 16.2V11.2L333 27.4z"/></g></svg>
		</a>


			<div class="deskMenu">
					<div class="flexDiv">
						<a href="/about/">About</a>
						<a href="/projects/">Projects</a>
						<a href="/map/">Millennials</a>
						<a href="/resources/">Resources</a>
						<a href="/gallery/">Gallery</a>
						<a href="/contact/">Contact</a>
						<a href="https://www.facebook.com/wiredproperties/?ref=br_rs/" target="_blank"><svg class="socialI" id="fbIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path class="svgIcon" d="M20.1.6C9.3.6.6 9.3.6 20.1s8.7 19.5 19.5 19.5 19.5-8.7 19.5-19.5S30.9.6 20.1.6zm10.7 29c0 .7-.5 1.2-1.2 1.2h-5.4v-8.3H27l.4-3.2h-3.2v-2.1c0-.9.3-1.6 1.6-1.6h1.7v-2.9c-.3 0-1.3-.1-2.5-.1-2.5 0-4.1 1.5-4.1 4.3v2.4h-2.8v3.2h2.8v8.3H10.6c-.7 0-1.2-.5-1.2-1.2v-19c0-.7.5-1.2 1.2-1.2h19c.7 0 1.2.5 1.2 1.2v19z"/></svg></a>
						<a href="https://www.linkedin.com/company-beta/2751168/"><svg class="socialI" id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path class="svgIcon" d="M20 .5C9.2.5.5 9.2.5 20S9.2 39.5 20 39.5 39.5 30.8 39.5 20 30.8.5 20 .5zm10.9 27.9c0 1.4-1.1 2.5-2.5 2.5H11.6c-1.4 0-2.5-1.1-2.5-2.5V11.6c0-1.4 1.1-2.5 2.5-2.5h16.8c1.4 0 2.5 1.1 2.5 2.5v16.8z"/><path class="svgIcon" d="M14 17.9h2.6v8.2H14v-8.2zm1.3-4.1c.8 0 1.5.7 1.5 1.5s-.7 1.5-1.5 1.5-1.5-.7-1.5-1.5.7-1.5 1.5-1.5M18.2 17.9h2.4V19c.3-.6 1.2-1.3 2.4-1.3 2.6 0 3.1 1.7 3.1 3.9v4.5h-2.6v-4c0-1 0-2.2-1.3-2.2s-1.5 1-1.5 2.1v4.1h-2.5v-8.2z"/></g></g></svg></a>
				</div>
			</div>

		<div class="mobile-nav-button">
      <div class="mobile-nav-button__line"></div>
      <div class="mobile-nav-button__line"></div>
      <div class="mobile-nav-button__line"></div>
    </div>

	  <div class="mobileMenu">
			<ul>
			<li><a href="/about/">About</a></li>
			<li><a href="/projects/">Projects</a></li>
			<li><a href="/map/">Millennials</a></li>
			<li><a href="/resources/">Resources</a></li>
			<li><a href="/gallery/">Gallery</a></li>
			<li><a href="/contact/">Contact</a></li>
			<li class="mSoc">
				<a href="https://www.facebook.com/wiredproperties/?ref=br_rs/" target="_blank"><svg class="socialI" id="fbIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path class="svgIcon" d="M20.1.6C9.3.6.6 9.3.6 20.1s8.7 19.5 19.5 19.5 19.5-8.7 19.5-19.5S30.9.6 20.1.6zm10.7 29c0 .7-.5 1.2-1.2 1.2h-5.4v-8.3H27l.4-3.2h-3.2v-2.1c0-.9.3-1.6 1.6-1.6h1.7v-2.9c-.3 0-1.3-.1-2.5-.1-2.5 0-4.1 1.5-4.1 4.3v2.4h-2.8v3.2h2.8v8.3H10.6c-.7 0-1.2-.5-1.2-1.2v-19c0-.7.5-1.2 1.2-1.2h19c.7 0 1.2.5 1.2 1.2v19z"/></svg></a>
				<a href="https://www.linkedin.com/company-beta/2751168/"><svg class="socialI" id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path class="svgIcon" d="M20 .5C9.2.5.5 9.2.5 20S9.2 39.5 20 39.5 39.5 30.8 39.5 20 30.8.5 20 .5zm10.9 27.9c0 1.4-1.1 2.5-2.5 2.5H11.6c-1.4 0-2.5-1.1-2.5-2.5V11.6c0-1.4 1.1-2.5 2.5-2.5h16.8c1.4 0 2.5 1.1 2.5 2.5v16.8z"/><path class="svgIcon" d="M14 17.9h2.6v8.2H14v-8.2zm1.3-4.1c.8 0 1.5.7 1.5 1.5s-.7 1.5-1.5 1.5-1.5-.7-1.5-1.5.7-1.5 1.5-1.5M18.2 17.9h2.4V19c.3-.6 1.2-1.3 2.4-1.3 2.6 0 3.1 1.7 3.1 3.9v4.5h-2.6v-4c0-1 0-2.2-1.3-2.2s-1.5 1-1.5 2.1v4.1h-2.5v-8.2z"/></g></g></svg></a>
			</li>
		</ul>
 </div>
</div>
	</div>
	</nav>


		</head>
