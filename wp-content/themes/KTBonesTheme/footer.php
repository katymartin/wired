			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
				<?php include 'contactForm.php';?>

				<link href="/wp-content/themes/KTBonesTheme/library/js/scripts.js" rel="stylesheet">
				<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.14.2/TweenMax.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js" rel="stylesheet"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js" rel="stylesheet"></script>
				<script src="/wp-content/themes/KTBonesTheme/library/js/drawSVG.js" rel="stylesheet"></script>
				<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
				<script src="/wp-content/themes/KTBonesTheme/library/js/aos.js" rel="stylesheet"></script>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>



</html> <!-- end of site. what a ride! -->
