<?php
/**
 Template Name: Projects
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>Projects &amp; Services | WiRED Properties</title>
	<meta name="description" content="Description of WiRED's services and links to project case studies.s">
</head>

<body>

<div id="wrapper">

<section class="projectCategories">
<div class="secWrapper">
	<div class="flex">
		<div class="pCatInfo" data-aos="fade-right">
			<img src="<?php the_field('category_1_icon'); ?>"/>
			<h1><?php the_field('category_1'); ?><svg class="accentT" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 32"><path class="tPath" d="M4.5 3.6V28L25 15.8z"/></svg></h1>
			<p><?php the_field('category_1_copy'); ?></p>
			<?php
		if( have_rows('category_1_bullets') ):
		while( have_rows('category_1_bullets') ): the_row(); ?>
		<ul class="ulLeft">
			<li><?php the_sub_field('left_bullet') ;?></li>
		</ul>
		<ul class="ulRight">
			<li><?php the_sub_field('right_bullet') ;?></li>
		</ul>

	<?php endwhile; endif; ?>

		</div>
	</div>
		<div class="flex" id="strategy">
		<div class="logoFlex" data-aos="zoom-in">
			<?php
				$args = array(
				    'post_type' => 'project',
				    'post_status' => 'publish',
				    'category_name' => 'strategy'
				);
				$projects_loop = new WP_Query($args);
				if ($projects_loop->have_posts()):
				    while ($projects_loop->have_posts()):
				        $projects_loop->the_post();

				        $logo = get_field('post_logo');
				        $alt  = get_field('project_logo_name');
				        $link = get_permalink();
				?>
				    <a href="<?php echo $link; ?>" class="logoPrev">
				        <img src="<?php echo $logo; ?>" alt="<?php echo $alt; ?>">
				    </a>
				    <?php
				      endwhile;
				    wp_reset_postdata();
				  endif;
				   ?>
		</div>

	</div>


</div>
</section>


<section class="projectCategories">
<div class="secWrapper" id="midBorder">
	<div class="flex" id="risk">
		<div class="pCatInfo" data-aos="fade-right">
			<img src="<?php the_field('category_2_icon'); ?>"/>
			<h1><?php the_field('category_2'); ?><svg class="accentT" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 32"><path class="tPath" d="M4.5 3.6V28L25 15.8z"/></svg></h1>
			<p><?php the_field('category_2_copy'); ?></p>
			<?php
		if( have_rows('category_2_bullets') ):
		while( have_rows('category_2_bullets') ): the_row(); ?>
		<ul class="ulLeft">
			<li><?php the_sub_field('left_bullet2') ;?></li>
		</ul>
		<ul class="ulRight">
			<li><?php the_sub_field('right_bullet2') ;?></li>
		</ul>

	<?php endwhile; endif; ?>

		</div>
	</div>
	<div class="flex">
	<div class="logoFlex" data-aos="zoom-in">
		<?php

			$args = array(
					'post_type' => 'project',
					'post_status' => 'publish',
					'category_name' => 'risk'
			);
			$projects_loop = new WP_Query($args);
			if ($projects_loop->have_posts()):
					while ($projects_loop->have_posts()):
							$projects_loop->the_post();

							$logo = get_field('post_logo');
							$alt  = get_field('project_logo_name');
							$link = get_permalink();
			?>
					<a href="<?php echo $link; ?>" class="logoPrev">
							<img src="<?php echo $logo; ?>" alt="<?php echo $alt; ?>">
					</a>
					<?php
						endwhile;
					wp_reset_postdata();
				endif;
				 ?>
	</div>

</div>
</div>
</section>


<section class="projectCategories">
<div class="secWrapper">
	<div class="flex" id="fee">
		<div class="pCatInfo" data-aos="fade-right">
			<img src="<?php the_field('category_3_icon'); ?>"/>
			<h1><?php the_field('category_3'); ?><svg class="accentT" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 32"><path class="tPath" d="M4.5 3.6V28L25 15.8z"/></svg></h1>
			<p><?php the_field('category_3_copy'); ?></p>
			<?php
		if( have_rows('category_3_bullets') ):
		while( have_rows('category_3_bullets') ): the_row(); ?>
		<ul class="ulLeft">
			<li><?php the_sub_field('left_bullet3') ;?></li>
		</ul>
		<ul class="ulRight">
			<li><?php the_sub_field('right_bullet3') ;?></li>
		</ul>

	<?php endwhile; endif; ?>

		</div>
	</div>
	<div class="flex">

		<a class="tempProj" href="/project/milwaukee-bucks-stadium/"data-aos="zoom-in"></a>

	<!--
	<div class="logoFlex" data-aos="zoom-in">
		< ?php

			$args = array(
					'post_type' => 'project',
					'post_status' => 'publish',
					'category_name' => 'fee'
			);
			$projects_loop = new WP_Query($args);
			if ($projects_loop->have_posts()):
					while ($projects_loop->have_posts()):
							$projects_loop->the_post();

							$logo = get_field('post_logo');
							$alt  = get_field('project_logo_name');
							$link = get_permalink();
			?>
					<a href="< ?php echo $link; ?>" class="logoPrev">
							<img src="< ?php echo $logo; ?>" alt="< ?php echo $alt; ?>">
					</a>
					< ?php
						endwhile;
					wp_reset_postdata();
				endif;
				 ?>
	</div>
-->
</div>
</div>
</section>




</div>
</body>


<?php get_footer(); ?>
