<?php
/**
 Template Name: Updates
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Updates | WiRED Properties</title>
	<meta content="WiRED reviews news sources and provides commentary." name="description">
</head>
<body>
	<div id="wrapper">
	<section class="millIntro innerWrap" data-aos="fade-up">
		<h1><?php the_field('page_title') ;?><svg class="accentT" viewbox="0 0 29 32" xmlns="http://www.w3.org/2000/svg">
		<path class="tPath" d="M4.5 3.6V28L25 15.8z"></path></svg></h1><?php
		        if( have_rows('col_call') ):
		        while( have_rows('col_call') ): the_row(); ?>
		<div class="pCols">
			<div>
				<p><?php the_sub_field('pcol_left') ;?></p>
			</div>
			<div>
				<p><?php the_sub_field('pcol_right') ;?></p>
			</div>
		</div><?php endwhile; endif; ?>
	</section>


	<section class="featuredPosts" data-aos="fade-left">
		<?php
			$args = array(
					'post_type' => array('press', 'updates'),
					'post_status' => 'publish',
					'posts_per_page' => 1,
					'order' => 'DESC'
			);
			$projects_loop = new WP_Query($args);
			if ($projects_loop->have_posts()):
					while ($projects_loop->have_posts()):
							$projects_loop->the_post();
							$play     = get_field('play_button');
							$playAlt  = get_field('play_alt');
							$title    = get_the_title();
							$excerpt  = get_the_excerpt();
							$link     = get_the_permalink();
							$extLink  = get_field('cta_link');
							$extCTA   = get_field('cta_button');
							$updateB  = get_field('update_button');
			?>

								<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	              <div class="backImg" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; "  >
									<a href="<?php echo $link; ?>" >
											<img class="logoPrev" src="<?php echo $play; ?>" alt="<?php echo $playAlt; ?>">
									</a>

									<div class="aInfo">
										<h2><?php echo $title;?></h2>
										<p><?php echo $excerpt;?></p>
										<p><a href="<?php echo $extLink;?>" target="_blank"><?php echo $extCTA;?></a></p>
										<p><a href="<?php echo $link;?>"><?php echo $updateB;?></a></p>
									</div>
								</div>
								<?php
									endwhile;
								wp_reset_postdata();
							endif;
							 ?>

	</section>

	<section class="otherPosts">
				<?php
					$args = array(
							'post_type' => 'map',
							'post_type' => array('press', 'updates'),
							'posts_per_page' => 2,
							'offset' => 1,
							'order' => 'DESC'
					);
					$projects_loop = new WP_Query($args);
					if ($projects_loop->have_posts()):
							while ($projects_loop->have_posts()):
									$projects_loop->the_post();

									$title    = get_the_title();
									$play     = get_field('play_button');
									$playAlt  = get_field('play_alt');
									$excerpt  = get_the_excerpt();
									$link     = get_the_permalink();
									$extLink  = get_field('cta_link');
									$extCTA   = get_field('cta_button');
									$updateB  = get_field('update_button');
					?>

					<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
					<div class="backImg" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;" data-aos="fade-up" >

						<a href="<?php echo $link; ?>" >
								<img class="logoPrev" src="<?php echo $play; ?>" alt="<?php echo $playAlt; ?>">
						</a>

						<div class="aInfo">
							<h2><?php echo $title;?></h2>
							<p><?php echo $excerpt;?></p>
							<p><a href="<?php echo $extLink;?>" target="_blank"><?php echo $extCTA;?></a></p>
							<p><a href="<?php echo $link;?>"><?php echo $updateB;?></a></p>
						</div>
					</div>

					<?php
						endwhile;
					wp_reset_postdata();
				endif;
				 ?>
	</section>
	<section class="blogGrid">
		 <h1><?php the_field('blog_section_header'); ?></h1>
		<div class="blogGridPosts">
			<?php
			                $args = array(
			                        'post_type' => array('press', 'updates'),
			                        'post_status' => 'publish',
			                        'posts_per_page' => 100,
															'offset' => 3,
			                        'order' => 'DESC'
			                );
			                $projects_loop = new WP_Query($args);
			                if ($projects_loop->have_posts()):
			                        while ($projects_loop->have_posts()):
			                                $projects_loop->the_post();

			                                $title    = get_the_title();
			                                $play     = get_field('play_button');
			                                $playAlt  = get_field('play_alt');
			                                $excerpt  = get_the_excerpt();
																			$link     = get_the_permalink();
			                                $extLink  = get_field('cta_link');
			                                $extCTA   = get_field('cta_button');
			                                $updateB  = get_field('update_button');

			                ?><?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
			<div class="backImg" data-aos="fade-up" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
				<a href="<?php echo $link;?>"><img alt="<?php echo $playAlt; ?>" class="logoPrev" src="<?php echo $play;?>"></a>
				<div class="aInfo">
					<h2><?php echo $title;?></h2>
					<p><?php echo $excerpt;?></p>
					<p><a href="<?php echo $extLink;?>" target="_blank"><?php echo $extCTA;?></a></p>
					<p><a href="<?php echo $link;?>"><?php echo $updateB;?></a></p>
				</div>
			</div>

			<?php
			                    endwhile;
			                wp_reset_postdata();
			            endif;
			             ?>

		</div>
	</section>
</wrapper>
</body>

	<?php get_footer(); ?>

</html>
