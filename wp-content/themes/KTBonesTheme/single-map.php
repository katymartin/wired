<?php
/*
 * CUSTOM POST TYPE TEMPLATE
 *
 * This is the custom post type post template. If you edit the post type name, you've got
 * to change the name of this template to reflect that name change.
 *
 * For Example, if your custom post type is "register_post_type( 'bookmarks')",
 * then your single template should be single-bookmarks.php
 *
 * Be aware that you should rename 'custom_cat' and 'custom_tag' to the appropiate custom
 * category and taxonomy slugs, or this template will not finish to load properly.
 *
 * For more info: http://codex.wordpress.org/Post_Type_Templates
*/
?>

<?php get_header(); ?>
<div id="wrapper">
<div id="blogPost">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	<div class="backImg" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; " data-aos="fade-up" >
</div>

<div class="blog-content">
	<div class="blogHeader">
		<h1><?php the_title();?></h1>
		<h2><?php the_field('subtitle');?></h2>
		<img class="postLogo" src="<?php the_field('post_logo'); ?>">
	</div>

	<?php
		if( have_rows('col_call') ):
		while( have_rows('col_call') ): the_row(); ?>

		<div class="pCols">
			<div>
				<p><?php the_sub_field('pcol_left') ;?></p>
			</div>
			<div>
				<p><?php the_sub_field('pcol_right') ;?></p>
			</div>
		</div>

		<?php endwhile; endif; ?>

	<p><?php the_content();?></p>
		<?php
			if( have_rows('p_button') ):
			while( have_rows('p_button') ): the_row(); ?>

			<a class="postButton" href="<?php the_sub_field('post_button_link'); ?>" target="blank"><h3><?php the_sub_field('post_button'); ?></h3></a>

			<?php endwhile; endif; ?>
</div>

<div class="blogImgs">
	<?php
		if( have_rows('blog_imgs') ):
		while( have_rows('blog_imgs') ): the_row(); ?>
		<div class="bImg" style="background: url('<?php the_sub_field('b_img'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; "></div>
	<?php endwhile; endif; ?>
</div>


</div>




	<?php endwhile; ?>
	<?php endif; ?>
	</div>
</div>
</div>

<?php get_footer(); ?>
