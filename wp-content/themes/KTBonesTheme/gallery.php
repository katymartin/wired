<?php
/**
 Template Name: Gallery
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>Gallery | WiRED Properties</title>
	<meta name="description" content="Image gallery of real estate developmenet projects for WiRED properties.">
</head>

<body>
<div id="wrapper">
<section class="gallerySec innerWrap" data-aos="fade-up">
	<h1><?php the_field('page_title') ;?><svg class="accentT" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 32"><path class="tPath" d="M4.5 3.6V28L25 15.8z"/></svg></h1>

<div class="galleryFlex" >
	<?php

$images = array_reverse( get_field('gallery') );
$size = 'gallery-img'; // (thumbnail, medium, large, full or custom size)

if( $images ): ?>

        <?php foreach( $images as $image ): ?>
            <div>
							<a href="<?php echo $image['description']; ?>" target="_blank">
            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
							 <p><i class="fa fa-caret-up" aria-hidden="true"></i>  <?php echo $image['caption']; ?></p>
						 </a>
            </div>
        <?php endforeach; ?>

<?php endif; ?>
</div>

</section>
</div>
</body>


<?php get_footer(); ?>
