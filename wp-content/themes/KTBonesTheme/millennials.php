<?php
/**
 Template Name: MAP
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title> Millennial Action Plan | WiRED Properties</title>
	<meta name="description" content="Learn about WiRED strategic and tactical wayfinding for the millennial generation.">
</head>

<body>
	<div id="wrapper">

<section class="millIntro innerWrap" data-aos="fade-up">
	<h1><?php the_field('page_title') ;?><svg class="accentT" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 32"><path class="tPath" d="M4.5 3.6V28L25 15.8z"/></svg></h1>
	<?php
		if( have_rows('col_call') ):
		while( have_rows('col_call') ): the_row(); ?>

		<div class="pCols">
			<div>
				<p><?php the_sub_field('pcol_left') ;?></p>
			</div>
			<div>
				<p><?php the_sub_field('pcol_right') ;?></p>
			</div>
		</div>

		<?php endwhile; endif; ?>

</section>




<section class="featuredPosts" data-aos="fade-left">
	<?php
		$args = array(
				'post_type' => 'map',
				'post_status' => 'publish',
				'posts_per_page' => 1,
				'order' => 'DESC'
		);
		$projects_loop = new WP_Query($args);
		if ($projects_loop->have_posts()):
				while ($projects_loop->have_posts()):
						$projects_loop->the_post();
						$play     = get_field('play_button');
						$playAlt  = get_field('play_alt');
						$title    = get_the_title();
						$excerpt  = get_the_excerpt();
						$link    = get_the_permalink();
						$readM    = get_field('read_m');
		?>

							<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
              <div class="backImg" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; "  >
								<a href="<?php echo $link; ?>" >
										<img class="logoPrev" src="<?php echo $play; ?>" alt="<?php echo $playAlt; ?>">
								</a>

								<div class="aInfo">
									<h2><?php echo $title;?></h2>
									<p><?php echo $excerpt;?> <a href="<?php echo $link;?>"><?php echo $readM;?> ></a></p>
								</div>
							</div>
							<?php
								endwhile;
							wp_reset_postdata();
						endif;
						 ?>

</section>



<section class="otherPosts">
			<?php
				$args = array(
						'post_type' => 'map',
						'post_status' => 'publish',
						'posts_per_page' => 2,
						'offset' => 1,
						'order' => 'DESC'
				);
				$projects_loop = new WP_Query($args);
				if ($projects_loop->have_posts()):
						while ($projects_loop->have_posts()):
								$projects_loop->the_post();

								$title    = get_the_title();
								$play     = get_field('play_button');
								$playAlt  = get_field('play_alt');
								$excerpt  = get_the_excerpt();
								$link     = get_the_permalink();
								$readM    = get_field('read_m');
				?>

				<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
				<div class="backImg" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;" data-aos="fade-up" >

					<a href="<?php echo $link; ?>" >
							<img class="logoPrev" src="<?php echo $play; ?>" alt="<?php echo $playAlt; ?>">
					</a>

					<div class="aInfo">
						<h2><?php echo $title;?></h2>
						<p><?php echo $excerpt;?> <a href="<?php echo $link;?>"><?php echo $readM;?> ></a></p>
					</div>
				</div>

				<?php
					endwhile;
				wp_reset_postdata();
			endif;
			 ?>
</section>

<section class="blogGrid">
	<h1><?php the_field('blog_section_header'); ?></h1>
	<div class="blogGridPosts">
		<?php
										$args = array(
														'post_type' => 'map',
														'post_status' => 'publish',
														'posts_per_page' => 100,
														'offset' => 3,
														'order' => 'DESC'
										);
										$projects_loop = new WP_Query($args);
										if ($projects_loop->have_posts()):
														while ($projects_loop->have_posts()):
																		$projects_loop->the_post();

																		$title    = get_the_title();
																		$play     = get_field('play_button');
																		$playAlt  = get_field('play_alt');
																		$excerpt  = get_the_excerpt();
																		$readM    = get_field('read_m');
																		$link    = get_the_permalink();
										?><?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
		<div class="backImg" data-aos="fade-up" style="background: url('<?php echo $thumb['0'];?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
			<a href="<?php echo $link;?>"><img alt="<?php echo $playAlt; ?>" class="logoPrev" src="<?php echo $play;?>"></a>
			<div class="aInfo">
				<h2><?php echo $title;?></h2>
				<p><?php echo $excerpt;?> <a href="<?php echo $link;?>"><?php echo $readM;?> ></a></p>
			</div>
		</div>

		<?php
												endwhile;
										wp_reset_postdata();
								endif;
								 ?>

	</div>
</section>

<section class="testSec" >
	<div class="secWrapper">
	<div class="secHeader">
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 554 101" style="enable-background:new 0 0 554 101;" xml:space="preserve">
		<polygon class="miniTri fade-up-head" points="514.5,4.9 514.5,45 548.2,24.9 "/>
		<text transform="matrix(1 0 0 1 4.1025 31.749)" class="headerSubtxt fade-up-head" id="headYell">We’ve made</text>
		<g>
		<text transform="matrix(1 0 0 1 -2.0469 93.8262)" class="headerMntxt fade-up-head" fill="white" id="headBlack">some friends.</text>
		</g>
</svg>
	</div>

<div class="flex" data-aos="fade-up">
		<?php
		  if( have_rows('testimonial') ):
		  while( have_rows('testimonial') ): the_row(); ?>

			<div class="flexDiv">
				<div class="callout">
				<h4><?php the_sub_field('test_callout'); ?></h4>
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 381 37" style="enable-background:new 0 0 381 37;" xml:space="preserve">
					<line class="qLine" x1="0" y1="18" x2="360" y2="18"/>
					<g>
						<path class="qLR fade-in'" d="M27.6,28.4h-7.9v-6.9c0-3.9,0.5-6.9,1.5-9c1-2.1,2.8-3.8,5.5-4.9l1.5,4c-1.5,0.6-2.7,1.5-3.4,2.6
							c-0.7,1.1-1.1,2.7-1.1,4.6h3.8V28.4z M40.8,28.4h-7.9v-6.9c0-3.9,0.4-6.8,1.3-8.7c1.1-2.4,3-4.1,5.7-5.3l1.5,4
							c-1.6,0.6-2.7,1.5-3.4,2.6c-0.7,1.1-1.1,2.7-1.1,4.6h3.8V28.4z"/>
						<path class="qLR fade-in'" d="M49.1,7.6H57v6.9c0,3.9-0.5,6.8-1.4,8.7c-1.1,2.3-3,4.1-5.6,5.2l-1.5-4c1.6-0.6,2.7-1.5,3.4-2.6
							c0.7-1.1,1.1-2.6,1.1-4.6h-3.8V7.6z M62.4,7.6h7.9v6.9c0,3.9-0.4,6.7-1.3,8.7c-1.1,2.4-3,4.1-5.7,5.3l-1.5-4
							c1.6-0.6,2.7-1.5,3.4-2.6c0.7-1.1,1.1-2.6,1.1-4.6h-3.8V7.6z"/>
					</g>
					</svg>
				<p><?php the_sub_field('test_copy'); ?></p>
				<div class="qAuth">
				<h4><?php the_sub_field('test_author'); ?></h4>
				<p><?php the_sub_field('test_company'); ?></p>
			</div>
		</div>
	</div>

		<?php endwhile; endif; ?>
</div>
</div>
</section>
</div>
</body>


<?php get_footer(); ?>
