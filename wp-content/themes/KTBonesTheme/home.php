<?php
/**
 Template Name: Home
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>
<head>
	<title>Home | WiRED Properties</title>
	<meta name="description" content="About WiRED Properties. Communities don't just happen, they are made.">

</head>

<body>
<div id="wrapper">

<section class="hBanner" >
<div class="" >
	<div class="backImg" style="background: url('<?php the_field('banner_img'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; " data-aos="fade-left" ></div>


 <div class="homeTriangles" data-aos="fade-right">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 515 479" style="enable-background:new 0 0 515 479;" xml:space="preserve">

  <defs>
    <filter id="f_multiply" filterUnits="objectBoundingBox" x="0%" y="0%" width="100%" height="100%">
      <feBlend  mode="multiply"/>
    </filter>
  </defs>

<g>
	<polygon class="orangeT" filter="url(#f_multiply)" points="17,14 17,366 312.7,190 	"/>
</g>
<g>
	<polygon class="lineT" points="122,14 122,468.8 504,241.4 	"/>
</g>
</svg>
</div>


<div class="sideText">

<div class="topSlog">
<svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 478 221">

  <path class="orangePath" d="M478 25.5H0h478zM478 110.5H146.7 478zM478 195.5H84h394z"/>
  <g >
    <path alt="Communities don't" class="greyH1" txt-fade" d="M15.7 27.2c0-10.4 6.2-15.7 18.1-15.7 3.9 0 6.7.6 9.6 1.7v10c-3-1.4-5.7-2-8.9-2-4.5 0-7.2 1.5-7.2 6.1s2.8 6.1 7.3 6.1c3.1 0 5.9-.6 8.9-2v10c-2.9 1-5.7 1.7-9.6 1.7-11.9-.2-18.2-5.5-18.2-15.9zM45.9 31.2c0-7.8 4.6-11.7 14.1-11.7s14.2 3.9 14.2 11.7c0 7.8-4.6 11.7-14.2 11.7S45.9 39 45.9 31.2zm16.5 0c0-2.7-.8-3.5-2.5-3.5-1.6 0-2.4.9-2.4 3.5s.8 3.5 2.4 3.5c1.7 0 2.5-.8 2.5-3.5zM76.5 20h10.6l.2 1.4c1.8-1 4-1.8 6.9-1.8 2.7 0 4 .7 5.1 2.1 2-1.3 4.4-2.1 7.6-2.1 5.4 0 7.6 2.8 7.6 8.5v14.4h-11.2V29.4c0-1-.3-1.4-1.3-1.4-.5 0-.9.2-1.3.6v13.9H90.1V29.1c0-.8-.3-1.1-1-1.1-.6 0-1.2.3-1.6.9v13.6h-11V20zM117.6 20h10.6l.2 1.4c1.8-1 4-1.8 6.9-1.8 2.7 0 4 .7 5.1 2.1 2-1.3 4.4-2.1 7.6-2.1 5.4 0 7.6 2.8 7.6 8.5v14.4h-11.2V29.4c0-1-.3-1.4-1.3-1.4-.5 0-.9.2-1.3.6v13.9h-10.6V29.1c0-.8-.3-1.1-1-1.1-.6 0-1.2.3-1.6.9v13.6h-11V20zM158.5 34.2V20H170v13c0 1 .5 1.5 1.6 1.5.7 0 1.7-.4 2.1-.9V20h11.5v22.4h-10.7l-.2-1.3c-1.7 1-4.3 1.8-7.3 1.8-5.3 0-8.5-3-8.5-8.7zM188.4 20h10.7l.2 1.3c1.6-.9 4.6-1.8 7.9-1.8 5.4 0 7.7 2.8 7.7 8.5v14.4h-11.5v-13c0-1-.5-1.5-1.6-1.5-.7 0-1.7.4-2.1.9v13.5h-11.5V20zM221.2 28.2h-3.1l.9-8.2h13.6v22.4h-11.5V28.2zm-.8-18h12.3v7.9h-12.3v-7.9zM236.8 33.6v-5.4h-2.7V20h2.7v-4.4l11.5-2.3V20h4.6l-.2 8.2h-4.4V33c0 1.3.7 2 2.2 2 1 0 1.9-.1 2.8-.5v7.1c-2 .9-4.1 1.3-7 1.3-6.8 0-9.5-3.6-9.5-9.3zM257.8 28.2h-3.1l.9-8.2h13.6v22.4h-11.5V28.2zm-.8-18h12.3v7.9H257v-7.9zM271.7 31.3c0-7.1 4.3-11.8 12.7-11.8 7.7 0 12.7 3.9 12.7 10.9v3.8h-15c.9 1.2 3.1 1.9 6.6 1.9 2.8 0 5.7-.6 7.2-1.1v6.2c-2.4 1.1-5.8 1.7-10.3 1.7-7.9 0-13.9-4-13.9-11.6zm15.6-2.7v-.4c0-.9-.6-1.8-2.5-1.8s-2.7 1-2.7 2.3h5.2zM298.7 41.3v-7.2c2.2 1.2 6.1 1.9 8.1 1.9 1.4 0 2.6 0 2.6-.5 0-.7-2.6-.9-5.1-1.8-4.1-1.5-5.8-3.1-5.8-6.7 0-4.8 3.9-7.3 12.1-7.3 3 0 7.3.5 9.2 1.1V28c-2-.9-4.6-1.4-6.3-1.4-1.3 0-3.3 0-3.3.6s2.3.8 4.6 1.5c4.9 1.4 6.2 3.2 6.2 6.8 0 4.6-4.1 7.5-12 7.5-4.2-.1-8.3-.8-10.3-1.7zM336.7 31.9c0-8.4 4.6-12.4 11-12.4 1.7 0 3.7.4 4.9.9v-9.8h11.5v31.9h-9.9l-.5-1.5c-1.7 1.2-3.6 1.9-6.3 1.9-6.4 0-10.7-3-10.7-11zm15.9 2.1v-5.6c-.6-.5-1.1-.7-2-.7-1.4 0-2.2 1.4-2.2 4.1 0 2.7.9 2.9 2.2 2.9.9 0 1.4-.2 2-.7zM365.7 31.2c0-7.8 4.6-11.7 14.1-11.7S394 23.4 394 31.2c0 7.8-4.6 11.7-14.2 11.7-9.5 0-14.1-3.9-14.1-11.7zm16.6 0c0-2.7-.8-3.5-2.5-3.5-1.6 0-2.4.9-2.4 3.5s.8 3.5 2.4 3.5c1.7 0 2.5-.8 2.5-3.5zM396.4 20h10.7l.2 1.3c1.6-.9 4.6-1.8 7.9-1.8 5.4 0 7.7 2.8 7.7 8.5v14.4h-11.5v-13c0-1-.5-1.5-1.6-1.5-.7 0-1.7.4-2.1.9v13.5h-11.5V20zM428.3 11.5h9.4l-4.1 12H425l3.3-12zM440.9 33.6v-5.4h-2.7V20h2.7v-4.4l11.5-2.3V20h4.6l-.2 8.2h-4.4V33c0 1.3.7 2 2.2 2 1 0 1.9-.1 2.8-.5v7.1c-2 .9-4.1 1.3-7 1.3-6.8 0-9.5-3.6-9.5-9.3z"/>
  </g>
  <g >
    <path alt="just happen." class="greyH1" txt-fade" d="M169 131.9v-7.1c.9.3 1.8.5 2.8.5 1.5 0 2.2-.7 2.2-1.9v-15.5h-3l.9-8.2h13.6v24.5c0 5.2-2.7 9-9.6 9-2.8 0-4.9-.5-6.9-1.3zm4.3-42h12.3v7.9h-12.3v-7.9zM188.6 113.8V99.6h11.5v13c0 1 .5 1.5 1.6 1.5.7 0 1.7-.4 2.1-.9V99.6h11.5V122h-10.7l-.2-1.3c-1.7 1-4.3 1.8-7.3 1.8-5.3.1-8.5-3-8.5-8.7zM217.7 120.9v-7.2c2.2 1.2 6.1 1.9 8.1 1.9 1.4 0 2.6 0 2.6-.5 0-.7-2.6-.9-5.1-1.8-4.1-1.5-5.8-3.1-5.8-6.7 0-4.8 3.9-7.3 12.1-7.3 3 0 7.3.5 9.2 1.1v7.2c-2-.9-4.6-1.4-6.3-1.4-1.3 0-3.3 0-3.3.6s2.3.8 4.6 1.5c4.9 1.4 6.2 3.2 6.2 6.8 0 4.6-4.1 7.5-12 7.5-4.2 0-8.3-.8-10.3-1.7zM243.3 113.3v-5.4h-2.7v-8.2h2.7v-4.4l11.5-2.3v6.7h4.6l-.2 8.2h-4.4v4.8c0 1.3.7 2 2.2 2 1 0 1.9-.1 2.8-.5v7.1c-2 .9-4.1 1.3-7 1.3-6.8 0-9.5-3.6-9.5-9.3zM275.5 90.2H287v10.3c1.3-.7 3.6-1.3 5.9-1.3 6.3 0 9.2 2.9 9.2 9.1v13.8h-11.5v-12.9c0-1.1-.5-1.6-1.6-1.6-.7 0-1.7.4-2.1.9V122h-11.5V90.2zM304.1 115.1c0-5 3.1-7.7 9.8-7.7h5v-.2c0-.7-1.3-1.3-4-1.3-2.4 0-5.4.4-7.9 1.2v-6.2c2.5-1 6.7-1.7 10.2-1.7 7.5 0 12.6 3 12.6 9.5v13.4h-10l-.5-1.6c-1.5 1.2-3.8 2.1-7.2 2.1-4.4 0-8-2.2-8-7.5zm14.8.2v-2.1h-2.1c-1.5 0-2.1.4-2.1 1.6 0 1 .5 1.6 1.7 1.6 1.1-.1 1.9-.5 2.5-1.1zM332.9 99.6h9.9l.5 1.5c1.7-1.2 3.6-1.9 6.3-1.9 6.8 0 10.6 3.4 10.6 11.4s-4.1 12-11 12c-1.7 0-3.7-.4-4.9-.9v9.8h-11.5V99.6zm15.7 11.1c0-2.7-.9-3.4-2.2-3.4-.9 0-1.4.2-2 .7v5.6c.6.5 1.1.7 2 .7 1.3.1 2.2-.9 2.2-3.6zM362.6 99.6h9.9l.5 1.5c1.7-1.2 3.6-1.9 6.3-1.9 6.8 0 10.6 3.4 10.6 11.4s-4.1 12-11 12c-1.7 0-3.7-.4-4.9-.9v9.8h-11.5V99.6zm15.7 11.1c0-2.7-.9-3.4-2.2-3.4-.9 0-1.4.2-2 .7v5.6c.6.5 1.1.7 2 .7 1.3.1 2.2-.9 2.2-3.6zM391.4 111c0-7.1 4.3-11.8 12.7-11.8 7.7 0 12.7 3.9 12.7 10.9v3.8h-15c.9 1.2 3.1 1.9 6.6 1.9 2.8 0 5.7-.6 7.2-1.1v6.2c-2.4 1.1-5.8 1.7-10.3 1.7-7.9 0-13.9-4.1-13.9-11.6zm15.6-2.8v-.4c0-.9-.6-1.8-2.5-1.8s-2.7 1-2.7 2.3h5.2zM419.2 99.6H430l.2 1.3c1.6-.9 4.6-1.8 7.9-1.8 5.4 0 7.7 2.8 7.7 8.5V122h-11.5v-13c0-1-.5-1.5-1.6-1.5-.7 0-1.7.4-2.1.9v13.5h-11.5V99.6zM447.9 114.1h8.8v8h-8.8v-8z"/>
  </g>
  <g>
    <path alt="They are made." class="whiteH1 txt-fade" d="M112.1 186.2h-7.8V178h27v8.2h-7.8v22.3H112v-22.3zM134.3 176.6h11.5v10.3c1.3-.7 3.6-1.3 5.9-1.3 6.3 0 9.2 2.9 9.2 9.1v13.8h-11.5v-12.9c0-1.1-.5-1.6-1.6-1.6-.7 0-1.7.4-2.1.9v13.5h-11.5v-31.8zM163 197.3c0-7.1 4.3-11.8 12.7-11.8 7.7 0 12.7 3.9 12.7 10.9v3.8h-15c.9 1.2 3.1 1.9 6.6 1.9 2.8 0 5.7-.6 7.2-1.1v6.2c-2.4 1.1-5.8 1.7-10.3 1.7-7.9 0-13.9-4-13.9-11.6zm15.6-2.7v-.4c0-.9-.6-1.8-2.5-1.8s-2.7 1-2.7 2.3h5.2zM191.1 215.7v-7.3c.9.3 1.8.5 2.8.5 1.9 0 3.1-.2 4-1.1L187.7 186h12l3.9 11.2 3.7-11.2h11.9l-7.7 19.7c-2.7 6.9-6.9 11-13.7 11-3.1.1-4.7-.2-6.7-1zM233.9 201.5c0-5 3.1-7.7 9.8-7.7h5v-.2c0-.7-1.3-1.3-4-1.3-2.4 0-5.4.4-7.9 1.2v-6.2c2.5-1 6.7-1.7 10.2-1.7 7.5 0 12.6 3 12.6 9.5v13.4h-10l-.5-1.6c-1.5 1.2-3.8 2.1-7.2 2.1-4.4-.1-8-2.2-8-7.5zm14.8.2v-2.1h-2.1c-1.5 0-2.1.4-2.1 1.6 0 1 .5 1.6 1.7 1.6 1.1-.1 1.9-.5 2.5-1.1zM262.7 186h11.2l.2 1.3c1.6-.9 4.5-1.8 7.4-1.8v8.4c-2.4 0-5.7.3-7.4.9v13.5h-11.5V186zM283.5 197.3c0-7.1 4.3-11.8 12.7-11.8 7.7 0 12.7 3.9 12.7 10.9v3.8h-15c.9 1.2 3.1 1.9 6.6 1.9 2.8 0 5.7-.6 7.2-1.1v6.2c-2.4 1.1-5.8 1.7-10.3 1.7-7.9 0-13.9-4-13.9-11.6zm15.7-2.7v-.4c0-.9-.6-1.8-2.5-1.8s-2.7 1-2.7 2.3h5.2zM323 186h10.6l.2 1.4c1.8-1 4-1.8 6.9-1.8 2.7 0 4 .7 5.1 2.1 2-1.3 4.4-2.1 7.6-2.1 5.4 0 7.6 2.8 7.6 8.5v14.4h-11.2v-13.1c0-1-.3-1.4-1.3-1.4-.5 0-.9.2-1.3.6v13.9h-10.6v-13.4c0-.8-.3-1.1-1-1.1-.6 0-1.2.3-1.6.9v13.6h-11V186zM363.5 201.5c0-5 3.1-7.7 9.8-7.7h5v-.2c0-.7-1.3-1.3-4-1.3-2.4 0-5.4.4-7.9 1.2v-6.2c2.5-1 6.7-1.7 10.2-1.7 7.5 0 12.6 3 12.6 9.5v13.4h-10l-.5-1.6c-1.5 1.2-3.8 2.1-7.2 2.1-4.4-.1-8-2.2-8-7.5zm14.8.2v-2.1h-2.1c-1.5 0-2.1.4-2.1 1.6 0 1 .5 1.6 1.7 1.6 1.1-.1 1.9-.5 2.5-1.1zM391.3 198c0-8.4 4.6-12.4 11-12.4 1.7 0 3.7.4 4.9.9v-9.8h11.5v31.9h-9.9l-.5-1.5c-1.7 1.2-3.6 1.9-6.3 1.9-6.4-.1-10.7-3-10.7-11zm15.9 2v-5.6c-.6-.5-1.1-.7-2-.7-1.4 0-2.2 1.4-2.2 4.1 0 2.7.9 2.9 2.2 2.9.9.1 1.4-.2 2-.7zM421 197.3c0-7.1 4.3-11.8 12.7-11.8 7.7 0 12.7 3.9 12.7 10.9v3.8h-15c.9 1.2 3.1 1.9 6.6 1.9 2.8 0 5.7-.6 7.2-1.1v6.2c-2.4 1.1-5.8 1.7-10.3 1.7-7.8 0-13.9-4-13.9-11.6zm15.7-2.7v-.4c0-.9-.6-1.8-2.5-1.8s-2.7 1-2.7 2.3h5.2zM447.9 200.5h8.8v8h-8.8v-8z"/>
  </g>
</svg>
	</div>
<div class="mainP" data-aos="fade-left">
<p><span>Real estate is about more than structures that take up space; it’s about spaces that structure lives.</span> We work with stakeholders at every level to plan and to program spaces to generate community success - firmly believing that the most vibrant communities are the most physically interwoven and socially engaged.</p>
</div>
</div>
</section>

<section>
	<div class="servSec secWrapper">
		<div class="secHeader">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 510 101" style="enable-background:new 0 0 510 101;" xml:space="preserve">

					<polygon class="miniTri fade-up-head" points="469.5,19.9 469.5,60 503.2,39.9 "/>

					<text transform="matrix(1 0 0 1 5.9531 27.9663)" class="headerSubtxt fade-up-head" id="headYell">A bit of </text>
					<g >
					<text  transform="matrix(1 0 0 1 5.9531 98.8262)" class="headerMntxt fade-up-head" id="headBlack" fill="#272828">what we do.</text>
					</g>
			</svg>
		</div>


<div class="flex" data-aos="fade-up">
		<?php
		  if( have_rows('services_home') ):
		  while( have_rows('services_home') ): the_row(); ?>
			<div class="flexDiv">
				<div class="innerFlex">
					<a href="/projects#strategy"><img src="<?php the_sub_field("home_service_ico"); ?>" ></a>
					</div>
					<div class="innerFlex">
						<h3><?php the_sub_field('home_service'); ?></h3>
						<p><?php the_sub_field('home_service_desc'); ?></p>
				</div>
			</div>
			<?php endwhile; endif; ?>
	</div>
	</div>
</section>


<section class="testSec">
	<div class="secWrapper">
	<div class="secHeader">
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 554 101" style="enable-background:new 0 0 554 101;" xml:space="preserve">
		<polygon class="miniTri fade-up-head" points="514.5,4.9 514.5,45 548.2,24.9 "/>
		<text transform="matrix(1 0 0 1 4.1025 31.749)" class="headerSubtxt fade-up-head" id="headYell">We’ve made</text>
		<g>
		<text transform="matrix(1 0 0 1 -2.0469 93.8262)" class="headerMntxt fade-up-head" fill="white" id="headBlack">some friends.</text>
		</g>
</svg>
	</div>

<div class="flex" data-aos="fade-up">
		<?php
		  if( have_rows('testimonial') ):
		  while( have_rows('testimonial') ): the_row(); ?>

			<div class="flexDiv">
				<div class="callout">
				<h4><?php the_sub_field('test_callout'); ?></h4>
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 381 37" style="enable-background:new 0 0 381 37;" xml:space="preserve">
					<line class="qLine" x1="0" y1="18" x2="360" y2="18"/>
					<g>
						<path class="qLR fade-in'" d="M27.6,28.4h-7.9v-6.9c0-3.9,0.5-6.9,1.5-9c1-2.1,2.8-3.8,5.5-4.9l1.5,4c-1.5,0.6-2.7,1.5-3.4,2.6
							c-0.7,1.1-1.1,2.7-1.1,4.6h3.8V28.4z M40.8,28.4h-7.9v-6.9c0-3.9,0.4-6.8,1.3-8.7c1.1-2.4,3-4.1,5.7-5.3l1.5,4
							c-1.6,0.6-2.7,1.5-3.4,2.6c-0.7,1.1-1.1,2.7-1.1,4.6h3.8V28.4z"/>
						<path class="qLR fade-in'" d="M49.1,7.6H57v6.9c0,3.9-0.5,6.8-1.4,8.7c-1.1,2.3-3,4.1-5.6,5.2l-1.5-4c1.6-0.6,2.7-1.5,3.4-2.6
							c0.7-1.1,1.1-2.6,1.1-4.6h-3.8V7.6z M62.4,7.6h7.9v6.9c0,3.9-0.4,6.7-1.3,8.7c-1.1,2.4-3,4.1-5.7,5.3l-1.5-4
							c1.6-0.6,2.7-1.5,3.4-2.6c0.7-1.1,1.1-2.6,1.1-4.6h-3.8V7.6z"/>
					</g>
					</svg>
				<p><?php the_sub_field('test_copy'); ?></p>
				<div class="qAuth">
				<h4><?php the_sub_field('test_author'); ?></h4>
				<p><?php the_sub_field('test_company'); ?></p>
			</div>
		</div>
	</div>

		<?php endwhile; endif; ?>
</div>
</div>
</section>

<section class="logoSec">
	<div class="secWrapper">
	<div class="flex">
		<?php
		  if( have_rows('home_logo') ):
		  while( have_rows('home_logo') ): the_row(); ?>

			<div class="flexDiv" data-aos="zoom-in">
				<a href="<?php the_sub_field('logo_link'); ?>" >
			<img src="<?php the_sub_field('logo_img'); ?>" alt="<?php the_sub_field('logo_alt'); ?>"/>
			</a>
		</div>
	<?php endwhile; endif; ?>

	</div>
	</div>
</section>

</div>
</body>


<?php get_footer(); ?>
